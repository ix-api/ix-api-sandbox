
"""
Test Catalog Facilities Services
"""

import pytest
from model_mommy import mommy

from jea.catalog.services import facilities as facilities_svc

@pytest.mark.django_db
def test_list_facilities():
    """
    Test listing facilities
    """
    facility = mommy.make("catalog.Facility")

    facilities = facilities_svc.get_facilities(filters={
        "metro_area": facility.metro_area,
    })
    assert facility in facilities

    facilities = facilities_svc.get_facilities(filters={
        "metro_area": ".",
    })
    assert not facility in facilities


@pytest.mark.django_db
def test_get_facility_by_id():
    """Test retreiving a facility"""
    facility = mommy.make("catalog.Facility")
    facility_ = facilities_svc.get_facility(facility=str(facility.pk))

    assert facility.pk == facility_.pk

