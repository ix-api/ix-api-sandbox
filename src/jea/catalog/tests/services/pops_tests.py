
import pytest
from model_mommy import mommy

from jea.catalog.models import (
    MediaType,
)
from jea.catalog.exceptions import MediaTypeUnavailable
from jea.catalog.services import pops as pops_svc


@pytest.mark.django_db
def test_get_pops():
    """Test listing points of presence"""
    pop = mommy.make("catalog.PointOfPresence")
    pops = pops_svc.get_pops()
    assert pop in pops


@pytest.mark.django_db
def test_list_pops_filter_physical_facility():
    """Test filtering by physical facility id"""
    facility = mommy.make("catalog.Facility")
    pop = mommy.make("catalog.PointOfPresence", physical_facility=facility)

    pops = pops_svc.get_pops(filters={
        "physical_facility_id": facility.id,
    })

    assert pops.count() == 1
    assert pop in pops


@pytest.mark.django_db
def test_list_demarcs_filter_reachable_facility():
    """Test filtering by physical facility id"""
    #
    # Topology: DEMARC A: Has no physical devices
    #           DEMARC B: Has a device and is present at DEMARC A
    #
    # DEMARC A is in FACILITY A
    # DEMARC B is in FACILITY B

    facility_a = mommy.make("catalog.Facility")
    facility_b = mommy.make("catalog.Facility")

    pop_a = mommy.make("catalog.PointOfPresence",
                       physical_facility=facility_a)
    pop_b = mommy.make("catalog.PointOfPresence",
                       physical_facility=facility_b)

    device_a = mommy.make("catalog.Device",
                          name="demarc_b_device_a",
                          physical_point_of_presence=pop_b,
                          reachable_points_of_presence=[pop_a])

    pops = pops_svc.get_pops(filters={
        "reachable_facility": facility_b.pk,
    })

    # Facility B should be reachable:
    assert pops.count() == 1
    assert pops.first().id == pop_a.pk


@pytest.mark.django_db
def test_get_pops_by_id():
    """Test retrieving a pop by id"""
    pop = mommy.make("catalog.PointOfPresence")
    pop_ = pops_svc.get_pop(pop=str(pop.id))
    assert pop.id == pop_.id


@pytest.mark.django_db
def test_get_media_type_availability():
    """Test counting available ports with given capabilities"""
    pop = mommy.make("catalog.PointOfPresence")
    device_a = mommy.make("catalog.Device",
                          physical_point_of_presence=pop)
    mommy.make("catalog.DeviceCapability",
               device=device_a,
               media_type="10GSTUFF",
               availability_count=10)

    mommy.make("catalog.DeviceCapability",
               device=device_a,
               media_type="100GSTUFF",
               availability_count=0)

    device_b = mommy.make("catalog.Device")
    device_b.reachable_points_of_presence.add(pop)

    mommy.make("catalog.DeviceCapability",
               device=device_b,
               media_type="10GSTUFF",
               availability_count=13)

    count = pops_svc.get_media_type_availability(
        point_of_presence=pop,
        media_type="10GSTUFF",
        presence="any",
    )
    assert count == 23

    count = pops_svc.get_media_type_availability(
        point_of_presence=pop,
        media_type="10GSTUFF",
        presence="physical",
    )
    assert count == 10

    count = pops_svc.get_media_type_availability(
        point_of_presence=pop,
        media_type="100GSTUFF",
        presence="physical",
    )
    assert count == 0


@pytest.mark.django_db
def test_get_media_type_availability__invalid_media_type():
    """Try getting the capabilities of an unknown media type"""
    pop = mommy.make("catalog.PointOfPresence")
    device_a = mommy.make("catalog.Device",
                          physical_point_of_presence=pop)
    mommy.make("catalog.DeviceCapability",
               device=device_a,
               media_type="10GBASE-LR",
               availability_count=10)

    with pytest.raises(MediaTypeUnavailable):
        pops_svc.get_media_type_availability(
            point_of_presence=pop,
            media_type="100GFOO-MX")

