
"""
Filters for Catalog Models
"""

import django_filters
from django.db import models

from utils import filters
from jea.catalog.models import (
    PointOfPresence,
    Facility,
    Device,
    Product,

    PRODUCT_TYPE_EXCHANGE_LAN,
    PRODUCT_TYPE_CLOSED_USER_GROUP,
    PRODUCT_TYPE_ELINE,
    PRODUCT_TYPE_CLOUD,
    INV_PRODUCT_TYPES,
)

class FacilityFilter(django_filters.FilterSet):
    """Filter Facilities"""
    id = filters.BulkIdFilter()

    cluster = django_filters.CharFilter(
        distinct=True,
        field_name="cluster__name", lookup_expr="iexact")

    device = django_filters.CharFilter(
        distinct=True,
        field_name="physical_points_of_presence__physical_devices")

    capability_speed = django_filters.NumberFilter(
        distinct=True,
        field_name="physical_points_of_presence__physical_devices__capabilities__speed")

    capability_speed__lt = django_filters.NumberFilter(
        distinct=True,
        field_name="physical_points_of_presence__physical_devices__capabilities__speed",
        lookup_expr="lt")

    capability_speed__lte = django_filters.NumberFilter(
        distinct=True,
        field_name="physical_points_of_presence__physical_devices__capabilities__speed",
        lookup_expr="lte")

    capability_speed__gt = django_filters.NumberFilter(
        distinct=True,
        field_name="physical_points_of_presence__physical_devices__capabilities__speed",
        lookup_expr="gt")

    capability_speed__gte = django_filters.NumberFilter(
        distinct=True,
        field_name="physical_points_of_presence__physical_devices__capabilities__speed",
        lookup_expr="gte")

    organisation_name = django_filters.CharFilter(
        distinct=True,
        field_name="operator__name",
        lookup_expr="iexact")


    # TODO:
    # network_service = django_filters.CharFilter(
    #   distinct=True,
    #   field_name="huh."
    #

    class Meta:
        model = Facility
        fields = [
            "metro_area", "address_country", "address_locality",
            "postal_code"
        ]

        filter_overrides = {
            models.CharField: {
                'filter_class':
                    django_filters.CharFilter,
                'extra': lambda f: {
                    'lookup_expr': 'iexact',
                },
            },
        }


class DeviceFilter(django_filters.FilterSet):
    """Filters for devices"""
    id = filters.BulkIdFilter()

    capability_media_type = django_filters.CharFilter(
        distinct=True,
        field_name="capabilities__media_type",
        lookup_expr="iexact")

    capability_speed = django_filters.NumberFilter(
        distinct=True,
        field_name="capabilities__speed")

    capability_speed__lt = django_filters.NumberFilter(
        distinct=True,
        field_name="capabilities__speed",
        lookup_expr="lt")

    capability_speed__lte = django_filters.NumberFilter(
        distinct=True,
        field_name="capabilities__speed",
        lookup_expr="lte")

    capability_speed__gt = django_filters.NumberFilter(
        distinct=True,
        field_name="capabilities__speed",
        lookup_expr="gt")

    capability_speed__gte = django_filters.NumberFilter(
        distinct=True,
        field_name="capabilities__speed",
        lookup_expr="gte")

    facility = django_filters.CharFilter(
        distinct=True,
        field_name="physical_point_of_presence__physical_facility")

    product = django_filters.CharFilter(
        method="filter_product_available")

    def filter_product_available(self, queryset, name, value):
        """Filter devices queryset by product availability."""
        # TODO: ImplementMe, see below in product filter
        return queryset


    class Meta:
        model = Device
        fields = [
            "name",
        ]

        filter_overrides = {
            models.CharField: {
                'filter_class':
                    django_filters.CharFilter,
                'extra': lambda f: {
                    'lookup_expr': 'iexact',
                },
            },
        }


class PointOfPresenceFilter(django_filters.FilterSet):
    """Filters for pops"""
    id = filters.BulkIdFilter()

    physical_facility = django_filters.CharFilter(
        field_name="physical_facility_id")

    reachable_facility = django_filters.CharFilter(
        distinct=True,
        field_name="reachable_devices__physical_point_of_presence__physical_facility")

    reachable_devices = filters.BulkIdFilter(field_name="reachable_devices")
    #
    # placeholder:
    # product_id = django_filters.CharFiler(
    #   distinct=True, field_name="???")
    #
    class Meta:
        model = PointOfPresence
        fields = [] # allow only the fields above


class ProductFilter(django_filters.FilterSet):
    """Filter Products"""

    TYPE_CHOICES = (
        (PRODUCT_TYPE_EXCHANGE_LAN, PRODUCT_TYPE_EXCHANGE_LAN),

    )
    """
        (PRODUCT_TYPE_CLOSED_USER_GROUP, PRODUCT_TYPE_CLOSED_USER_GROUP),
        (PRODUCT_TYPE_ELINE, PRODUCT_TYPE_ELINE),
        (PRODUCT_TYPE_CLOUD, PRODUCT_TYPE_CLOUD),
    """

    # Field Filters
    id = filters.BulkIdFilter()

    name = django_filters.CharFilter(
        field_name="name", lookup_expr="iexact")

    metro_area = django_filters.CharFilter(
        field_name="exchangelannetworkproduct__metro_area",
        lookup_expr="iexact")

    provider = django_filters.CharFilter(
        field_name="cloudnetworkproduct__provider__name",
        lookup_expr="iexact")

    handover_point = django_filters.CharFilter(
        field_name="cloudnetworkproduct__handover_point",
        lookup_expr="iexact")

    zone = django_filters.CharFilter(
        field_name="cloudnetworkproduct__zone",
        lookup_expr="iexact")

    device = django_filters.CharFilter(
        method="filter_device")

    def filter_device(self, queryset, _name, value):
        """Filter by device availability"""
        # For now just assume this is available everywhere
        # TODO: Implement availability managment

        return queryset

    #
    # Polymorphic Type Filter
    #
    type = django_filters.ChoiceFilter(
        method="filter_type",
        choices=TYPE_CHOICES)


    def filter_type(self, queryset, _name, value):
        """Filter polymorphic by type"""
        product_class = INV_PRODUCT_TYPES.get(value)
        if not product_class:
            return queryset.none()

        # Apply filter
        return queryset.instance_of(product_class)


    class Meta:
        model = Product
        fields = {
            "name": ["exact", "contains"],
        }


