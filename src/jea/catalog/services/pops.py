
"""
Points Of Presence Service
"""

from typing import Optional

from django.db.models.query import QuerySet

from jea.eventmachine import active
from jea.catalog.exceptions import MediaTypeUnavailable
from jea.catalog.models import (
    PointOfPresence,
)
from jea.catalog.filters import (
    PointOfPresenceFilter,
)


def get_pops(scoping_customer=None, filters=None) -> QuerySet:
    """
    List all pops - apply filters.

    :param scoping_customer: The current customer
    :param filters: A dict of filter criteria
    """
    filtered = PointOfPresenceFilter(filters)
    return filtered.qs


def get_pop(
        scoping_customer=None,
        point_of_presence=None,
        pop=None,
    ) -> PointOfPresence:
    """
    Get a specific pop.
    Implemented lookup methods are:
     - pop_id

    :param pop: The pop to lookup
    :raises PointOfPresence.DoesNotExist: When pop is not found
    """
    if point_of_presence:
        pop = point_of_presence

    if isinstance(pop, PointOfPresence):
        return pop

    return PointOfPresence.objects.get(pk=pop)


def get_media_type_availability(
        scoping_customer=None,
        point_of_presence=None,
        media_type=None,
        presence="physical",
    ):
    """
    Count ports with given properties available on a dmearc.

    :param point_of_presence: The PoP to query
    :param media_type: The connection type e.g. 10GBASE-LR
    :param presence: Take `remote`, `physical` or `any` devices into account.

    :raises MediaTypeUnavailable: When no device could be found.
    """
    if not media_type:
        raise MediaTypeUnavailable()

    pop = get_pop(
        point_of_presence=point_of_presence,
        scoping_customer=scoping_customer)

    devices = point_of_presence.available_devices
    if presence != "any":
        devices = point_of_presence.available_devices.filter(presence=presence)

    # Apply connection type filter
    devices = devices.filter(capabilities__media_type__iexact=media_type) \
        .prefetch_related("capabilities")

    if not devices.exists():
        raise MediaTypeUnavailable()

    # Let's sum the availabilities
    count = 0
    for device in devices:
        capabilities = device.capabilities.filter(media_type__iexact=media_type)
        count += sum(capa.availability_count for capa in capabilities)

    return count

