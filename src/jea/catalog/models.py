
"""
Catalog Models
--------------

Provide models for a service catalog implementation.
This implementation might differ vastly from a real
world setup in an IX.
"""

import enumfields
from django.db import models
from django.db.models import expressions
from polymorphic.models import PolymorphicModel

from utils.datastructures import reverse_mapping

#
# TODO: Open Questions
#  - Why does the ExchangeLanProduct have a metro area?
#    This should be optional / nullable.
#


#
# Infrastructure
#
class CloudProvider(models.Model):
    """A model for cloud providers"""
    name = models.CharField(max_length=80)

    def __str__(self):
        """Make string representation"""
        return self.name

    def __repr__(self):
        """Make representation"""
        return f"<CloudProvider id='{self.pk}' name='{self.name}'>"

    class Meta:
        verbose_name = "Cloud Provider"


class FacilityOperator(models.Model):
    """A facility is run by some operator"""
    name = models.CharField(max_length=80)

    def __str__(self):
        """Make string representation"""
        return self.name

    def __repr__(self):
        """Make representation"""
        return f"<FacilityOperator id='{self.pk}' name='{self.name}'>"

    class Meta:
        verbose_name = "Facility Operator"


class FacilityCluster(models.Model):
    """Facilities can be clustered."""
    name = models.CharField(max_length=80)

    def __str__(self):
        """Make string representation"""
        return self.name

    def __repr__(self):
        """Make representation"""
        return f"<FacilityCluster id='{self.pk}' name='{self.name}'>"

    class Meta:
        verbose_name = "Facility Cluster"


class Facility(models.Model):
    """A model for physical facilities"""
    name = models.CharField(max_length=80)
    metro_area = models.CharField(max_length=3)

    # We derive the "organisationName" from the
    # operator of the facility.
    operator = models.ForeignKey(
        FacilityOperator,
        related_name="facilities",
        on_delete=models.CASCADE)
    cluster = models.ForeignKey(
        FacilityCluster,
        related_name="facilities",
        null=True, default=None, blank=True,
        on_delete=models.CASCADE)

    # Address
    address_country = models.CharField(max_length=2)
    address_locality = models.CharField(max_length=80)
    address_region = models.CharField(max_length=80)
    postal_code = models.CharField(max_length=18)
    street_address = models.CharField(max_length=80)

    # External references
    peeringdb_facility_id = models.PositiveIntegerField(
        blank=True,
        null=True,
        default=None)

    # Referenced By:
    # - Devices (As physical facility)
    # - PointOfPresence (As reachable facilities, M2M)
    # - PointOfPresence (As physical facility)

    def __str__(self):
        """Make facility string representation"""
        if self.cluster_id:
            return f"{self.name} in {self.cluster.name}"

        return self.name


    def __repr__(self):
        """Make representation"""
        if self.cluster_id:
            return (f"<Facility id='{self.pk}' name='{self.name}' "
                    f"cluster='{self.cluster.name}' >")

        return f"<Facility id='{self.pk}' name='{self.name}'>"

    @property
    def operator_name(self):
        """Retrieve the operator name"""
        if self.operator:
            return self.operator.name

        return None

    @property
    def cluster_name(self):
        """Get name of the cluster"""
        if self.cluster:
            return self.cluster.name

        return None

    @property
    def physical_devices(self):
        """Get all physical devices in the faciltiy via pops"""
        pops = self.physical_points_of_presence.all()
        return Device.objects.filter(
            physical_point_of_presence__in=pops).distinct()

    @property
    def reachable_devices(self):
        """Get all transported / virtual devices in the faciltiy via pops"""
        pops = self.physical_points_of_presence.all()
        return Device.objects.filter(
            reachable_points_of_presence__in=pops).distinct()

    @property
    def available_devices(self):
        """Get all devices in the facility"""
        return (self.physical_devices | self.reachable_devices).distinct()

    class Meta:
        verbose_name = "Facility"
        verbose_name_plural = "Facilities"


class MediaType(enumfields.Enum):
    """A type of media converter"""
    TYPE_1000BASE_LX = "1000BASE-LX"
    TYPE_10GBASE_LR = "10GBASE-LR"
    TYPE_100GBASE_LR = "100GBASE-LR"
    TYPE_100GBASE_LR4 = "100GBASE-LR4"
    TYPE_400GBASE_LR = "400GBASE-LR"
    TYPE_400GBASE_LR8 = "400GBASE-LR8"


class Device(models.Model):
    """A model for a network device"""
    name = models.CharField(max_length=180)

    # Relations
    physical_point_of_presence = models.ForeignKey(
        "catalog.PointOfPresence",
        on_delete=models.CASCADE,
        related_name="physical_devices")

    @property
    def physical_facility_id(self):
        """Get the physical facility via the pop"""
        return self.physical_point_of_presence.physical_facility_id

    @property
    def physical_facility(self):
        """Get the physical facility via the pop"""
        return self.physical_point_of_presence.physical_facility

    def __str__(self):
        """Make string representation"""
        return self.name

    def __repr__(self):
        return f"<Device id='{self.pk}' name='{self.name}'>"

    class Meta:
        verbose_name = "Device"


class DeviceConnection(models.Model):
    """Device to device connection with capacity"""
    max_capacity = models.PositiveIntegerField()

    # Device A
    device = models.ForeignKey(
        Device,
        related_name="outbound_device_connections",
        on_delete=models.CASCADE)

    # Device B
    connected_device = models.ForeignKey(
        Device,
        related_name="inbound_device_connections",
        on_delete=models.CASCADE)

    def __str__(self):
        """Make string representation"""
        str_repr = ""
        if self.device_id:
            str_repr += f"{self.device.name} [id: {self.device_id}]"
        str_repr += f" -[{self.max_capacity} Mbps]-> "
        if self.connected_device_id:
            str_repr += (f" {self.connected_device.name} "
                         f"[id: {self.connected_device_id}]")

        return str_repr

    def __repr__(self):
        """Make simple representation"""
        return f"<DeviceConnection {self.__str__()}>"

    class Meta:
        verbose_name = "Device To Device Connection"


class DeviceCapability(models.Model):
    """Capabilities of a device"""
    media_type = models.CharField(max_length=20)
    speed = models.PositiveIntegerField()

    q_in_q = models.BooleanField()
    max_lag = models.PositiveSmallIntegerField() # Max: 32767

    availability_count = models.PositiveIntegerField()

    # Relations
    device = models.ForeignKey(
        Device,
        related_name="capabilities",
        on_delete=models.CASCADE)

    def __str__(self):
        """Device Capability To String"""
        return f"DeviceCapability ({self.pk}, {self.type})"

    def __repr__(self):
        """DeviceCapability representation"""
        return (f"<DeviceCapability id='{self.pk}' "
                f"type='{self.type}' "
                f"speed='{self.speed}'>")

    class Meta:
        verbose_name = "Device Capability"
        verbose_name_plural = "Device Capabilities"


class PointOfPresence(models.Model):
    """A point of presence"""
    name = models.CharField(max_length=40)

    # Relations
    physical_facility = models.ForeignKey(
        Facility,
        related_name="physical_points_of_presence",
        on_delete=models.CASCADE)

    # Reachable devices are devices which are 'patched'
    # into the pop and are available there,
    # eventhough they might be in a different facility.
    reachable_devices = models.ManyToManyField(
        Device,
        related_name="reachable_points_of_presence")


    @property
    def available_devices(self):
        """Get list of devices this pop is available"""
        # This is the union of pysical devices
        # and reachable devices.
        devices = (self.physical_devices.all() |
                   self.reachable_devices.all()).distinct()

        # We annotate the devices with a presence flag
        return devices.annotate(
            presence=expressions.Case(
                expressions.When(physical_point_of_presence_id=self.pk,
                                 then=expressions.Value(
                                     "physical", models.CharField())),
                default=expressions.Value("remote", models.CharField())))


    @property
    def available_physical_devices(self):
        """Like available devices, just with phys"""
        return self.physical_devices.all()


    @property
    def available_facilities(self):
        """Get a queryset of all available facilities"""
        devices = self.physical_devices.all()|self.reachable_devices.all()

        return Facility.objects.filter(
            physical_points_of_presence__physical_devices__in=devices) \
                .distinct()


    @property
    def reachable_facilities(self):
        """Get reachable facilities via reachable devices"""
        devices = self.reachable_devices.all()
        return Facility.objects.filter(
            physical_points_of_presence__physical_devices__in=devices) \
                .distinct()


    def __str__(self):
        """PoP string representation"""
        return self.name

    def __repr__(self):
        """Make representation"""
        return f"<PointOfPresence id='{self.pk}' name='{self.name}'>"

    class Meta:
        verbose_name = "Point Of Presence"

#
# Products
#

class Product(PolymorphicModel):
    """The product base model"""
    name = models.CharField(max_length=80)

    # Available on the following devices
    devices = models.ManyToManyField(
        Device,
        related_name="products")


class ExchangeLanNetworkProduct(Product):
    """A model for the exchange lan product"""
    metro_area = models.CharField(
        max_length=3,
        null=True, blank=True) # airport code

    def __str__(self):
        """String representation"""
        if self.metro_area:
            return f"ExchangeLAN Product: {self.name} in {self.metro_area}"

        return f"ExchangeLAN Product: {self.name}"

    def __repr__(self):
        """Make representation"""
        return (f"<ExchangeLanNetworkProduct id='{self.pk}' "
                f"metro_area='{self.metro_area}'>")

    class Meta:
        verbose_name = "ExchangeLAN Network Product"


class ClosedUserGroupNetworkProduct(Product):
    """A closed user group product model"""
    def __str__(self):
        """String representation"""
        return f"ClosedUserGroup: {self.name}"

    def __repr__(self):
        """Make representation"""
        return f"<ClosedUserGroupNetworkProduct id='{self.pk}'>"

    class Meta:
        verbose_name = "Closed UserGroup Network Product"


class ELineNetworkProduct(Product):
    """A network product on the eline"""
    def __str__(self):
        """String representation"""
        return f"E-Line: {self.name}"

    def __repr__(self):
        """Make representation"""
        return f"<ELineNetworkProduct id='{self.pk}'>"

    class Meta:
        verbose_name = "E-Line Network Product"


class CloudNetworkProduct(Product):
    """A cloud network product"""
    zone = models.CharField(max_length=80)
    handover_point = models.CharField(max_length=80)

    provider = models.ForeignKey(
        CloudProvider,
        related_name="cloud_network_products",
        on_delete=models.CASCADE)

    @property
    def cloud_provider_name(self):
        """Retrieve cloud provider name"""
        if self.provider:
            return self.provider.name
        return None

    # TODO: Add cloud provider to prefetch_related defaults.

    def __str__(self):
        """String representation"""
        return f"CloudNetwork: {self.name}, {self.zone}, {self.handover_point}"

    def __repr__(self):
        """Make representation"""
        return (f"<CloudNetworkProduct id='{self.pk}' "
                f"zone='{self.zone}' handover_point='{self.handover_point}'>")

    class Meta:
        verbose_name = "Cloud Network Product"

# Define set of products types with a mapping
# to the corresponding classes:
PRODUCT_TYPE_EXCHANGE_LAN = "exchange_lan"
PRODUCT_TYPE_CLOSED_USER_GROUP = "closed_user_group"
PRODUCT_TYPE_ELINE = "eline"
PRODUCT_TYPE_CLOUD = "cloud"

PRODUCT_TYPES = {
    ExchangeLanNetworkProduct: PRODUCT_TYPE_EXCHANGE_LAN,
    ClosedUserGroupNetworkProduct: PRODUCT_TYPE_CLOSED_USER_GROUP,
    ELineNetworkProduct: PRODUCT_TYPE_ELINE,
    CloudNetworkProduct: PRODUCT_TYPE_CLOUD,
}

INV_PRODUCT_TYPES = reverse_mapping(PRODUCT_TYPES)


