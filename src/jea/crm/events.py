
"""
Event Creators
"""

from jea.eventmachine.models import Event
from jea.crm.models import (
    NocContact,
    BillingContact,
    LegalContact,
    ImplementationContact,
)

CUSTOMER_CREATED = "@crm/customer_created"
CUSTOMER_UPDATED = "@crm/customer_updated"
CUSTOMER_DELETED = "@crm/customer_deleted"
CUSTOMER_STATE_CHANGED = "@crm/customer_state_changed"

CONTACT_CREATED = "@crm/contact_created"
CONTACT_UPDATED = "@crm/contact_updated"
CONTACT_DELETED = "@crm/contact_deleted"


def customer_created(customer):
    """Make customer created event"""
    manager = customer.scoping_customer
    if not manager:
        manager = customer

    return Event(
        type=CUSTOMER_CREATED,
        payload={
            "customer": customer.pk,
        },
        ref=customer,
        customer=manager,
    )


def customer_updated(customer):
    """Make customer updated event"""
    manager = customer.scoping_customer
    if not manager:
        manager = customer

    return Event(
        type=CUSTOMER_UPDATED,
        payload={
            "customer": customer.pk,
        },
        ref=customer,
        customer=manager,
    )


def customer_state_changed(customer, prev_state, next_state):
    """Customer state changed"""
    manager = customer.scoping_customer
    if not manager:
        manager = customer

    return Event(
        type=CUSTOMER_STATE_CHANGED,
        payload={
            "customer": customer.pk,
            "prev_state": prev_state.value,
            "next_state": next_state.value,
        },
        ref=customer,
        customer=manager,
    )


def contact_created(contact):
    """Make contact created event"""
    contact_type = "unknown"
    if isinstance(contact, NocContact):
        contact_type = "noc"
    elif isinstance(contact, BillingContact):
        contact_type = "billing"
    elif isinstance(contact, LegalContact):
        contact_type = "legal"
    elif isinstance(contact, ImplementationContact):
        contact_type = "implementation"

    return Event(
        type=CONTACT_CREATED,
        payload={
            "contact": contact.pk,
            "contact_type": contact_type,
        },
        ref=contact,
        customer=contact.scoping_customer,
    )


def contact_updated(contact):
    """Make a contact updated eveent"""
    return Event(
        type=CONTACT_UPDATED,
        payload={
            "contact": contact.pk,
            "managing_customer": contact.managing_customer_id,
            "consuming_customer": contact.consuming_customer_id,
        },
        ref=contact,
        customer=contact.scoping_customer,
    )


def contact_deleted(contact):
    """Make contact delete event"""
    return Event(
        type=CONTACT_DELETED,
        payload={
            "contact": contact.pk,
        },
        ref=contact.consuming_customer,
        customer=contact.scoping_customer,
    )

