
"""
Test event creators
"""

import pytest
from model_mommy import mommy

from jea.eventmachine.models import (
    Event,
    State,
)
from jea.crm import events


#
# Customers
#

@pytest.mark.django_db
def test_customer_created():
    """Test customer create event creator"""
    customer = mommy.make("crm.Customer")
    event = events.customer_created(customer)

    assert isinstance(event, Event)


@pytest.mark.django_db
def test_customer_updated():
    """Test customer updated event creator"""
    customer = mommy.make("crm.Customer")
    event = events.customer_updated(customer)
    assert event


@pytest.mark.django_db
def test_customer_state_changed():
    """Test customer create event creator"""
    customer = mommy.make("crm.Customer")
    event = events.customer_state_changed(
        customer,
        customer.state, State.ERROR)

    assert isinstance(event, Event)

#
# Contacts
#


@pytest.mark.django_db
def test_contact_created():
    """Test customer contact create event creator"""
    contact = mommy.make("crm.NocContact")
    event = events.contact_created(contact)

    assert isinstance(event, Event)
    assert event.payload["contact_type"] == "noc"


@pytest.mark.django_db
def test_contact_updated():
    """Test customer contact update event"""
    contact = mommy.make("crm.BillingContact")
    event = events.contact_updated(contact)
    assert event


@pytest.mark.django_db
def test_contact_deleted():
    """Test customer contact deleted event"""
    contact = mommy.make("crm.ImplementationContact")
    event = events.contact_deleted(contact)
    assert event

