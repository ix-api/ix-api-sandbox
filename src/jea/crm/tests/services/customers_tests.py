
"""
Customers Services Tests
"""

import pytest
from model_mommy import mommy
from django.core.exceptions import PermissionDenied

from jea.exceptions import ResourceAccessDenied
from jea.crm.models import Customer
from jea.crm.services import customers as customers_svc


@pytest.mark.django_db
def test_get_customers():
    """Test customer queryset"""
    manager = mommy.make(Customer, name="manager_1")
    c1 = mommy.make(Customer, name="c1", scoping_customer=manager)
    c2 = mommy.make(Customer, name="c2")

    customers = customers_svc.get_customers()
    assert manager in customers
    assert c1 in customers
    assert c2 in customers

    customers = customers_svc.get_customers(scoping_customer=manager)
    assert manager in customers
    assert c1 in customers
    assert not c2 in customers

    customers = customers_svc.get_customers(scoping_customer=c2)
    assert not manager in customers
    assert not c1 in customers
    assert c2 in customers


@pytest.mark.django_db
def test_create_customer():
    """Try to create a new customer"""

    # A simple test customer
    generator = mommy.prepare(Customer)

    # Now create the customer without a parent
    data = {"name": "RootCustomer" + generator.name[:23]}
    subcustomer = customers_svc.create_customer(
        customer_input=data)

    customer = mommy.make(Customer, name="root customer",
        scoping_customer=subcustomer)

    # Create with manager 
    data = {
        "name": "SubCustomer" + generator.name[:23],
        "parent": customer}
    customer = customers_svc.create_customer(
        scoping_customer=subcustomer,
        customer_input=data)
    assert customer.scoping_customer.pk == subcustomer.pk


@pytest.mark.django_db
def test_get_customer__no_check():
    """Test retrieving a single customer"""
    c1 = mommy.make(Customer)
    customer = customers_svc.get_customer(
        customer=c1)
    assert customer.pk == c1.pk


@pytest.mark.django_db
def test_get_customer__by_id():
    """Test getting the customer with only the id present"""
    c1 = mommy.make(Customer)
    customer = customers_svc.get_customer(
        customer=str(c1.pk))
    assert customer.pk == c1.pk


@pytest.mark.django_db
def test_get_customer__permission_check():
    """Test retrieving a single customer, perform a permission check"""
    m1 = mommy.make(Customer, name="manager1")
    c1 = mommy.make(Customer, scoping_customer=m1)
    c2 = mommy.make(Customer)

    # Objects are resolved, only perform a permission check
    customer = customers_svc.get_customer(
        scoping_customer=m1,
        customer=c1)
    assert customer == c1

    with pytest.raises(ResourceAccessDenied):
        customers_svc.get_customer(
            scoping_customer=m1,
            customer=c2.pk)


@pytest.mark.django_db
def test_update_customer():
    """Test updating a customer"""
    manager = mommy.make(Customer)
    customer = mommy.make(Customer, scoping_customer=manager)

    update = {
        "external_ref": "fnorf2000",
        "name": "foo",
    }

    customer = customers_svc.update_customer(
        scoping_customer=manager,
        customer=customer,
        customer_update=update)

    assert customer.external_ref == "fnorf2000"
    assert customer.name == "foo"

