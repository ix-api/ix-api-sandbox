
"""
Contacts Service Tests
"""

import pytest
import mock
from model_mommy import mommy

from jea.crm.models import (
    NocContact,
    BillingContact,
    ImplementationContact,
)
from jea.crm.services import contacts as contacts_svc
from jea.crm.exceptions import RequiredContactTypesInvalid


@pytest.mark.django_db
def test_get_contacts():
    """Get all contacts within managing customer scope"""
    m1 = mommy.make("crm.Customer")
    c1 = mommy.make("crm.BillingContact", scoping_customer=m1)
    c2 = mommy.make("crm.ImplementationContact")

    contacts = contacts_svc.get_contacts(
        scoping_customer=m1)
    assert c1 in contacts
    assert not c2 in contacts


def test_get_contacts__missing_scoping_customer():
    """Test getting contacts without a managing customer"""
    with pytest.raises(Exception):
        contacts_svc.get_contacts()


@pytest.mark.django_db
def test_create_customer_contact_data_type():
    """Test Contact Creation"""
    customer = mommy.make("crm.Customer")
    managing_customer = mommy.make("crm.Customer", scoping_customer=customer)
    consuming_customer = mommy.make("crm.Customer", scoping_customer=customer)
    contact_data = {
        "type": "noc",
        "email": "email@addr.ess",
        "telephone": "123456",
        "managing_customer": managing_customer.pk,
        "consuming_customer": consuming_customer.pk,
    }

    # Assume contact_data is validated.
    contact = contacts_svc.create_contact(
        scoping_customer=customer,
        contact_input=contact_data)
    assert contact.pk, \
        "Contact should have an assigned primary key"
    assert isinstance(contact, NocContact)


@pytest.mark.django_db
def test_create_customer_contact_fixed_type():
    """Test Contact Creation"""
    customer = mommy.make("crm.Customer")
    managing_customer = mommy.make("crm.Customer", scoping_customer=customer)
    consuming_customer = mommy.make("crm.Customer", scoping_customer=customer)
    contact_data = {
        "email": "email@addr.ess",
        "telephone": "123456",
        "managing_customer": managing_customer,
        "consuming_customer": consuming_customer,
    }

    contact = contacts_svc.create_contact(
        scoping_customer=customer,
        contact_input=contact_data,
        contact_type="noc",
    )

    assert contact.pk, \
        "Contact should have an assigned primary key"
    assert isinstance(contact, NocContact)


@pytest.mark.django_db
def test_update_contact():
    """Test updating a contact"""
    customer = mommy.make("crm.Customer")
    managing_customer = mommy.make("crm.Customer", scoping_customer=customer)
    consuming_customer = mommy.make("crm.Customer", scoping_customer=customer)
    contact_data = {
        "email": "email@addr.ess",
        "telephone": "123456",
        "managing_customer": managing_customer,
        "consuming_customer": consuming_customer,
    }

    contact = contacts_svc.create_contact(
        scoping_customer=customer,
        contact_input=contact_data,
        contact_type="noc")

    contact = contacts_svc.update_contact(
        scoping_customer=customer,
        contact=contact,
        contact_update={
            "telephone": "0000",
        })

    assert contact.telephone == "0000"

    # managing customer id and type should be ignored from the update
    contact = contacts_svc.update_contact(
        scoping_customer=contact.scoping_customer,
        contact=contact,
        contact_update={
            "scoping_customer_id": "F00",
            "type": "implementation",
        })

    assert contact.scoping_customer_id != "F00", \
        "Customer id should not be reassigned in update"
    assert not isinstance(contact, ImplementationContact), \
        "Contact type should be read only in updates"


@pytest.mark.django_db
def test_delete_contact():
    """Test contact removal"""
    contact = mommy.make(NocContact)

    contacts_svc.delete_contact(
        scoping_customer=contact.scoping_customer,
        contact=contact)

    with pytest.raises(NocContact.DoesNotExist):
        contact.refresh_from_db()


@pytest.mark.django_db()
def test_has_contact_refs():
    """Test has contact refs check"""
    impl_contact = mommy.make(ImplementationContact)
    billing_contact = mommy.make(BillingContact)
    # This contact is nowhere in use.
    assert not contacts_svc.has_contact_refs(impl_contact)

    # This contact is now in use by a connection
    connection = mommy.make(
        "access.Connection",
        contacts=[impl_contact, billing_contact])

    assert contacts_svc.has_contact_refs(impl_contact)


@pytest.mark.django_db()
def test_get_contact_refs():
    """Test has contact refs check"""
    impl_contact = mommy.make(ImplementationContact)
    billing_contact = mommy.make(BillingContact)
    # This contact is nowhere in use.
    assert not contacts_svc.get_contact_refs(impl_contact)

    # This contact is now in use by a connection
    connection = mommy.make(
        "access.Connection",
        contacts=[impl_contact, billing_contact])
    config = mommy.make(
        "access.ExchangeLanNetworkServiceConfig",
        contacts=[impl_contact])

    refs = contacts_svc.get_contact_refs(impl_contact)

    assert connection in refs
    assert config in refs


def test_assert_presence_of_contact_types():
    """Test required contact types validation"""
    # This should work
    required = ["noc", "implementation"]
    contacts = [
        mommy.prepare("crm.NocContact"),
        mommy.prepare("crm.ImplementationContact")]
    contacts_svc.assert_presence_of_contact_types(
        required, contacts)

    # This should, well, not.
    required = ["noc"]
    contacts = [
        mommy.prepare("crm.NocContact"),
        mommy.prepare("crm.ImplementationContact")]
    with pytest.raises(RequiredContactTypesInvalid):
        contacts_svc.assert_presence_of_contact_types(
            required, contacts)

    # And this for a different reason...
    required = ["billing", "implementation"]
    contacts = [
        mommy.prepare("crm.ImplementationContact")]
    with pytest.raises(RequiredContactTypesInvalid):
        contacts_svc.assert_presence_of_contact_types(
            required, contacts)

