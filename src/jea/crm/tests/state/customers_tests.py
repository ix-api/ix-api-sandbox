
"""
Test Customer Event Handlers
"""

import pytest
from model_mommy import mommy

from jea.eventmachine.models import (
    State,
)
from jea.crm.events import (
    CUSTOMER_CREATED,
    CONTACT_CREATED,

    customer_created,
    contact_created,
    contact_deleted,
)
from jea.crm.state import customers as customers_state


@pytest.mark.django_db
def test_update_customer_state():
    """Test state updating based on events"""
    customer = mommy.make("crm.Customer", state=State.REQUESTED)

    # Apply handler
    event = customer_created(customer)
    customers_state.update_customer_state(event)

    # Next state should be error
    customer.refresh_from_db()
    assert customer.state == State.ERROR

    # Add contacts
    contact = mommy.make("crm.NocContact", consuming_customer=customer)
    event = contact_created(contact)
    customers_state.update_customer_state(event)

    customer.refresh_from_db()
    assert customer.state == State.ERROR
    assert customer.status.count()

    legal_contact = mommy.make("crm.LegalContact", consuming_customer=customer)
    event = contact_created(legal_contact)
    customers_state.update_customer_state(event)

    # Now everything should be ok
    customer.refresh_from_db()
    assert customer.state == State.PRODUCTION
    assert not customer.status.count()

    # Let's remove the legal contact
    legal_contact.delete()
    event = contact_deleted(legal_contact)
    customers_state.update_customer_state(event)

    # Now we should be back to error
    customer.refresh_from_db()
    assert customer.state == State.ERROR
    assert customer.status.count()


