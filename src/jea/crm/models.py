
from django.db import models
from polymorphic.models import PolymorphicModel

from utils.datastructures import reverse_mapping
from jea.eventmachine.models import StatefulMixin


#
# Mixins
#

class OwnableDeprecatedMixin(models.Model):
    """
    DEPRECATED: 

    This mixin adds ownership to a model:
    Billing and owning customer references are added.

    An optional external reference
    can be used for linking to foreign datasource.
    """
    managing_customer = models.ForeignKey(
        "crm.Customer",
        related_name="managed_%(class)ss",
        on_delete=models.PROTECT)
    consuming_customer = models.ForeignKey(
        "crm.Customer",
        related_name="consumed_%(class)ss",
        on_delete=models.PROTECT)

    purchase_order = models.CharField(max_length=80)
    contract_ref = models.CharField(
        max_length=128,
        null=True,
        blank=False)
    external_ref = models.CharField(
        max_length=128,
        null=True,
        blank=False)

    class Meta:
        abstract = True


class OwnableMixin(models.Model):
    """
    This mixin adds ownership to a model:
    Managing and consuming customers are referenced.

    An optional external reference
    can be used for linking to foreign datasource.
    """
    managing_customer = models.ForeignKey(
        "crm.Customer",
        related_name="managed_%(class)ss",
        on_delete=models.PROTECT)
    consuming_customer = models.ForeignKey(
        "crm.Customer",
        related_name="consumed_%(class)ss",
        on_delete=models.PROTECT)
    external_ref = models.CharField(
        max_length=128,
        null=True,
        blank=False)

    class Meta:
        abstract = True


class ContactableMixin(models.Model):
    """
    Models with this trait have a polymorphic contacts
    attribute.
    """
    # Contacts
    contacts = models.ManyToManyField(
        "crm.Contact",
        related_name="%(class)ss")

    class Meta:
        abstract = True


class InvoiceableMixin(ContactableMixin, models.Model):
    """
    This trait marks objects as billable and
    adds required fields.

    Invoiceable objects are contactable.
    """
    purchase_order = models.CharField(max_length=80)
    contract_ref = models.CharField(
        max_length=128,
        null=True,
        blank=False)

    class Meta:
        abstract = True


class CustomerScopedMixin(models.Model):
    """
    Scoped objects can have a related `scoping_customer`.
    Permission checks should be done on this relation.

    The default permission if the `scoping_customer` is None,
    is read only. This will be the case with most "system"
    entities, like the exchange lan network service provided
    by the IX.

    Rationale: Why are we using 'None' in this case and
               not an IXP customer?
    Granting access to a shared object (like a network service)
    requires a M:N relationship. Preferrably with an explicit
    permission like "read/write".

    This still can be introduced in the future, but for now
    the simpler assumption is sufficient.
    """
    scoping_customer = models.ForeignKey(
        "crm.Customer",
        null=True,
        related_name="scoped_%(class)ss",
        on_delete=models.PROTECT)


    class Meta:
        abstract = True

#
# Models
#
class Customer(
        StatefulMixin,
        CustomerScopedMixin,
        models.Model,
    ):
    """
    A JEA Customer.
    The customer is one of the entry points to the graph.
    """
    parent = models.ForeignKey(
        "crm.Customer",
        related_name="subcustomers",
        null=True,
        on_delete=models.SET_NULL)
    name = models.CharField(max_length=80)

    external_ref = models.CharField(
        max_length=128,
        null=True,
        blank=False)

    def __str__(self):
        return "{} ({})".format(self.name, self.pk)


#
# Contacts
#
CONTACT_TYPE_LEGAL = "legal"
CONTACT_TYPE_NOC = "noc"
CONTACT_TYPE_PEERING = "peering"
CONTACT_TYPE_IMPLEMENTATION = "implementation"
CONTACT_TYPE_BILLING = "billing"


class Contact(
        OwnableDeprecatedMixin,
        CustomerScopedMixin,
        PolymorphicModel,
    ):
    """The customer contact base object."""
    def __str__(self):
        """Contact string representation"""
        class_name = self.__class__.__name__
        return f"{class_name}: BaseContact, id={self.id}"

    def __repr__(self):
        """Contact representation"""
        class_name = self.__class__.__name__
        return (f"<{class_name} id={self.id} "
                f"managed_by={self.scoping_customer_id}>")


class LegalContact(Contact):
    """
    Contact of type: Legal Contact
    """
    legal_company_name = models.CharField(max_length=80)

    email = models.CharField(max_length=80)

    address_country = models.CharField(max_length=2)
    address_locality = models.CharField(max_length=80)
    address_region = models.CharField(
        max_length=80,
        null=True,
        blank=False)
    postal_code = models.CharField(max_length=80)
    post_office_box_number = models.CharField(max_length=80,
                                              null=True,
                                              blank=False)
    street_address = models.CharField(max_length=80)

    def __str__(self):
        """Contact string representation"""
        class_name = self.__class__.__name__
        return f"{class_name}: {self.legal_company_name}, id={self.id}"

    def __repr__(self):
        """Contact representation"""
        class_name = self.__class__.__name__
        return (f"<{class_name} id={self.id} name={self.legal_company_name} "
                f"managed_by={self.scoping_customer_id}>")


class NocContact(Contact):
    """
    Contact of type: NOC
    """
    telephone = models.CharField(max_length=40, null=True)
    email = models.CharField(max_length=80)

    def __str__(self):
        """NOC Contact to string"""
        class_name = self.__class__.__name__
        return f"{class_name}: {self.email}, id={self.pk}"

    def __repr__(self):
        """NOC Contact representation"""
        class_name = self.__class__.__name__
        return (f"<{class_name} id={self.id} email={self.email} "
                f"managed_by={self.scoping_customer_id}>")


class PeeringContact(Contact):
    """
    Contact of Type: Peering
    """
    email = models.CharField(max_length=80)

    def __str__(self):
        """NOC Contact to string"""
        class_name = self.__class__.__name__
        return f"{class_name}: {self.email}, id={self.pk}"

    def __repr__(self):
        """NOC Contact representation"""
        class_name = self.__class__.__name__
        return (f"<{class_name} id={self.id} email={self.email} "
                f"managed_by={self.scoping_customer_id}>")


class ImplementationContact(Contact):
    """
    Contact of type: Implementation
    """
    name = models.CharField(max_length=80)
    telephone = models.CharField(max_length=80, null=True)
    email = models.CharField(max_length=80)
    legal_company_name = models.CharField(
        max_length=80,
        blank=False,
        null=True)

    def __str__(self):
        """Implementation Contact to string"""
        class_name = self.__class__.__name__
        return f"{class_name}: {self.name}, id={self.id}"

    def __repr__(self):
        """Implementation Contact representation"""
        class_name = self.__class__.__name__
        return (f"<{class_name} id={self.id} name={self.name} "
                f"managed_by={self.scoping_customer_id}>")


class BillingContact(Contact):
    """
    Contact of type: Billing
    """
    legal_company_name = models.CharField(max_length=80)

    address_country = models.CharField(max_length=2)
    address_locality = models.CharField(max_length=80)
    address_region = models.CharField(
        max_length=80,
        null=True,
        blank=False)
    postal_code = models.CharField(max_length=80)
    post_office_box_number = models.CharField(max_length=80,
                                              null=True,
                                              blank=False)
    street_address = models.CharField(max_length=80)
    email = models.CharField(max_length=80)
    vat_number = models.CharField(
        null=True, blank=False,
        max_length=20)

    def __str__(self):
        """Billing Contact to string"""
        class_name = self.__class__.__name__
        return f"{class_name}: {self.legal_company_name}, id={self.id}"

    def __repr__(self):
        """NOC Contact representation"""
        class_name = self.__class__.__name__
        return (f"<{class_name} id={self.id} name={self.legal_company_name} "
                f"managed_by={self.scoping_customer_id}>")


# We can now define a set of contact types with a mapping
# to the corresponding classes:
CONTACT_TYPES = {
    LegalContact: CONTACT_TYPE_LEGAL,
    NocContact: CONTACT_TYPE_NOC,
    ImplementationContact: CONTACT_TYPE_IMPLEMENTATION,
    BillingContact: CONTACT_TYPE_BILLING,
    PeeringContact: CONTACT_TYPE_PEERING,
}

INV_CONTACT_TYPES = reverse_mapping(CONTACT_TYPES)

