
"""
A service for contact handling
"""

from typing import Optional, Any, List
from copy import deepcopy

from django.core.exceptions import ValidationError

from jea.exceptions import ResourceAccessDenied
from jea.eventmachine import active
from jea.crm.events import (
    contact_created,
    contact_deleted,
)
from jea.crm.filters import (
    ContactFilter,
)
from jea.crm.exceptions import (
    ContactInUse,
    RequiredContactTypesInvalid,
)
from jea.crm.models import (
    Customer,
    Contact,
    CONTACT_TYPES,
    INV_CONTACT_TYPES,
)
from jea.crm.services import customers as customers_svc


def get_contacts(
        scoping_customer: Customer = None,
        filters: dict = None,
    ):
    """
    Get a list of filtered contacts.
    A managing customer is required.

    :param scoping_customer: The current managing customer
    :param filters: An optional dict of query filter params
    """
    if not scoping_customer:
        raise ValidationError("Not allowed without managed scope")

    contacts = scoping_customer.scoped_contacts.all()
    filtered = ContactFilter(filters, queryset=contacts)

    return filtered.qs


def get_contact(
        scoping_customer: Customer = None,
        contact: Any = None,
    ) -> Contact:
    """
    Get a customer contact identified by the primary key.

    :param scoping_customer: The managing customer.
    :param pk: The contact's primary key

    :raises: Contact.DoesNotExist
    :raises: ResourceAccessDenied
    """
    # Resolve contact
    if not isinstance(contact, Contact):
        contact = Contact.objects.get(pk=contact)

    # Permission check
    if scoping_customer:
        if contact.scoping_customer_id != scoping_customer.pk:
            raise ResourceAccessDenied(contact)

    return contact


@active.command
def create_contact(
        dispatch,
        scoping_customer=None,
        contact_input=None,
        contact_type=None,
    ) -> Contact:
    """
    Create a customer contact

    :param customer: A jea customer instance
    :param contact_type: The contact type to derive the contact class from
    :param contact_input: Contact data. *MUST* be validated.
    """
    if not contact_input:
        raise ValidationError("contact data is missing")

    data = deepcopy(contact_input) # Prevent mutations

    # Permission check on referenced customers
    data["managing_customer"] = customers_svc.get_customer(
        scoping_customer=scoping_customer,
        customer=data["managing_customer"])
    data["consuming_customer"] = customers_svc.get_customer(
        scoping_customer=scoping_customer,
        customer=data["consuming_customer"])

    # Get contact type
    if not contact_type and data.get("type"):
        contact_type = data["type"]

    if not contact_type:
        raise ValidationError("a contact type is required")

    # Assert contact is managed by the current customer
    if data.get("scoping_customer"):
        del data["scoping_customer"]
    data["scoping_customer_id"] = scoping_customer.pk

    if data.get("type"):
        del data["type"]

    # Instanciate contact
    contact_class = INV_CONTACT_TYPES[contact_type]
    contact = contact_class(**data)
    contact.save()

    dispatch(contact_created(contact))

    return contact


@active.command
def update_contact(
        dispatch,
        scoping_customer: Customer = None,
        contact: Contact = None,
        contact_update = None,
    ):
    """
    Update a customer contact.

    :param contact: The contact to update
    :param contact_update: The validated! update data
    """
    update = deepcopy(contact_update) # Prevent mutations
    contact = get_contact(
        scoping_customer=scoping_customer,
        contact=contact) # Apply permission checks

    # Check permissions on customers relations
    if update.get("managing_customer"):
        update["managing_customer"] = customers_svc.get_customer(
            scoping_customer=scoping_customer,
            customer=update["managing_customer"])
    if update.get("consuming_customer"):
        update["consuming_customer"] = customers_svc.get_customer(
            scoping_customer=scoping_customer,
            customer=update["consuming_customer"])

    # Clean data
    if update.get("type"):
        del update["type"] # Type is already set.

    if update.get("scoping_customer_id"):
        del update["scoping_customer_id"] # Do not allow resassignments
    if update.get("scoping_customer"):
        del update["scoping_customer"]

    # Update fields
    for attr, value in update.items():
        setattr(contact, attr, value)

    contact.save()

    return contact


def has_contact_refs(
        contact=None,
        scoping_customer=None,
    ):
    """
    Check if the contact has related objects...
    """
    contact = get_contact(
        contact=contact, scoping_customer=scoping_customer)

    # We go through all related objects...
    # While this might be expensive, it is not as expensive
    # as getting all related objects.
    results = []
    for rel in contact._meta.related_objects:
        field_name = rel.get_accessor_name()
        field = getattr(contact, field_name)
        if field.exists():
            return True

    return False


def get_contact_refs(
        contact=None,
        scoping_customer=None,
    ):
    """
    Get everything the contact is referenced by.
    This is quite expensive. Only use with caution.
    """
    contact = get_contact(
        contact=contact, scoping_customer=scoping_customer)

    # We go through all related objects... this might be expensive
    results = []
    for rel in contact._meta.related_objects:
        field_name = rel.get_accessor_name()
        field = getattr(contact, field_name)
        results += list(field.all())

    return results


@active.command
def delete_contact(
        dispatch,
        scoping_customer=None,
        contact=None,
    ):
    """
    Delete contact either by providing a contact object or
    by passing the id.

    :param scoping_customer: The current managing customer scope
    :param contact: The contact to delete
    """
    contact = get_contact(
        scoping_customer=scoping_customer,
        contact=contact)

    # Check if the contact is in use
    in_use = has_contact_refs(
        contact=contact,
        scoping_customer=scoping_customer)
    if in_use:
        raise ContactInUse

    contact.delete()

    # Trigger state machine transitions
    dispatch(contact_deleted(contact))

    return contact


def assert_presence_of_contact_types(
        required_contact_types: List[str] = None,
        contacts: List[Contact] = None,
        scoping_customer=None,
    ):
    """
    Check if all required contact types and only these contact types
    are present in the list of provided polymorphic contacts.

    :param required_contact_types: A list of required contact types
    :param contact: A list of contact objects. Might be references.
    :param scoping_customer: An optional customer scope. 

    :raises RequiredContactTypesInvalid: in case a required 
        contact type is missing or not required present.
    """
    if not required_contact_types:
        required_contact_types = [] # So we don't care.

    # Load contacts and check permissions
    contacts = [get_contact(contact=c, scoping_customer=scoping_customer)
                for c in contacts]

    # We will compare sets and check if they are the same.
    required_contact_types = set(required_contact_types)
    contact_types_present = {CONTACT_TYPES[c.__class__]
                             for c in contacts}
    if required_contact_types != contact_types_present:
        raise RequiredContactTypesInvalid()

