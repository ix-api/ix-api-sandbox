
"""
JEA Api Permissions
"""

import logging
from functools import wraps

from rest_framework.permissions import BasePermission

from jea.auth import roles
from jea.auth.exceptions import (
    AuthorizationError,
    AuthorizationIncompleteError,
)
from jea.api.v1.auth.users import ApiUser


logger = logging.getLogger(__name__)


#
# TODO: make elegant.
#

class HasAccessRole(BasePermission):
    """
    Check if an accessing ApiUser has the 'access' role.
    """
    def has_permission(self, request, view):
        """Test if role is assigned"""
        if not isinstance(request.user, ApiUser):
            return False

        logger.debug(
            "checking for role '%s' in '%s'",
            roles.ACCESS,
            request.user.access_roles,
        )

        return roles.ACCESS in request.user.access_roles


class HasTokenRefreshRole(BasePermission):
    """
    Check if an accessing ApiUser has the 'token_refresh' role.
    """
    def has_permission(self, request, view):
        """Test if role is assigned"""
        if not isinstance(request.user, ApiUser):
            return False

        logger.debug(
            "checking for role '%s' in '%s'",
            roles.TOKEN_REFRESH,
            request.user.access_roles,
        )

        return roles.TOKEN_REFRESH in request.user.access_roles


def require_customer(view):
    """
    The view requires a customer set in the request.

    If a customer is not available in the request,
    raise a permission denied access error.

    :param view: a django view
    """
    @wraps(view)
    def _wrapper(self, request, *args, **kwargs):
        """Get customer and root customer"""
        if not isinstance(request.user, ApiUser):
            raise AuthorizationError

        if not request.user.customer:
            raise AuthorizationIncompleteError

        # Add customer to kwargs
        kwargs["customer"] = request.user.customer

        return view(self, request, *args, **kwargs)

    return _wrapper

