
"""
API User

The api user is a crm.Customer centric authentication user
to be used within all API requests.
"""
from typing import List

from django.db.models.manager import EmptyManager
from django.contrib.auth.models import (
    Group,
    Permission,
    _user_get_all_permissions,
    _user_has_perm,
    _user_has_module_perms,
)

from jea.crm.models import Customer


class ApiUser:
    """
    The ApiUser is quite similar to the AnonymousUser, so most of
    the code is taken from `django.contrib.auth.models.AnonymousUser`.
    However, it does not share it's singleton property.

    We discard the frameworks User centric approach, because
    the users are only objects to bind API keys to.

    The view to the API is entierly customer centric.
    """
    id = None
    pk = None

    # Information from the JWT
    customer = None
    customer_id = None

    access_roles: List[str] = []

    username = ''

    is_staff = False
    is_active = True

    is_superuser = False

    _groups = EmptyManager(Group)
    _user_permissions = EmptyManager(Permission)


    def __init__(
            self,
            customer: Customer,
            token=None,
            roles=None,
        ):
        """
        Initialize the ApiUser with a customer and the
        api entrypoint customer.
        """
        # Get token information
        if token:
            self.access_roles = token.get("roles", [])
        if roles:
            self.access_roles = roles # provide override

        # Set customer
        self.customer = customer
        if customer:
            self.customer_id = customer.id

        # Derive a username
        if self.customer:
            self.username = "{}".format(self.customer.name)
        else:
            self.username = "AnonymousCustomer"

    def __str__(self):
        return self.username

    def save(self):
        raise NotImplementedError("Django doesn't provide a DB representation for AnonymousUser.")

    def delete(self):
        raise NotImplementedError("Django doesn't provide a DB representation for AnonymousUser.")

    def set_password(self, raw_password):
        raise NotImplementedError("Django doesn't provide a DB representation for AnonymousUser.")

    def check_password(self, raw_password):
        raise NotImplementedError("Django doesn't provide a DB representation for AnonymousUser.")

    @property
    def groups(self):
        return self._groups

    @property
    def user_permissions(self):
        return self._user_permissions

    def get_group_permissions(self, obj=None):
        return set()

    def get_all_permissions(self, obj=None):
        return _user_get_all_permissions(self, obj=obj)

    def has_perm(self, perm, obj=None):
        return _user_has_perm(self, perm, obj=obj)

    def has_perms(self, perm_list, obj=None):
        return all(self.has_perm(perm, obj) for perm in perm_list)

    def has_module_perms(self, module):
        return _user_has_module_perms(self, module)

    @property
    def is_anonymous(self):
        return self.customer == None

    @property
    def is_authenticated(self):
        return True

    def get_username(self):
        return self.username
