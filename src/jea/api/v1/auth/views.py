
"""
JEA API Authentication

Get your access and refresh tokens here.
"""

import logging

from rest_framework.request import Request

from jea.auth.services import (
    token_authentication as token_authentication_svc,
)
from jea.api.v1.viewsets import  JEAViewSet
from jea.api.v1.response import ApiSuccess, ApiResponse
from jea.api.v1.auth import serializers
from jea.api.v1.auth.handlers import JWTAuthentication
from jea.api.v1.errors import problems


logger = logging.getLogger(__name__)


#
# NOTE: The auth description is currently
#       created directly within the schema generator.
#       This is because things got hacky.
#
#       See: `src/jea/api/docs/openapi/schema.py`
#


class TokenViewSet(JEAViewSet):
    """
    Open an authorized session with the IX-API by
    sending an `api_key` and `api_secret` pair to the
    `/api/v1/tokens` resource.

    This is equivalent to a 'login' endpoint.

    Upon success, you will receive an `access_token` and
    a `refreh_token`. The refresh token can be used for
    regenerating the lifetime limited access token.

    Use the `access_token` as `Bearer` in your `Authorziation` header
    for getting access to the API.

    The `auth_token` (and `refresh_token`) have limited lifetimes.
    As both are JWTs, you can directy get this information from them.

    When the session (`access_token`) expires, you can (within
    the lifetime of the `refresh_token`) reauthenticate by
    providing only the `refresh_token`.

    Make a `POST` request to the `/api/v1/auth/refresh` resource.
    """
    authentication_classes = ()
    permission_classes = ()

    responses = {
        "create": {
            "request": serializers.AuthTokenRequestSerializer(),
            "problems": [
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
                problems.NotAuthenticatedProblem(),
                problems.ValidationErrorProblem(),
            ],
            200: serializers.AuthTokensSerializer(),
        }
    }

    def create(self, request: Request) -> ApiResponse:
        """
        Authenticate a customer identified by `api_key` and `api_secret`.
        """
        serializer = serializers.AuthTokenRequestSerializer(
            data=request.data)

        # Validate create token request
        serializer.is_valid(raise_exception=True)
        valid_request = serializer.validated_data

        logger.debug("Authenticating token request: %s", valid_request)

        # Authenticate via api key
        access_token, refresh_token = \
            token_authentication_svc.authenticate_api_key(
                valid_request["api_key"], valid_request["api_secret"])

        tokens_serializer = serializers.AuthTokensSerializer({
            "access_token": access_token,
            "refresh_token": refresh_token,
        })

        return ApiSuccess(tokens_serializer.data)


class RefreshTokenViewSet(JEAViewSet):
    """
    The `auth_token` (and `refresh_token`) have limited lifetimes.
    As both are JWTs, you can directy get this information from them.

    When the session (`access_token`) expires, you can (within
    the lifetime of the `refresh_token`) reauthenticate by
    providing only the `refresh_token`.
    """
    authentication_classes = ()
    permission_classes = ()

    responses = {
        "create": {
            "request": serializers.RefreshTokenRequestSerializer(),
            "problems": [
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
                problems.NotAuthenticatedProblem(),
                problems.ValidationErrorProblem(),
            ],
            200: serializers.AuthTokensSerializer(),
        }
    }

    def create(self, request: Request) -> ApiResponse:
        """
        Reauthenticate the API user, issue a new `access_token`
        and `refresh_token` pair by providing the `refresh_token`
        in the request body.
        """
        serializer = serializers.RefreshTokenRequestSerializer(
            data=request.data)

        # Validate refresh token request
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        # Issue new tokens
        access_token, refresh_token = \
            token_authentication_svc.refresh(validated_data["refresh_token"])

        tokens_serializer = serializers.AuthTokensSerializer({
            "access_token": access_token,
            "refresh_token": refresh_token,
        })

        return ApiSuccess(tokens_serializer.data)
