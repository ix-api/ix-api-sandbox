import logging

from django.apps import AppConfig

logger = logging.getLogger(__name__)


class ApiConfig(AppConfig):
    name = 'jea.api.v1'

    def ready(self):
        """Application ready"""
        logger.info("Initializing app: {}".format(self.name))


