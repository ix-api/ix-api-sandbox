
"""
Service :: Views

Implements service related endpoints.
In our case services are all network services.
"""

from rest_framework import exceptions, status

from jea.api.v1.errors import problems
from jea.api.v1.permissions import require_customer
from jea.api.v1.viewsets import JEAViewSet
from jea.api.v1.response import ApiSuccess, ApiError
from jea.api.v1.service.serializers import (
    NetworkServiceSerializer,
    NetworkFeatureSerializer,
)
from jea.service.filters import (
    NetworkServiceFilter,
    NetworkFeatureFilter,
)
from jea.service.services import (
    network as network_svc,
)


class NetworkServicesViewSet(JEAViewSet):
    """
    A `NetworkService` is an instances of a `Product` accessible by one
    or multiple users, depending on the type of product

    For example, each Exchange Network LAN is considered as a shared
    instance of the related LAN's `Product`
    """
    serializer_class = NetworkServiceSerializer
    filterset_class = NetworkServiceFilter

    responses = {
        "list": {
            200: NetworkServiceSerializer(many=True),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
            ],
        },
        "read": {
            200: NetworkServiceSerializer(),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
                problems.NotFoundProblem(),
            ],
        }
    }

    @require_customer
    def list(self, request, customer=None):
        """List available `network-services`."""
        network_services = network_svc.get_network_services(
            scoping_customer=customer,
            filters=request.query_params)
        serializer = NetworkServiceSerializer(network_services, many=True)

        return serializer.data

    @require_customer
    def retrieve(self, request, customer=None, pk=None):
        """Get a specific `network-service` by id"""
        network_service = network_svc.get_network_service(
            network_service=pk)
        serializer = NetworkServiceSerializer(network_service)

        return serializer.data


class NetworkFeaturesViewSet(JEAViewSet):
    """
    `NetworkFeatures` are functionality made available to customers
    within a `NetworkService`.
    Certain features may need to be configured by a customer to use that
    service.

    This can be for example a `route server` on an `exchange lan`.

    Some of these features are mandatory to configure if you
    want to access the platform. Which of these features you have to
    configure you can query using: `/api/v1/network-features?required=true`
    """
    serializer_class = NetworkFeatureSerializer
    filterset_class = NetworkFeatureFilter

    responses = {
        "list": {
            200: NetworkFeatureSerializer(many=True),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
            ],
        },
        "read": {
            200: NetworkFeatureSerializer(),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
                problems.NotFoundProblem(),
            ],
        }
    }

    @require_customer
    def list(self, request, customer=None):
        """List available network features."""
        features = network_svc.get_network_features(
            scoping_customer=customer,
            filters=request.query_params)
        serializer = NetworkFeatureSerializer(features, many=True)

        return serializer.data

    @require_customer
    def retrieve(self, request, customer=None, pk=None):
        """Get a single network feature by it's id"""
        feature = network_svc.get_network_feature(
            scoping_customer=customer,
            network_feature=pk)
        serializer = NetworkFeatureSerializer(feature)

        return serializer.data

