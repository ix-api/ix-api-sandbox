
"""
Services Serializers

Seralizers for network services and features.
"""

from rest_framework import serializers

from jea.api.v1.serializers import (
    JeaPrimaryKeyRelatedField,
    EnumField,
    PolymorphicSerializer,
)
from jea.api.v1.catalog.serializers import (
    ProductSerializer,
)
from jea.api.v1.crm.serializers import (
    CustomerSerializer,
    OwnableObjectSerializer,
    InvoiceableObjectSerializer,
)
from jea.access.models import BGPSessionType, RouteServerSessionMode
from jea.ipam.models import (
    AddressFamilies,
)
from jea.catalog.models import (
    Product,
)
from jea.crm.models import (
    Customer,
)
from jea.service import models as service_models
from jea.service.models import (
    SERVICE_TYPES,
    FEATURE_TYPES,
)


class IXPSpecificFeatureFlagSerializer(serializers.Serializer):
    """IXP-Specific Feature Flag"""
    name = serializers.CharField(
        max_length=20,
        help_text="""
            The name of the feature flag.

            Example: RPKI
        """)
    description = serializers.CharField(
        max_length=80,
        help_text="""
            The description of the feature flag.

            Example: RPKI Hard Filtering is available
        """)


class NetworkFeatureBaseSerializer(
        serializers.Serializer,
    ):
    """Feature Base"""
    id = serializers.CharField(read_only=True)

    name = serializers.CharField(max_length=80)
    required = serializers.BooleanField()

    required_contact_types = serializers.ListField(
        child=serializers.CharField(),
        allow_empty=True,
        help_text="""
            The configuration will require at least one of each of the
            specified contact types.

            They can be assigned in the `contacts` list property of the
            network feature configuration.
        """)

    flags = IXPSpecificFeatureFlagSerializer(
        source="ixp_specific_flags",
        many=True,
        help_text="""
            A list of IXP specific feature flags. This can be used
            to see if e.g. RPKI hard filtering is available.
        """)

    network_service = JeaPrimaryKeyRelatedField(
        queryset=service_models.NetworkService.objects.all())


class BlackholingNetworkFeatureSerializer(NetworkFeatureBaseSerializer):
    """Blackholing Network Feature"""
    # Includes only base fields
    __polymorphic_type__ = \
        service_models.FEATURE_TYPE_BLACKHOLING
    __hidden_in_docs__ = True


class RouteServerNetworkFeatureSerializer(NetworkFeatureBaseSerializer):
    """Route Server Network Feature"""
    asn = serializers.IntegerField(min_value=0)
    fqdn = serializers.CharField(
        max_length=80,
        help_text="""
            The FQDN of the route server.

            Example: rs1.moon-ix.net
        """)
    looking_glass_url = serializers.CharField(
        help_text="""
            The url of the looking glass.

            Example: https://lg.moon-ix.net/rs1
        """)

    address_families = serializers.ListField(
        child=EnumField(AddressFamilies),
        min_length=1,
        max_length=2,
        help_text="""
            When creating a route server feature config, remember
            to specify which address family or families to use.

            Example: ["af_inet"]
        """)

    session_mode = EnumField(
        RouteServerSessionMode,
        help_text="""
            When creating a route server feature config, remember
            to specify the same session_mode as the route server.

            Example: "public"
        """)

    available_bgp_session_types = serializers.ListField(
        child=EnumField(BGPSessionType),
        min_length=1,
        max_length=2,
        help_text="""
            The route server provides the following session modes.

            Example: ["passive"]
        """)

    ips = JeaPrimaryKeyRelatedField(
        source="ip_addresses",
        many=True,
        read_only=True)

    __polymorphic_type__ = \
        service_models.FEATURE_TYPE_ROUTESERVER


class IXPRouterNetworkFeatureSerializer(NetworkFeatureBaseSerializer):
    """IXP Router Network Feature"""
    asn = serializers.IntegerField(min_value=0)
    fqdn = serializers.CharField(max_length=80)

    ips = JeaPrimaryKeyRelatedField(
        source="ip_addresses",
        read_only=True,
        many=True)

    __polymorphic_type__ = \
        service_models.FEATURE_TYPE_IXPROUTER
    __hidden_in_docs__ = True



class NetworkFeatureSerializer(PolymorphicSerializer):
    """Polymorphic Network Feature"""
    serializer_classes = {
        service_models.FEATURE_TYPE_BLACKHOLING:
            BlackholingNetworkFeatureSerializer,
        service_models.FEATURE_TYPE_ROUTESERVER:
            RouteServerNetworkFeatureSerializer,
        service_models.FEATURE_TYPE_IXPROUTER:
            IXPRouterNetworkFeatureSerializer,
    }

    entity_types = FEATURE_TYPES


class NetworkServiceBaseSerializer(
        OwnableObjectSerializer,
        InvoiceableObjectSerializer,
        serializers.Serializer,
    ):
    """Network Service (Base)"""
    id = serializers.CharField(read_only=True)

    product = JeaPrimaryKeyRelatedField(
        queryset=Product.objects.all())

    required_contact_types = serializers.ListField(
        child=serializers.CharField(),
        allow_empty=True,
        help_text="""
            The configuration will require at least one of each of the
            specified contact types.
            They can be assigned in the `contacts` list property of the
            config.
        """)

    network_features = JeaPrimaryKeyRelatedField(
        many=True, read_only=True)


class ExchangeLanNetworkServiceSerializer(NetworkServiceBaseSerializer):
    """Exchange Lan Network Service"""
    name = serializers.CharField(
        max_length=40,
        help_text="""
            Exchange dependend service name, will be shown on the invoice.
        """)
    metro_area = serializers.CharField(
        max_length=3,
        help_text="""
            3 Letter (IATA) Airport Code of the MetroArea where 
            the exchange lan network service is available.

            Example: FRA
        """)
    peeringdb_ixid = serializers.IntegerField(
        allow_null=True,
        required=False,
        help_text="""
            PeeringDB ixid
        """)
    ixfdb_ixid = serializers.IntegerField(
        allow_null=True,
        required=False,
        help_text="""
            id of ixfdb
        """)

    ips = JeaPrimaryKeyRelatedField(
        source="ip_addresses",
        read_only=True, many=True)

    __polymorphic_type__ = \
        service_models.SERVICE_TYPE_EXCHANGE_LAN


class ClosedUserGroupNetworkServiceSerializer(NetworkServiceBaseSerializer):
    """Closed User Group Network Service"""
    # Well. D'oh.
    __polymorphic_type__ = \
        service_models.SERVICE_TYPE_CLOSED_USER_GROUP
    __hidden_in_docs__ = True


class ELineNetworkServiceSerializer(NetworkServiceBaseSerializer):
    """E-Line Network Service"""
    # Well. D'oh. Does not get any better here.
    __polymorphic_type__ = \
        service_models.SERVICE_TYPE_ELINE
    __hidden_in_docs__ = True


class CloudNetworkServiceSerializer(NetworkServiceBaseSerializer):
    """Cloud Network Service"""
    # We have to update this according to the usecase...
    cloud_provider_name = serializers.CharField()

    __polymorphic_type__ = \
        service_models.SERVICE_TYPE_CLOUD
    __hidden_in_docs__ = True


class NetworkServiceSerializer(PolymorphicSerializer):
    """Polymorphic Network Services"""
    serializer_classes = {
        service_models.SERVICE_TYPE_ELINE:
            ELineNetworkServiceSerializer,
        service_models.SERVICE_TYPE_EXCHANGE_LAN:
            ExchangeLanNetworkServiceSerializer,
        service_models.SERVICE_TYPE_CLOSED_USER_GROUP:
            ClosedUserGroupNetworkServiceSerializer,
        service_models.SERVICE_TYPE_CLOUD:
            CloudNetworkServiceSerializer,
    }

    entity_types = SERVICE_TYPES

