from django.conf.urls import url, handler404
from django.urls import path, include, re_path
from rest_framework.routers import DefaultRouter, APIRootView

from jea.api.v1.views import resource_not_found
from jea.api.v1.auth.handlers import (
    JWTAuthentication,
    ApiSessionAuthentication,
)
from jea.api.v1.auth import views as auth_views
from jea.api.v1.crm import views as crm_views
from jea.api.v1.access import views as access_views
from jea.api.v1.catalog import views as catalog_views
from jea.api.v1.service import views as service_views
from jea.api.v1.ipam import views as ipam_views
from jea.api.v1.eventmachine import views as eventmachine_views


class JEASandboxView(APIRootView):
    """Joint External API"""
    authentication_classes = (
        ApiSessionAuthentication,
        JWTAuthentication,
    )


class JEARouter(DefaultRouter):
    """API Router"""
    APIRootView = JEASandboxView


# Create a router and register our viewsets with it.
router = JEARouter(trailing_slash=False)
# == Catalog:
router.register(r"facilities",
                catalog_views.FacilitiesViewSet,
                basename="facilties")

router.register(r"devices",
                catalog_views.DevicesViewSet,
                basename="devices")

router.register(r"products",
                catalog_views.ProductsViewSet,
                basename="products")

router.register(r"pops",
                catalog_views.PointsOfPresenceViewSet,
                basename="pops")

# == Services:
router.register(r"network-services",
                service_views.NetworkServicesViewSet,
                basename="network-services")

router.register(r"network-features",
                service_views.NetworkFeaturesViewSet,
                basename="network-features")

router.register(r"network-service-configs",
                access_views.NetworkServiceConfigsViewSet,
                basename="service-configs")

router.register(r"network-feature-configs",
                access_views.NetworkFeatureConfigsViewSet,
                basename="feature-configs")

# == Access:
router.register(r"demarcs",
                access_views.DemarcsViewSet,
                basename="demarcs")

router.register(r"connections",
                access_views.ConnectionsViewSet,
                basename="conenctions")

# == Ip and Mac addresses:
router.register(r"ips",
                ipam_views.IpAddressViewSet,
                basename="ip-addresses")

router.register(r"macs",
                ipam_views.MacAddressViewSet,
                basename="mac-addresses")

# == CRM:
router.register(r"customers",
                crm_views.CustomersViewSet,
                basename="customers")

router.register(r"contacts",
                crm_views.ContactsViewSet,
                basename="contacts")

# == Auth:
router.register(r"auth/token",
                auth_views.TokenViewSet,
                basename="auth_token")

router.register(r"auth/refresh",
                auth_views.RefreshTokenViewSet,
                basename="auth_refresh")

# == Events
router.register(r"events",
                eventmachine_views.EventsViewSet,
                basename="events")


urlpatterns = [
    path("", include(router.urls)),
    re_path("(?P<path>.*)", resource_not_found),
]


