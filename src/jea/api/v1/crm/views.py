
"""
CRM :: Views

Manage customers and customer contacts.
"""

from rest_framework import exceptions, status

from jea.api.v1.errors import problems
from jea.api.v1.permissions import require_customer
from jea.api.v1.response import tags
from jea.api.v1.viewsets import JEAViewSet
from jea.api.v1.response import ApiSuccess, ApiError
from jea.api.v1.crm.serializers import (
    CustomerSerializer,
    CustomerInputSerializer,
    CustomerUpdateSerializer,
    ContactSerializer,
)
from jea.crm.services import (
    customers as customers_svc,
    contacts as contacts_svc,
)
from jea.crm.models import Customer
from jea.crm.filters import (
    CustomerFilter,
    ContactFilter,
)


class CustomersViewSet(JEAViewSet):
    """
    A `Customer` is a company using services from an IXP. The customers
    can have a hierarchy. A customer can have subcustomers. Each customer needs
    to have different contacts, depending on the exchange and the service he
    wants to use.

    For a customer to become operational, you need to provide a 
    `legal` contact.
    """

    serializer_class = CustomerSerializer
    filterset_class = CustomerFilter

    responses = {
        "read": {
            200: CustomerSerializer(),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
                problems.NotFoundProblem(),
            ],
        },
        "list": {
            200: CustomerSerializer(many=True),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
            ],
        },
        "create": {
            "request": CustomerInputSerializer(),
            201: CustomerSerializer(),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
                problems.ValidationErrorProblem(),
            ],
        },
        "update": {
            "request": CustomerUpdateSerializer(),
            200: CustomerSerializer(),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
                problems.NotFoundProblem(),
                problems.ValidationErrorProblem(),
            ],
        },
        "partial_update": {
            "request": CustomerUpdateSerializer(partial=True),
            200: CustomerSerializer(),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.SignatureExpiredProblem(),
                problems.AuthenticationProblem(),
                problems.NotFoundProblem(),
                problems.ValidationErrorProblem(),
            ],
        },
        "delete": {
            200: CustomerSerializer(),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
                problems.NotFoundProblem(),
            ],
        }
    }


    @require_customer
    def list(self, request, customer=None):
        """
        Retrieve a list of customers.

        This includes all customers the current authorized customer
        is managing and the current customer itself.
        """
        customers = customers_svc.get_customers(
            scoping_customer=customer,
            filters=request.query_params)

        serializer = CustomerSerializer(customers, many=True)

        return serializer.data

    @require_customer
    def retrieve(self, request, customer=None, pk=None):
        """
        Get a single customer.
        """
        # Retrieve customer
        requested_customer = customers_svc.get_customer(
            scoping_customer=customer,
            customer=pk)
        serializer = CustomerSerializer(requested_customer)

        return serializer.data

    @require_customer
    def create(self, request, customer=None):
        """
        Create a new customer.

        Please remember that a customer may require some `contacts`
        to be created. Otherwise it will stay in an `error` state.
        """
        serializer = CustomerSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        # Check ownership of referenced parent
        # or use current requesting customer.
        parent = customers_svc.get_customer(
            scoping_customer=customer,
            customer=validated_data["parent"])
        if not parent:
            parent = customer # Use requesting customer
        validated_data["parent"] = parent

        # Create new Customer
        customer = customers_svc.create_customer(
            scoping_customer=customer,
            customer_input=validated_data)

        serializer = CustomerSerializer(customer)
        return ApiSuccess(serializer.data, status=status.HTTP_201_CREATED)

    @require_customer
    def update(self, request, customer=None, pk=None):
        """Update a customer."""
        # Load customer and deserialize request
        update_customer = customers_svc.get_customer(
            scoping_customer=customer,
            customer=pk)
        serializer = CustomerSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        update_customer = customers_svc.update_customer(
            scoping_customer=customer,
            customer=update_customer,
            customer_update=serializer.validated_data)

        serializer = CustomerSerializer(update_customer)
        return ApiSuccess(serializer.data)

    @require_customer
    def partial_update(self, request, customer=None, pk=None):
        """Update some fields of a customer."""
        # Load customer and deserialize request
        update_customer = customers_svc.get_customer(
            scoping_customer=customer,
            customer=pk)
        serializer = CustomerSerializer(
            data=request.data,
            partial=True)
        serializer.is_valid(raise_exception=True)

        update_customer = customers_svc.update_customer(
            scoping_customer=customer,
            customer=update_customer,
            customer_update=serializer.validated_data)

        serializer = CustomerSerializer(update_customer)
        return ApiSuccess(serializer.data)

    # TODO: Implement destroy customer
    #@require_customer
    #def destroy(self, request, customer=None, pk=None):
    #    """Destroy a customer."""
    #    # Use customers service to destroy the customer
    #    destroyed_customer = customers_svc.delete_customer(
    #        customer=pk,
    #        scoping_customer=customer)
    #
    #    serializer = CustomerSerializer(destroyed_customer)
    #    return ApiSuccess(serializer.data)


class ContactsViewSet(JEAViewSet):
    """
    A `Contact` is a role undertaking a specific responsibility within a
    customer, typically a department or agent of the customer company.

    These can be `implementation`,`noc`, `legal`, `peering` or `billing`.

    A contact is bound to the customer by the `consuming_customer`
    property.
    """
    filterset_class = ContactFilter

    responses = {
        "read": {
            200: ContactSerializer(),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
                problems.NotFoundProblem(),
            ],
        },
        "list": {
            200: ContactSerializer(many=True),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
            ],
        },
        "create": {
            "request": ContactSerializer(),
            201: ContactSerializer(),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
                problems.ValidationErrorProblem(),
            ],
        },
        "update": {
            "request": ContactSerializer(),
            200: ContactSerializer(),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
                problems.NotFoundProblem(),
                problems.ValidationErrorProblem(),
            ],
        },
        "partial_update": {
            "request": ContactSerializer(partial=True),
            200: ContactSerializer(),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
                problems.NotFoundProblem(),
                problems.ValidationErrorProblem(),
            ],
        },
        "delete": {
            200: ContactSerializer(),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
                problems.NotFoundProblem(),
            ],
        }
    }

    @require_customer
    def list(self, request, customer=None):
        """List available contacts managed by the authorized customer"""
        contacts = contacts_svc.get_contacts(
            scoping_customer=customer,
            filters=request.query_params)

        serializer = ContactSerializer(contacts, many=True)

        return serializer.data

    @require_customer
    def retrieve(self, request, customer=None, pk=None):
        """Get a contact by it's id"""
        contact = contacts_svc.get_contact(
            scoping_customer=customer,
            contact=pk)
        serializer = ContactSerializer(contact)

        return serializer.data

    @require_customer
    def create(self, request, customer=None):
        """
        Create a new contact.

        Please remember to set the correct `type` of the contact
        when sending the request. Available types are `noc`, `billing`,
        `legal` and `implementation`.
        """
        # Validate input
        serializer = ContactSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Everythings fine, lets save our contact
        contact = contacts_svc.create_contact(
            scoping_customer=customer,
            contact_input=serializer.validated_data)

        # And respond with our fresh contact
        serializer = ContactSerializer(contact)

        return ApiSuccess(serializer.data, status=status.HTTP_201_CREATED)

    @require_customer
    def update(self, request, customer=None, pk=None):
        """Update a contact"""
        contact = contacts_svc.get_contact(
            scoping_customer=customer,
            contact=pk)

        # Input validation
        serializer = ContactSerializer(data=request.data,
                                       entity_type=contact.__class__)

        serializer.is_valid(raise_exception=True)
        update = serializer.validated_data

        # Perform update
        contact = contacts_svc.update_contact(
            scoping_customer=customer,
            contact=contact,
            contact_update=update)
        serializer = ContactSerializer(contact)

        return serializer.data

    @require_customer
    def partial_update(self, request, customer=None, pk=None):
        """Update parts of a contact"""
        contact = contacts_svc.get_contact(
            scoping_customer=customer,
            contact=pk)

        # Input validation
        serializer = ContactSerializer(data=request.data,
                                       partial=True,
                                       entity_type=contact.__class__)

        # Validate input
        serializer.is_valid(raise_exception=True)

        # Update contact
        contact = contacts_svc.update_contact(
            scoping_customer=customer,
            contact=contact,
            contact_update=serializer.validated_data)
        serializer = ContactSerializer(contact)

        return serializer.data

    @require_customer
    def destroy(self, request, customer=None, pk=None):
        """Remove a contact"""
        contact = contacts_svc.delete_contact(
            scoping_customer=customer,
            contact=pk)

        # Serialize old data
        serializer = ContactSerializer(contact)
        return serializer.data
