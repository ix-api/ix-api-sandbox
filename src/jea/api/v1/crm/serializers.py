
from rest_framework import serializers
from rest_framework.serializers import Serializer, Field

from jea.api.v1.serializers import (
    PolymorphicSerializer,
    JeaPrimaryKeyRelatedField,
    EnumField,
)
from jea.api.v1.eventmachine.serializers import (
    StatusSerializer,
    StatefulObjectSerializer,
)
from jea.eventmachine.models import (
    State,
)
from jea.crm import models as crm_models
from jea.crm.models import (
    Customer,
    Contact,
    LegalContact,
    NocContact,
    ImplementationContact,
    BillingContact,
    PeeringContact,
    CONTACT_TYPES,
    INV_CONTACT_TYPES,
)



class OwnableObjectSerializer(serializers.Serializer):
    """Ownable"""
    managing_customer = JeaPrimaryKeyRelatedField(
        queryset=Customer.objects.all(),
        help_text="""
            The `id` of the customer responsible for managing the service via
            the API.
            Used to be `billing_customer`.

            Example: "238189294"
        """)
    consuming_customer = JeaPrimaryKeyRelatedField(
        queryset=Customer.objects.all(),
        help_text="""
            The `id` of the customer consuming a service.

            Used to be `owning_customer`.

            Example: "2381982"
        """)
    external_ref = serializers.CharField(
        default=None,
        allow_null=True,
        allow_blank=False,
        max_length=128,
        help_text="""
            Reference field, free to use for the API user.
            Example: IX:Service:23042
        """)


class ContactableObjectSerializer(serializers.Serializer):
    """Contactable"""
    contacts = JeaPrimaryKeyRelatedField(
        required=True,
        queryset=crm_models.Contact.objects.all(),
        many=True,
        help_text="""
            Polymorphic set of contacts. See the documentation
            on the specific `required_contact_types` on what
            contacts to provide.

            Example: ["c-impl:123", "c-noc:331"]
        """)


class InvoiceableObjectSerializer(
        ContactableObjectSerializer, 
        serializers.Serializer,
    ):
    """Invoiceable"""
    purchase_order = serializers.CharField(
        allow_blank=True,
        allow_null=False,
        default="",
        max_length=80,
        help_text="""
            Purchase Order ID which will be displayed on the invoice.

            Example: "Project: DC Moon"
        """)
    contract_ref = serializers.CharField(
        allow_null=True,
        allow_blank=False,
        default=None,
        max_length=128,
        help_text="""
            A reference to a contract.

            Example: "contract:31824"
        """)


#
# Customer Serializers
#

class CustomerBaseSerializer(
        Serializer,
    ):
    """Customer Base"""
    id = serializers.CharField(max_length=80, read_only=True)

    parent = JeaPrimaryKeyRelatedField(
        default=None,
        allow_null=True,
        queryset=Customer.objects.all(),
        help_text="""
            The `id` of a parent customer. Can be used for creating
            a customer hierachy.

            Example: IX:Customer:231
        """)

    name = serializers.CharField(
        max_length=80, required=True,
        help_text="""
            Name of customer, how it gets represented
            in all customer lists.

            Example: Moonpeer Inc.
        """)

    external_ref = serializers.CharField(
        max_length=80,
        default=None,
        allow_null=True,
        allow_blank=False,
        required=False,
        label="External Reference",
        help_text="""
            Reference field, free to use for the API user.
            Example: IX:Service:23042
        """)

class CustomerSerializer(
        StatefulObjectSerializer,
        CustomerBaseSerializer,
    ):
    """Customer"""


class CustomerInputSerializer(
        CustomerBaseSerializer,
    ):
    """Customer Request"""


class CustomerUpdateSerializer(
        CustomerBaseSerializer,
    ):
    """Customer Update Request"""

#
# Contacts Serializers
#
class ContactBaseSerializer(
        OwnableObjectSerializer,
        serializers.Serializer,
    ):
    """Contact Base"""
    id = serializers.CharField(max_length=80, read_only=True)


class LegalContactSerializer(ContactBaseSerializer):
    """Legal Contact Response"""
    legal_company_name = serializers.CharField(
        max_length=80,
        help_text="""
            The official name of the organization,
            e.g. the registered company name.

            Example: Example Inc.
        """)

    address_country = serializers.CharField(
        max_length=2,
        help_text="""
            ISO 3166-1 alpha-2 country code, for example DE
            example: US
        """)
    address_locality = serializers.CharField(
        max_length=40,
        help_text="""
            The locality/city. For example, Mountain View.
            example: Mountain View
        """)
    address_region = serializers.CharField(
        max_length=80,
        default=None,
        allow_null=True,
        allow_blank=False,
        help_text="""
            The region. For example, CA
            example: CA
        """)
    postal_code = serializers.CharField(
        max_length=24,
        help_text="""
            A postal code. For example, 9404
            example: 9409
        """)
    street_address = serializers.CharField(
        max_length=80,
        help_text="""
            The street address. For example, 1600 Amphitheatre Pkwy.
            example: 1600 Amphitheatre Pkwy.
        """)
    post_office_box_number = serializers.CharField(
        max_length=18,
        default=None,
        allow_null=True,
        help_text="""
            The post office box number for PO box addresses.
            example: 2335232
        """)

    email = serializers.EmailField(
        max_length=80,
        help_text="""
            The email of the legal company entity.

            Example: info@moon-peer.net
        """)

    __polymorphic_type__ = crm_models.CONTACT_TYPE_LEGAL


class NocContactSerializer(ContactBaseSerializer):
    """NOC Contact Response"""
    telephone = serializers.CharField(
        max_length=40,
        default=None,
        allow_null=True,
        help_text="""
            The telephone number in E.164 Phone Number Formatting
            example: +442071838750
        """) 
    email = serializers.EmailField(
        max_length=80,
        help_text="""
            The email of the NOC. Typically noc@<customer>

            Example: noc@moon-peer.net
        """)

    __polymorphic_type__ = crm_models.CONTACT_TYPE_NOC


class PeeringContactSerializer(ContactBaseSerializer):
    """Peering Contact Response"""
    email = serializers.EmailField(
        max_length=80,
        help_text="""
            The peering email of the customer. Typically peering@<customer>

            Example: peering@moon-peer.net
        """)

    __polymorphic_type__ = crm_models.CONTACT_TYPE_PEERING


class ImplementationContactSerializer(ContactBaseSerializer):
    """Implementation Contact Response"""
    name = serializers.CharField(
        max_length=80,
        help_text="""
            Name of implementation contact
            Example: John Dough
        """)
    telephone = serializers.CharField(
        max_length=80,
        default=None,
        allow_null=True,
        help_text="""
            The telephone number in E.164 Phone Number Formatting
            example: +442071838750
        """) 
    email = serializers.EmailField(
        max_length=80,
        help_text="""
            Email address of the implementation contact.
            Example: implementation@example.org
        """)
    legal_company_name = serializers.CharField(
        default=None,
        allow_blank=False,
        allow_null=True,
        max_length=80,
        help_text="""
            Name of the company the implentation person is
            working for, e.g. a third party company.

            Example: Moonoc Network Services LLS.
        """)

    __polymorphic_type__ = crm_models.CONTACT_TYPE_IMPLEMENTATION


class BillingContactSerializer(ContactBaseSerializer):
    """Billing Contact Response"""
    legal_company_name = serializers.CharField(
        max_length=80,
        help_text="""
            The official name of the organization,
            e.g. the registered company name.

            Example: Example Inc.
        """)

    address_country = serializers.CharField(
        max_length=2,
        help_text="""
            ISO 3166-1 alpha-2 country code, for example DE
            example: US
        """)
    address_locality = serializers.CharField(
        max_length=40,
        help_text="""
            The locality/city. For example, Mountain View.
            example: Mountain View
        """)
    address_region = serializers.CharField(
        max_length=80,
        default=None,
        allow_null=True,
        allow_blank=False,
        help_text="""
            The region. For example, CA
            example: CA
        """)
    postal_code = serializers.CharField(
        max_length=24,
        help_text="""
            A postal code. For example, 9404
            example: 9409
        """)
    street_address = serializers.CharField(
        max_length=80,
        help_text="""
            The street address. For example, 1600 Amphitheatre Pkwy.
            example: 1600 Amphitheatre Pkwy.
        """)
    post_office_box_number = serializers.CharField(
        max_length=18,
        default=None,
        allow_null=True,
        help_text="""
            The post office box number for PO box addresses.
            example: 2335232
        """)

    email = serializers.EmailField(
        max_length=80,
        help_text="""
            The email address for sending the invoice.
            example: invoice@example.org
        """)

    vat_number = serializers.CharField(
        min_length=2,
        max_length=20,
        allow_blank=False,
        allow_null=True,
        help_text="""
            Value-added tax number, required for
            european reverse charge system.

            Example: UK2300000042
        """)

    __polymorphic_type__ = crm_models.CONTACT_TYPE_BILLING


# As we now have defined our base contact type serializers,
# we can now create a polymorphic serializer:
class ContactSerializer(PolymorphicSerializer):
    """Polymorphic Contact"""
    serializer_classes = {
        crm_models.CONTACT_TYPE_LEGAL:
            LegalContactSerializer,
        crm_models.CONTACT_TYPE_NOC:
            NocContactSerializer,
        crm_models.CONTACT_TYPE_PEERING:
            PeeringContactSerializer,
        crm_models.CONTACT_TYPE_IMPLEMENTATION:
            ImplementationContactSerializer,
        crm_models.CONTACT_TYPE_BILLING:
            BillingContactSerializer,
    }

    entity_types = crm_models.CONTACT_TYPES


