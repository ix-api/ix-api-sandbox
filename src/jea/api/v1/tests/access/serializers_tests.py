
"""
Test Serializers
"""

import pytest
from model_mommy import mommy

from jea.api.v1.access.serializers import (
    ConnectionSerializer,
    ConnectionRequestSerializer,
    ConnectionUpdateSerializer,
    DemarcationPointSerializer,
    DemarcationPointRequestSerializer,
    DemarcationPointUpdateSerializer,
    NetworkServiceConfigSerializer,
    NetworkServiceConfigInputSerializer,
    NetworkServiceConfigUpdateSerializer,
    NetworkFeatureConfigSerializer,
    NetworkFeatureConfigUpdateSerializer,
    NetworkFeatureConfigInputSerializer,
    NetworkServiceConfigUpdateBaseSerializer,
    ExchangeLanNetworkServiceConfigUpdateSerializer,
    ClosedUserGroupNetworkServiceConfigUpdateSerializer,
    ELineNetworkServiceConfigUpdateSerializer,
    CloudNetworkServiceConfigUpdateSerializer,
    BlackholingNetworkFeatureConfigUpdateSerializer,
    IXPRouterNetworkFeatureConfigUpdateSerializer,
    RouteServerNetworkFeatureConfigUpdateSerializer,
    get_config_update_serializer,
)


@pytest.mark.django_db
def test_connection_serialization():
    """Test serialization of a connection"""
    conn = mommy.make("access.Connection")
    demarc = mommy.make("access.DemarcationPoint", connection=conn)

    serializer = ConnectionSerializer(conn)
    data = serializer.data

    assert data, "Serializer should serialize without crashing"
    assert str(demarc.id) in data["demarcs"]


@pytest.mark.django_db
def test_connection_request_serializer():
    """Test create connection request serializer"""
    customer = mommy.make("crm.Customer")
    contact = mommy.make("crm.ImplementationContact")

    payload = {
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
        "mode": "lag_lacp",
        "lacp_timeout": "slow",
        "contacts": [contact.pk],
    }

    serializer = ConnectionRequestSerializer(data=payload)
    serializer.is_valid()
    assert not serializer.errors


@pytest.mark.django_db
def test_connection_update_serializer():
    """Test create connection request serializer"""
    customer = mommy.make("crm.Customer")
    contact = mommy.make("crm.ImplementationContact")

    payload = {
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
        "mode": "lag_static",
        "contacts": [contact.pk],
    }

    serializer = ConnectionUpdateSerializer(data=payload)
    serializer.is_valid()
    assert not serializer.errors


@pytest.mark.django_db
def test_polymorphic_network_service_config_serializer():
    """Test Polymorphic network service config serializer"""
    config = mommy.make("access.ExchangeLanNetworkServiceConfig")
    serializer = NetworkServiceConfigSerializer(config)

    assert serializer.data
    assert serializer.data["type"] == "exchange_lan"

    config = mommy.make("access.ClosedUserGroupNetworkServiceConfig")
    serializer = NetworkServiceConfigSerializer(config)

    assert serializer.data
    assert serializer.data["type"] == "closed_user_group"

    config = mommy.make("access.ELineNetworkServiceConfig")
    serializer = NetworkServiceConfigSerializer(config)

    assert serializer.data
    assert serializer.data["type"] == "eline"

    config = mommy.make("access.CloudNetworkServiceConfig")
    serializer = NetworkServiceConfigSerializer(config)

    assert serializer.data
    assert serializer.data["type"] == "cloud"


@pytest.mark.django_db
def test_polymorphic_network_service_config_deserialization():
    """Test deserialization of network service input"""
    customer = mommy.make("crm.Customer")
    conn = mommy.make("access.Connection")
    service = mommy.make("service.ExchangeLanNetworkService")
    contact = mommy.make("crm.ImplementationContact")
    mac = mommy.make("ipam.MacAddress")

    data = {
        "type": "exchange_lan",
        "capacity": None,
        "contacts": [contact.pk],
        "connection": conn.pk,
        "network_service": service.pk,
        "asns": [2342],
        "macs": [mac.pk],
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
        "outer_vlan": 42,
    }

    serializer = NetworkServiceConfigSerializer(data=data)
    serializer.is_valid()
    assert not serializer.errors
    assert serializer.validated_data["type"] == "exchange_lan"

    data = {
        "type": "exchange_lan",
        "capacity": 1,
        "contacts": [contact.pk],
        "asns": [2342],
        "outer_vlan": [[0, 4095]],
        "connection": "",
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    }

    serializer = NetworkServiceConfigSerializer(data=data)
    assert not serializer.is_valid()


@pytest.mark.django_db
def test_demarcation_point_serialization():
    """Test serialization of a port demarc"""
    demarc = mommy.make("access.DemarcationPoint")
    serializer = DemarcationPointSerializer(demarc)

    data = serializer.data
    assert data
    assert isinstance(data["contacts"], list)


@pytest.mark.django_db
def test_demarcation_point_request_serializer():
    """Test demarcation point request serializer"""
    connection = mommy.make("access.Connection")
    customer = mommy.make("crm.Customer")
    contact = mommy.make("crm.ImplementationContact")
    pop = mommy.make("catalog.PointOfPresence")

    payload = {
        "pop": pop.pk,
        "media_type": "100GBASE-LR",
        "connection": connection.pk,
        "contacts": [contact.pk],
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    }

    serializer = DemarcationPointRequestSerializer(data=payload)
    assert serializer.is_valid()


@pytest.mark.django_db
def test_demarcation_point_update_serializer():
    """Test demarcation point update serializer"""
    connection = mommy.make("access.Connection")
    customer = mommy.make("crm.Customer")
    contact = mommy.make("crm.ImplementationContact")

    payload = {
        "connection": connection.pk,
        "contacts": [contact.pk],
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    }

    serializer = DemarcationPointUpdateSerializer(data=payload)
    assert serializer.is_valid()


@pytest.mark.django_db
def test_feature_config_serialization():
    """Test serialization of feature config objects"""
    service_config = mommy.make("access.ExchangeLanNetworkServiceConfig")
    feature = mommy.make("service.RouteServerNetworkFeature")
    feature_config = mommy.make(
        "access.RouteServerNetworkFeatureConfig",
        network_service_config=service_config,
        network_feature=feature)
    serializer = NetworkFeatureConfigSerializer(feature_config)
    assert serializer.data, "Serializer should serialize without crashing"
    assert serializer.data["type"] == "route_server"

    feature = mommy.make("service.IXPRouterNetworkFeature")
    feature_config = mommy.make(
        "access.IXPRouterNetworkFeatureConfig",
        network_service_config=service_config,
        network_feature=feature)
    serializer = NetworkFeatureConfigSerializer(feature_config)
    assert serializer.data, "Serializer should serialize without crashing"
    assert serializer.data["type"] == "ixp_router"

    feature = mommy.make("service.BlackholingNetworkFeature")
    feature_config = mommy.make(
        "access.BlackholingNetworkFeatureConfig",
        network_service_config=service_config,
        network_feature=feature)
    serializer = NetworkFeatureConfigSerializer(feature_config)
    assert serializer.data, "Serializer should serialize without crashing"
    assert serializer.data["type"] == "blackholing"


@pytest.mark.django_db
def test_network_service_config_udpate_base_serializer():
    """Test service config update base serializer"""
    customer = mommy.make("crm.Customer")
    impl = mommy.make("crm.ImplementationContact")
    connection = mommy.make("access.Connection")

    serializer = NetworkServiceConfigUpdateBaseSerializer(data={
        "contacts": [impl.pk],
        "connection": connection.pk,
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_exchange_lan_network_service_config_update_serializer():
    """Test update serializer for network service config"""
    customer = mommy.make("crm.Customer")
    impl = mommy.make("crm.ImplementationContact")
    connection = mommy.make("access.Connection")
    mac = mommy.make("ipam.MacAddress")

    serializer = ExchangeLanNetworkServiceConfigUpdateSerializer(data={
        "contacts": [impl.pk],
        "connection": connection.pk,
        "macs": [mac.pk],
        "asns": [42],
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_closed_user_group_config_update_serializer():
    """Test update serializer for network, service config"""
    customer = mommy.make("crm.Customer")
    impl = mommy.make("crm.ImplementationContact")
    connection = mommy.make("access.Connection")
    mac = mommy.make("ipam.MacAddress")

    serializer = ClosedUserGroupNetworkServiceConfigUpdateSerializer(data={
        "contacts": [impl.pk],
        "connection": connection.pk,
        "macs": [mac.pk],
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_eline_network_service_config_update_serializer():
    """Test update serializer for network service config"""
    customer = mommy.make("crm.Customer")
    impl = mommy.make("crm.ImplementationContact")
    connection = mommy.make("access.Connection")

    serializer = ELineNetworkServiceConfigUpdateSerializer(data={
        "contacts": [impl.pk],
        "connection": connection.pk,
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    })


    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_cloud_network_service_config_update_serializer():
    """Test update serializer for network service config"""
    customer = mommy.make("crm.Customer")
    impl = mommy.make("crm.ImplementationContact")
    connection = mommy.make("access.Connection")

    serializer = CloudNetworkServiceConfigUpdateSerializer(data={
        "contacts": [impl.pk],
        "connection": connection.pk,
        "cloud_key": "f000000",
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_blackholing_network_feature_config_update_serializer():
    """Test update serializer for feature config: blackholing"""
    customer = mommy.make("crm.Customer")
    prefix = mommy.make("ipam.IpAddress")
    serializer = BlackholingNetworkFeatureConfigUpdateSerializer(data={
        "purchase_order": "fooooooo",
        "filtered_prefixes": [prefix.pk],
        "ixp_specific_configuration": "DELETE FROM `peers` WHERE 1",
        "activated": True,
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_route_server_network_feature_config_update_serializer():
    """Test update serializer for route server feature configs"""
    customer = mommy.make("crm.Customer")
    serializer = RouteServerNetworkFeatureConfigUpdateSerializer(data={
        "asn": 2342,
        "as_set": "AS-FOO",
        "max_prefix_v4": 5000,
        "insert_ixp_asn": True,
        "session_mode": "public",
        "bgp_session_type": "active",
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_ixp_router_network_feature_config_update_serializer():
    """Test update serializer for ixp router feature config"""
    customer = mommy.make("crm.Customer")
    serializer = IXPRouterNetworkFeatureConfigUpdateSerializer(data={
        "max_prefix": 23,
        "bgp_session_type": "active",
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_get_config_update_serializer():
    """Get serializer based on config type"""
    # Network Services
    #  - Exchange Lan
    config = mommy.make("access.ExchangeLanNetworkServiceConfig")
    assert get_config_update_serializer(config) == \
        ExchangeLanNetworkServiceConfigUpdateSerializer

    #  - Closed User Group
    config = mommy.make("access.ClosedUserGroupNetworkServiceConfig")
    assert get_config_update_serializer(config) == \
        ClosedUserGroupNetworkServiceConfigUpdateSerializer

    #  - ELine
    config = mommy.make("access.ELineNetworkServiceConfig")
    assert get_config_update_serializer(config) == \
        ELineNetworkServiceConfigUpdateSerializer

    #  - Cloud
    config = mommy.make("access.CloudNetworkServiceConfig")
    assert get_config_update_serializer(config) == \
        CloudNetworkServiceConfigUpdateSerializer

    # Network Features
    #  - Blackholing
    config = mommy.make("access.BlackholingNetworkFeatureConfig")
    assert get_config_update_serializer(config) == \
        BlackholingNetworkFeatureConfigUpdateSerializer

    #  - Route Server
    config = mommy.make("access.RouteServerNetworkFeatureConfig")
    assert get_config_update_serializer(config) == \
        RouteServerNetworkFeatureConfigUpdateSerializer

    #  - IXP Router
    config = mommy.make("access.IXPRouterNetworkFeatureConfig")
    assert get_config_update_serializer(config) == \
        IXPRouterNetworkFeatureConfigUpdateSerializer

    with pytest.raises(TypeError):
        config = mommy.make("access.NetworkServiceConfig")
        get_config_update_serializer(config)


