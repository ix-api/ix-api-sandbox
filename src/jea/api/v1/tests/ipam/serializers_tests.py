
"""
Test IPAM serializers
"""

import pytest
from model_mommy import mommy

from jea.ipam import models as ipam_models
from jea.api.v1.ipam.serializers import (
    InlineIpAddressSerializer,
    IpVersionField,
    IpAddressSerializer,
    MacAddressSerializer,
)

def test_inline_ip_address_serializer():
    """Test inline IP Address serializer"""
    ip_address = mommy.prepare("ipam.IpAddress")
    serializer = InlineIpAddressSerializer(ip_address)

    result = serializer.data
    assert result, "Serializer should serialize without crashing"


def test_ip_version_field__to_representation():
    """Test IP version field"""
    field = IpVersionField()
    version = ipam_models.IpVersion(4)
    result = field.to_representation(version)
    assert isinstance(result, int)
    assert result == 4

    version = ipam_models.IpVersion(6)
    result = field.to_representation(version)
    assert isinstance(result, int)
    assert result == 6


def test_ip_version_field__to_internal_value():
    """Test IP version to internal value"""
    field = IpVersionField()
    result = field.to_internal_value(4)
    assert result == ipam_models.IpVersion(4)

    result = field.to_internal_value(6)
    assert result == ipam_models.IpVersion(6)


def test_ip_address_serializer():
    """Test ip Address serializer"""
    ip_address = mommy.prepare("ipam.IpAddress")
    serializer = IpAddressSerializer(ip_address)

    result = serializer.data
    assert result, "Serializer should serialize without crashing"


def test_mac_address_serializer():
    """Test mac aaddress serializer"""
    mac_address = mommy.prepare("ipam.MacAddress")
    serializer = MacAddressSerializer(mac_address)

    result = serializer.data
    assert result, "Serializer should serialize without crashing"


