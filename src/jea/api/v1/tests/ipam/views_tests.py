
"""
Tests for IPAM views
"""

import pytest
from model_mommy import mommy

from jea.api.v1.tests import authorized_requests
from jea.api.v1.ipam import views


@pytest.mark.django_db
def test_ip_addresses_view_set__list():
    """Test listing all ip addresss"""
    customer = mommy.make("crm.Customer")

    view = views.IpAddressViewSet.as_view({"get": "list"})
    request = authorized_requests.get(customer)
    response = view(request)

    assert response.status_code == 200


@pytest.mark.django_db
def test_ip_addresses_view_set__retrieve():
    """Test getting a specific ip address"""
    customer = mommy.make(
        "crm.Customer")
    ip_addr = mommy.make(
        "ipam.IpAddress",
        scoping_customer=customer)
    view = views.IpAddressViewSet.as_view({"get": "retrieve"})
    request = authorized_requests.get(customer)
    response = view(request)

    assert response.status_code == 200


@pytest.mark.django_db
def test_ip_addresses_view_set__update():
    """Test updating properties of the ip"""
    customer = mommy.make(
        "crm.Customer")
    ip_addr = mommy.make(
        "ipam.IpAddress",
        managing_customer=customer,
        consuming_customer=customer,
        scoping_customer=customer)
    view = views.IpAddressViewSet.as_view({"put": "update"})
    request = authorized_requests.put(customer, {
        "fqdn": "foo.bar",
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    })
    response = view(request, pk=ip_addr.pk)

    assert response.status_code == 200


@pytest.mark.django_db
def test_ip_addresses_view_set__partial_update():
    """Test partially updating properties of the ip"""
    customer = mommy.make(
        "crm.Customer")
    ip_addr = mommy.make(
        "ipam.IpAddress",
        scoping_customer=customer)
    view = views.IpAddressViewSet.as_view({"patch": "partial_update"})
    request = authorized_requests.patch(customer, {
        "fqdn": "foo.bar",
    })
    response = view(request, pk=ip_addr.pk)

    assert response.status_code == 200


@pytest.mark.django_db
def test_mac_addresses_view_set__list():
    """Test listing all the mac addresses"""
    customer = mommy.make("crm.Customer")

    view = views.MacAddressViewSet.as_view({"get": "list"})
    request = authorized_requests.get(customer)
    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_mac_addresses_view_set__retrieve():
    """Test getting a specific mac address"""
    customer = mommy.make("crm.Customer")
    mac = mommy.make(
        "ipam.MacAddress",
        managing_customer=customer,
        consuming_customer=customer,
        scoping_customer=customer)

    view = views.MacAddressViewSet.as_view({"get": "retrieve"})
    request = authorized_requests.get(customer)
    response = view(request, pk=mac.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_mac_address_view_set__create():
    """Test creating a mac address"""
    customer = mommy.make("crm.Customer")
    view = views.MacAddressViewSet.as_view({"post": "create"})
    request = authorized_requests.post(customer, {
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
        "address": "ff:00:ff:00:ff:00",
    })
    response = view(request)
    assert response.status_code == 201


@pytest.mark.django_db
def test_mac_address_view_set__destroy():
    """Test removing a mac"""
    customer = mommy.make(
        "crm.Customer")
    mac = mommy.make(
        "ipam.MacAddress",
        scoping_customer=customer)
    view = views.MacAddressViewSet.as_view({"delete": "destroy"})
    request = authorized_requests.delete(customer)
    response = view(request, pk=mac.pk)
    assert response.status_code == 200
