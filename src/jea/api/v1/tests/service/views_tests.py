
"""
Test NetworkService views
"""

import pytest
from model_mommy import mommy

from jea.api.v1.tests import authorized_requests
from jea.api.v1.service import views


@pytest.mark.django_db
def test_network_service_view_set__list():
    """List all the network services"""
    customer = mommy.make("crm.Customer")
    view = views.NetworkServicesViewSet.as_view({"get": "list"})
    request = authorized_requests.get(customer)
    response = view(request)

    assert response.status_code == 200


@pytest.mark.django_db
def test_network_service_view_set__retrieve():
    """Get a specific network service"""
    customer = mommy.make("crm.Customer")
    service = mommy.make("service.ExchangeLanNetworkService")
    view = views.NetworkServicesViewSet.as_view({"get": "retrieve"})
    request = authorized_requests.get(customer)
    response = view(request, pk=service.pk)

    assert response.status_code == 200


@pytest.mark.django_db
def test_network_features_view_set__list():
    """List all the network features"""
    customer = mommy.make("crm.Customer")
    view = views.NetworkFeaturesViewSet.as_view({"get": "list"})
    request = authorized_requests.get(customer)
    response = view(request)

    assert response.status_code == 200


@pytest.mark.django_db
def test_network_features_view_set__retrieve():
    """A a specific of all the network features"""
    customer = mommy.make("crm.Customer")
    feature = mommy.make("service.RouteServerNetworkFeature")
    view = views.NetworkFeaturesViewSet.as_view({"get": "retrieve"})
    request = authorized_requests.get(customer)
    response = view(request, pk=feature.pk)

    assert response.status_code == 200

