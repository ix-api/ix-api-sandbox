
import pytest
from model_mommy import mommy

from jea.api.v1.eventmachine.serializers import (
    StatusSerializer,
    EventSerializer,
)

@pytest.mark.django_db
def test_status_serializer():
    """Test status serializer"""
    status = mommy.prepare("eventmachine.StatusMessage")
    serializer = StatusSerializer(status)

    assert serializer.data, "Serializer should serialize without crashing"


@pytest.mark.django_db
def test_event_serializer():
    """Test status serializer"""
    event = mommy.prepare("eventmachine.Event")
    serializer = EventSerializer(event)

    assert serializer.data, "Serializer should serialize without crashing"

