
"""
API request helpers
"""

from rest_framework import test

from jea.crm import models as crm_models
from jea.auth import roles
from jea.api.v1.auth.users import ApiUser


def _make_api_user(customer: crm_models.Customer) -> ApiUser:
    """Create api user from customer"""
    return ApiUser(customer=customer, roles=[
        roles.ACCESS,
    ])

#
# Request factories
#
def get(
        customer: crm_models.Customer,
        data=None,
        path="",
        format="json",
        **extra,
    ):
    """Create an authorized GET request"""
    api_user = _make_api_user(customer)
    request = test.APIRequestFactory().get(
        path, data=data, format=format, **extra)
    test.force_authenticate(request, user=api_user)

    return request


def post(
        customer: crm_models.Customer,
        data=None,
        path="",
        format="json",
        **extra,
    ):
    """Create an authorized POST request"""
    api_user = _make_api_user(customer)
    request = test.APIRequestFactory().post(
        path, data=data, format=format, **extra)
    test.force_authenticate(request, user=api_user)

    return request


def put(
        customer: crm_models.Customer,
        data=None,
        path="",
        format="json",
        **extra,
    ):
    """Create an authorized PUT request"""
    api_user = _make_api_user(customer)
    request = test.APIRequestFactory().put(
        path, data=data, format=format, **extra)
    test.force_authenticate(request, user=api_user)

    return request


def patch(
        customer: crm_models.Customer,
        data=None,
        path="",
        format="json",
        **extra,
    ):
    """Create an authorized PATCH request"""
    api_user = _make_api_user(customer)
    request = test.APIRequestFactory().patch(
        path, data=data, format=format, **extra)
    test.force_authenticate(request, user=api_user)

    return request


def delete(
        customer: crm_models.Customer,
        data=None,
        path="",
        format="json",
        **extra,
    ):
    """Create an authorized DELETE request"""
    api_user = _make_api_user(customer)
    request = test.APIRequestFactory().delete(
        path, data=data, format=format, **extra)
    test.force_authenticate(request, user=api_user)

    return request
