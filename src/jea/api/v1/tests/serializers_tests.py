
"""
Test Api Serializers
"""

from enum import Enum
from collections import namedtuple

import pytest
from model_mommy import mommy
from django.db import models
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from jea.api.v1.serializers import (
    JeaPrimaryKeyRelatedField,
    OneHotPrimaryKeyRelatedField,
    PolymorphicSerializer,
    EnumField,
    MacAddressField,
)


#
# JeaPrimaryKeyRelatedField
#
def test_jea_primary_key_related_field():
    """Test pk to string conversion"""
    class FooModel(models.Model):
        name = models.CharField()

    field = JeaPrimaryKeyRelatedField(
        queryset=FooModel.objects.all())

    foo = FooModel(pk=23, name="test")
    pk_repr = field.to_representation(foo)

    assert isinstance(pk_repr, str)
    assert pk_repr == "23"


#
# EnumField
#
def test_enum_field_to_internal_value():
    """Test enum field's to internal value"""
    class E(Enum):
        E_100 = 100
        E_1000 = 1000

    field = EnumField(E, values={
        E.E_100: "1e2",
    })

    value = field.to_internal_value("1e2")
    assert value == E.E_100

    value = field.to_internal_value("E_1000")
    assert value == E.E_1000

    value = field.to_internal_value("e_1000")
    assert value == E.E_1000

    value = field.to_internal_value(100)
    assert value == E.E_100

    value = field.to_internal_value(1000)
    assert value == E.E_1000

    with pytest.raises(ValidationError):
        field.to_internal_value("1e23")


def test_enum_field_to_representation():
    """Test enum field's to representation method"""
    class E(Enum):
        E_100 = 100
        E_1000 = 1000

    field = EnumField(E, values={
        E.E_1000: "1e3",
    })

    e_rep = field.to_representation(E.E_1000)
    assert e_rep == "1e3"

    e_rep = field.to_representation(E.E_100)
    assert e_rep == 100

#
# OneHotSourceField
#
def test_one_hot_primary_key_related_field():
    """Test group polymorphic field"""

    class ExampleSerializer(serializers.Serializer):
        field = OneHotPrimaryKeyRelatedField(
            sources=["foo", "bar"],
            read_only=True)

    Obj = namedtuple("Obj", ["foo", "bar"]) 
    obj = Obj(foo=23, bar=None)

    serializer = ExampleSerializer(obj)
    assert serializer.data["field"] == 23

    obj = Obj(foo=None, bar=42)
    serializer = ExampleSerializer(obj)
    assert serializer.data["field"] == 42

    obj = Obj(bar=200, foo=100)
    serializer = ExampleSerializer(obj)
    assert serializer.data["field"] == 100

    obj = Obj(foo=None, bar=None)
    serializer = ExampleSerializer(obj)
    assert serializer.data["field"] == None


def test_mac_address_field__to_internal_value():
    """Test mac address validation"""
    field = MacAddressField() 
    mac = "00:29:18:2f:45:2b"
    assert mac == field.to_internal_value(mac)

    # Validate length
    with pytest.raises(serializers.ValidationError):
        field.to_internal_value("aa:bb")

    with pytest.raises(serializers.ValidationError):
        field.to_internal_value("00:2f:28:f8:1f:95:4b:95")

    with pytest.raises(serializers.ValidationError):
        field.to_internal_value("00:aa:bb:cc:xx:yy")

