
import pytest
from model_mommy import mommy
from rest_framework import serializers

from jea.crm.models import (
    LegalContact,
    BillingContact,
    ImplementationContact,
    NocContact,
    PeeringContact,
)
from jea.api.v1.crm.serializers import (
    CustomerSerializer,
    ContactSerializer,
)


@pytest.mark.django_db
def test_customer_serializer():
    """Test customer serializer"""
    root_customer = mommy.make("crm.Customer")
    customer = mommy.make("crm.Customer", parent=root_customer)

    serializer = CustomerSerializer(customer)

    # Assertations
    assert serializer.data["parent"] == str(root_customer.id)


@pytest.mark.django_db
def test_polymorphic_contact_serialization__legal():
    """Test polymorphic contact serializer"""
    legal_contact = mommy.make("crm.LegalContact")

    # Serialization
    serializer = ContactSerializer(legal_contact)
    data = serializer.data

    # Check response
    assert data, "Serializer should ouput data"


@pytest.mark.django_db
def test_polymorphic_contact_deserialization__legal():
    """Test polymorphic contact deserialization"""
    customer = mommy.make("crm.Customer")
    # Request data:
    data = {
        "type": "legal",
        "legal_company_name": "name",
        "address_country": "xx",
        "address_region": "region",
        "address_locality": "locality",
        "street_address": "23 streetway",
        "postal_code": "01234",
        "email": "e@mail.com",
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    }

    # Deserialization
    serializer = ContactSerializer(data=data)
    assert serializer.is_valid(raise_exception=True), \
        "Serializer should accept valid data"

    # Some invalid data
    serializer = ContactSerializer(data={"type": "legal", "foo": 23})
    with pytest.raises(serializers.ValidationError):
        serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_polymorphic_contact_serialization__noc():
    """Test polymorphic contact serializer"""
    noc_contact = mommy.make("crm.NocContact")

    # Serialization
    serializer = ContactSerializer(noc_contact)
    data = serializer.data

    # Check response
    assert data, "Serializer should ouput data"


@pytest.mark.django_db
def test_polymorphic_contact_deserialization__noc():
    """Test polymorphic contact serializer"""
    customer = mommy.make("crm.Customer")
    data = {
        "type": "noc",
        "telephone": "+1234",
        "email": "ben@utzer.com",
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    }

    serializer = ContactSerializer(data=data)
    assert serializer.is_valid(raise_exception=True)

    # Some invalid data
    data["email"] = "invalid.email.address"
    serializer = ContactSerializer(data=data)
    with pytest.raises(serializers.ValidationError):
        serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_polymorphic_contact_serialization__peering():
    """Test polymorphic contact serializer for peering"""
    contact = mommy.make("crm.PeeringContact")

    # Serialization
    serializer = ContactSerializer(contact)
    data = serializer.data

    # Check response
    assert data, "Serializer should ouput data"


@pytest.mark.django_db
def test_polymorphic_contact_deserialization__noc():
    """Test polymorphic contact serializer"""
    customer = mommy.make("crm.Customer")
    data = {
        "type": "peering",
        "email": "ben@utzer.com",
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    }

    serializer = ContactSerializer(data=data)
    assert serializer.is_valid(raise_exception=True)

    # Some invalid data
    data["email"] = "invalid.email.address"
    serializer = ContactSerializer(data=data)
    with pytest.raises(serializers.ValidationError):
        serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_polymorphic_contact_serialization__billing():
    """Test polymorphic contact serializer"""
    billing_contact = mommy.make("crm.BillingContact")
    # Serialization
    serializer = ContactSerializer(billing_contact)
    data = serializer.data

    # Check response
    assert data, "Serializer should ouput data"


@pytest.mark.django_db
def test_polymorphic_contact_deserialization__billing():
    """Test polymorphic contact serializer"""
    customer = mommy.make("crm.Customer")
    data = {
        "type": "billing",
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
        "legal_company_name": "Amazing Networks Europe Inc.",
        "address_country": "xx",
        "address_region": "region",
        "address_locality": "locality",
        "street_address": "23 Foostreet",
        "postal_code": "01234",
        "vat_number": "DE123012",
        "email": "ben@utzer.com",
    }

    serializer = ContactSerializer(data=data)
    assert serializer.is_valid(raise_exception=True)



@pytest.mark.django_db
def test_polymorphic_contact_serialization__implementation():
    """Test polymorphic contact serializer"""
    implemenation_contact = mommy.make("crm.ImplementationContact")

    # Serialization
    serializer = ContactSerializer(implemenation_contact)
    data = serializer.data

    # Check response
    assert data, "Serializer should ouput data"


@pytest.mark.django_db
def test_polymorphic_contact_deserialization__implementation():
    """Test polymorphic contact serializer"""
    customer = mommy.make("crm.Customer")
    data = {
        "type": "implementation",
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
        "name": "Ben Utzer",
        "telephone": "+1234",
        "email": "ben@utzer.com",
    }

    serializer = ContactSerializer(data=data)
    assert serializer.is_valid(raise_exception=True)

    # Some invalid data
    data["email"] = "invalid.email.address"
    serializer = ContactSerializer(data=data)
    with pytest.raises(serializers.ValidationError):
        serializer.is_valid(raise_exception=True)

