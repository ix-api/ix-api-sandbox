
"""
Test CRM Views
"""

import pytest
from model_mommy import mommy

from jea.api.v1.tests import authorized_requests
from jea.api.v1.crm import views


#
# Customers
#

@pytest.mark.django_db
def test_customers_view_set__list():
    """Test listing all customers"""
    customer = mommy.make("crm.Customer")
    request = authorized_requests.get(customer)
    view = views.CustomersViewSet.as_view({"get": "list"})

    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_customers_view_set__retrieve():
    """Test retriving a specific customer"""
    customer = mommy.make("crm.Customer")
    request = authorized_requests.get(customer)
    view = views.CustomersViewSet.as_view({"get": "retrieve"})

    # Customer should be able to see itself
    response = view(request, pk=customer.pk)

    assert response.status_code == 200


@pytest.mark.django_db
def test_customers_view_set__create():
    """Test creating a new customer"""
    view = views.CustomersViewSet.as_view({"post": "create"})
    customer = mommy.make("crm.Customer")
    new_customer = mommy.prepare("crm.Customer")

    request = authorized_requests.post(customer, {
        "name": new_customer.name,
        "external_ref": new_customer.external_ref,
    })

    response = view(request)
    assert response.status_code == 201


@pytest.mark.django_db
def test_customers_view_set__update():
    """Test updating a customer"""
    view = views.CustomersViewSet.as_view({"put": "update"})
    customer = mommy.make("crm.Customer")
    new_customer = mommy.prepare("crm.Customer")

    request = authorized_requests.put(customer, {
        "name": new_customer.name,
    })

    response = view(request, pk=customer.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_customers_view_set__partial_update():
    """Test updating parts of the customer"""
    view = views.CustomersViewSet.as_view({"patch": "partial_update"})
    customer = mommy.make("crm.Customer")

    request = authorized_requests.patch(customer, {
        "external_ref": "FOO-BAR-23",
    })

    response = view(request, pk=customer.pk)
    assert response.status_code == 200


#
# Contacts
#

@pytest.mark.django_db
def test_customer_view_set__create__with_parent():
    """Provide a different parent"""
    view = views.CustomersViewSet.as_view({"post": "create"})
    customer = mommy.make("crm.Customer")
    new_customer = mommy.prepare("crm.Customer")
    # With parent id
    parent = mommy.make("crm.Customer", scoping_customer=customer)
    request = authorized_requests.post(customer, {
        "name": new_customer.name,
        "parent": parent.pk,
    })
    response = view(request)
    assert response.status_code == 201


@pytest.mark.django_db
def test_contacts_view_set__list():
    """Test listing all contacts"""
    view = views.ContactsViewSet.as_view({"get": "list"})
    customer = mommy.make("crm.Customer")
    request = authorized_requests.get(customer)

    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_contact_view_set__retrieve():
    """Test getting a specific customer contact"""
    view = views.ContactsViewSet.as_view({"get": "retrieve"})
    customer = mommy.make(
        "crm.Customer")
    contact = mommy.make(
        "crm.ImplementationContact",
        scoping_customer=customer)

    request = authorized_requests.get(customer)
    response = view(request, pk=contact.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_contact_view_set__create():
    """Test creating a contact"""
    view = views.ContactsViewSet.as_view({"post": "create"})
    customer = mommy.make("crm.Customer")
    request = authorized_requests.post(customer, {
        "type": "implementation",
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
        "name": "Test Implementation Contact",
        "email": "foo@bar.net",
    })

    response = view(request)
    assert response.status_code == 201


@pytest.mark.django_db
def test_contact_view_set__update():
    """Test updating a contact"""
    view = views.ContactsViewSet.as_view({"put": "update"})
    customer = mommy.make("crm.Customer")
    contact = mommy.make(
        "crm.NocContact",
        scoping_customer=customer)
    request = authorized_requests.put(customer, {
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
        "email": "noc@test.example.net",
    })

    response = view(request, pk=contact.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_contact_view_set__partial_update():
    """Test partial updating a contact"""
    view = views.ContactsViewSet.as_view({"patch": "partial_update"})
    customer = mommy.make("crm.Customer")
    contact = mommy.make(
        "crm.NocContact",
        scoping_customer=customer)
    request = authorized_requests.patch(customer, {
        "email": "noc@test.example.net",
    })

    response = view(request, pk=contact.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_contact_view_set__destroy():
    """Test deleting a contact"""
    view = views.ContactsViewSet.as_view({"delete": "destroy"})
    customer = mommy.make("crm.Customer")
    contact = mommy.make(
        "crm.NocContact",
        scoping_customer=customer)
    request = authorized_requests.delete(customer)

    response = view(request, pk=contact.pk)
    assert response.status_code == 200

