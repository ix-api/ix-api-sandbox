

from jea.api.v1.errors import problems


def test_problems____init__():
    """Initialize problem, check if defaults are overridden"""
    problem = problems.Problem(title="title")

    assert problem.ptype == "about:blank" # default value
    assert problem.title == "title" # as above
    assert problem.status == None # default none


#
# Problem classes: Rest Framework
#
def test_generic_server_problem():
    """Test the generic error problem"""
    assert problems.ServerErrorProblem()


def test_validation_error_problem():
    """Test validation error problem"""
    problem = problems.ValidationErrorProblem(extra={
        "properties": ["foo"],
    })

    assert problem
    assert problem.extra["properties"][0] == "foo"


def test_parse_error_problem():
    """Test parse error problem"""
    assert problems.ParseErrorProblem()


def test_authentication_problem():
    """Test authentication problem"""
    assert problems.AuthenticationProblem()


def test_not_authenticated_problem():
    """Test authentication missing"""
    assert problems.NotAuthenticatedProblem()


def test_permission_denied_problem():
    """Test permission denied"""
    assert problems.PermissionDeniedProblem()


def test_not_found_problem():
    """Test 404 not found"""
    assert problems.NotFoundProblem()


def test_method_not_allowed_problem():
    """Test method not allowed"""
    assert problems.MethodNotAllowedProblem()


def test_not_acceptable_problem():
    """Test not acceptable errors"""
    assert problems.NotAcceptableProblem()


def test_unsupported_media_type_problem():
    """Test creeating an unsupported media type problem"""
    assert problems.UnsupportedMediaTypeProblem()


def test_too_many_requests_problem():
    """Test a throttled problem"""
    # I guess the problem is not trottled.
    assert problems.TooManyRequestsProblem()



