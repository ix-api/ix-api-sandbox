

"""
Test Problem serializers
"""


from jea.access import exceptions as access_exceptions
from jea.api.v1.access import serializers as access_serializers
from jea.api.v1.errors import (
    problems,
    problem_factory,
    serializers,
)


def test_problem_serializer():
    """Test serializing a problem"""
    problem = problems.MethodNotAllowedProblem()
    serializer = serializers.ProblemSerializer(problem)

    assert serializer.data


def test_problem_serializer__field_validation_error():
    """Test more complex error"""
    exc = access_exceptions.DemarcationPointInUse()
    problem = problem_factory.make_problem(exc)
    assert isinstance(problem, problems.ValidationErrorProblem)

    serializer = serializers.ProblemSerializer(problem)

    assert serializer.data
    print(serializer.data)


def test_problem_serializer__validation_error():
    """Test more complex error"""
    serializer = access_serializers.ConnectionRequestSerializer(data={})
    problem = None
    try:
        serializer.is_valid(raise_exception=True)
    except Exception as e:
        problem = problem_factory.make_problem(e)

    assert isinstance(problem, problems.ValidationErrorProblem)

    serializer = serializers.ProblemSerializer(problem)
    assert serializer.data
    print(serializer.data)

