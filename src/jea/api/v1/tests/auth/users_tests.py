
"""
Tests for the api v1 auth ApiUser
"""

from datetime import datetime

from model_mommy import mommy

from jea.api.v1.auth.users import ApiUser


def test__token_claims_are_assigned():
    """
    Test that all required token claims are assigned to the
    user object.
    """
    customer = mommy.prepare("crm.Customer")
    root_customer = mommy.prepare("crm.Customer")

    claims = {
        "iss": "1",
        "sub": None,
        "iat": datetime(2000, 1, 1),
        "exp": datetime(2000, 1, 1),
        "roles": [
            "access"
        ]
    }

    user = ApiUser(customer, claims)

    assert user.customer == customer
    assert user.customer_id == customer.id

    assert user.access_roles == claims["roles"]

