
"""
API Serializers and Serialization Helpers
"""

from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.serializers import (
    PrimaryKeyRelatedField,
)

from utils.datastructures import reverse_mapping


class JeaPrimaryKeyRelatedField(PrimaryKeyRelatedField):
    """
    Subclass pk related field, to make the response compliant
    with our own OpenAPI spec: ID should be of type string.
    """
    def to_representation(self, value):
        """Override representation"""
        res = super(JeaPrimaryKeyRelatedField, self).to_representation(value)

        return str(res)


class PolymorphicSerializer(serializers.Serializer):
    """
    Serializer for polymorphic models.

    Selects the fitting serializer based on a mapping,
    and adds the type to the result.
    """
    # Prevent serializer from showing up in the schema
    __serializer_base__ = True

    # Serializer mapping
    serializer_classes: dict = {}
    entity_types: dict = {}

    def __init__(self, *args, entity_type=None, **kwargs):
        """Override init to provide an optional type kwarg"""
        super().__init__(*args, **kwargs)

        # Map class to entity's type string representation
        if entity_type and not isinstance(entity_type, str):
            entity_type = self.entity_types[entity_type]

        self.entity_type = entity_type

    def to_representation(self, entity):
        """
        Serialize an entity. Select serializer based on model class.

        :param entity: The entity to serialize.
        """
        entity_class = entity.__class__
        entity_type = self.entity_types.get(entity_class)
        if not entity_type:
            raise RuntimeError(f"Invalid Type Mapping: {entity_class}")

        # Get serializer
        serializer_class = self.serializer_classes[entity_type]
        serializer = serializer_class(entity)

        data = serializer.data

        # Add type to result
        data["type"] = entity_type

        return data

    def to_internal_value(self, data):
        """
        Deserialize data with a matching entity deserializer.

        :param data: Incoming entity data
        """
        entity_type = data.get("type", self.entity_type)
        if not entity_type:
            raise serializers.ValidationError({
                "type": "type field missing"})

        # Get serializer
        serializer_class = self.serializer_classes.get(entity_type)
        if not serializer_class:
            raise serializers.ValidationError({
                "type": "invalid type given"})

        serializer = serializer_class(data=data, partial=self.partial)
        serializer.is_valid(raise_exception=True)

        # Include type in data
        data = serializer.validated_data
        data["type"] = entity_type

        return data


class EnumField(serializers.ChoiceField):
    """Serialize an enum field"""
    def __init__(self, enum_class, values={}, **kwargs):
        """
        Override initialization with fixed choice set
        You can provide a mapping of values:

        Consider:
            Speed(Enum):
                SPEED_1G = 1000

        A mapping would be:
            Speed.SPEED_1G: "1G"

        """
        self.enum_class = enum_class
        self.values = values
        self.inv_values = reverse_mapping(values)

        # Choices from mapping
        choices = [(e.name, e.name) for e in enum_class] + \
                  [(e.value, e.name) for e in enum_class] + \
                  [(e.value, e.value) for e in enum_class] + \
                  [(name, name) for _, name in values.items()]

        super().__init__(choices, **kwargs)

    def to_internal_value(self, data):
        """Convert data to enum value"""
        value = self.inv_values.get(data)
        if not value:
            try:
                value = self.enum_class(data)
            except ValueError:
                try:
                    value = self.enum_class[data]

                except KeyError:
                    pass

            if value:
                return value

            # Try to fall back enum_class's values
            if isinstance(data, str):
                data = data.upper()

            try:
                value = self.enum_class(data)
            except ValueError:
                try:
                    value = self.enum_class[data]
                except KeyError:
                    # We did everything we could.
                    raise ValidationError("Unknown enum value")

        return value

    def to_representation(self, value):
        """Convert enum value to representation"""
        value_rep = self.values.get(value)
        if not value_rep:
            value_rep = value.value

        return value_rep


class OneHotPrimaryKeyRelatedField(JeaPrimaryKeyRelatedField):
    """
    This field may be used in a quasi polymorphic scenario.
    """
    def __init__(self, **kwargs):
        """Initialize serializer field"""
        self.sources = kwargs.pop("sources", [])
        self.field = kwargs.pop("field", None)
        kwargs.pop("source", None)
        super(OneHotPrimaryKeyRelatedField, self).__init__(
            source="*", **kwargs)

    def to_representation(self, value):
        """
        Pick the first non None value of the object,
        with properties in the defined sources.
        """
        values = [getattr(value, source, None)
                  for source in self.sources]

        # Get first not non value. This implies, that
        # in case there are conflicting values, the precidence
        # is determined by the order of the attributes in the
        # sources list.
        for v in values:
            if not v is None:
                return v

        return None

    def to_internal_value(self, data):
        """
        Override to internal value
        """
        res = super(OneHotPrimaryKeyRelatedField, self).to_internal_value(
            data)

        return {
            self.field: res,
        }


class MacAddressField(serializers.CharField):
    """
    Derived from a char field, we validate that the input
    is a mac address.
    """
    def to_internal_value(self, data):
        """Parse and validate mac address"""
        if not isinstance(data, str):
            raise ValidationError(
                "A MAC address input must be a string")

        if not (len(data) > 10 and len(data) <= 17):
            raise ValidationError(
                "Invalid MAC address length")

        tokens = data.split(":")  
        if not len(tokens) == 6:
            raise ValidationError(
                "MAC address too short")

        try:
            [int(t, 16) for t in tokens]
        except ValueError:
            raise ValidationError(
                "A mac address must only contain hexadecimal byte values")

        # Normalize
        return data.lower()

