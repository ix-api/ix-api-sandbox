
from rest_framework import serializers

from jea.eventmachine.models import State
from jea.api.v1.serializers import (
    EnumField,
    JeaPrimaryKeyRelatedField,
)


class StatusSerializer(serializers.Serializer):
    """Status Message"""
    severity = serializers.IntegerField(
        min_value=0,
        max_value=7,
        help_text="""
            We are using syslog severity levels: 0 = Emergency,
            1 = Alert, 2 = Critical, 3 = Error, 4 = Warning,
            5 = Notice, 6 = Informational, 7 = Debug.

            Example: 2
        """)
    tag = serializers.CharField()
    message = serializers.CharField()
    attrs = serializers.JSONField()
    timestamp = serializers.DateTimeField()


class EventSerializer(serializers.Serializer):
    """Event"""
    serial = serializers.IntegerField()
    customer = JeaPrimaryKeyRelatedField(read_only=True)

    type = serializers.CharField()
    payload = serializers.JSONField()

    timestamp = serializers.DateTimeField()


class StatefulObjectSerializer(serializers.Serializer):
    """A stateful object"""
    state = EnumField(State, read_only=True)
    status = StatusSerializer(many=True, read_only=True)

