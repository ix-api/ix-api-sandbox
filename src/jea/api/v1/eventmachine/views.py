
"""
Eventmachine :: Views

You can receive events, authorized as a customer for
all of your objects and your subcustomers objects
down to one level.
"""


from rest_framework import exceptions, status

from jea.api.v1.permissions import require_customer
from jea.api.v1.response import tags
from jea.api.v1.viewsets import JEAViewSet
from jea.api.v1.response import ApiSuccess, ApiError
from jea.api.v1.eventmachine.serializers import (
    EventSerializer,
)
from jea.eventmachine.services import (
    events as events_svc,
)


class EventsViewSet(JEAViewSet):
    """
    `Events` are generated on the platform for example, when
    a `demarc` gets put in production or a `customer` is
    created and whenever a stateful object changes it's
    state.

    You can provide the serial of the last event
    in the `after` query parameter to retrieve only new events.
    """
    # Hide from documentation by not defining the responses
    __responses = {
        "list": {
            200: EventSerializer(many=True),
        }
    }

    @require_customer
    def list(self, request, customer=None):
        """Retrieve all events."""
        events = events_svc.get_events(
            customer=customer,
            after=request.query_params.get("after", 0),
        )

        serializer = EventSerializer(events, many=True)

        return serializer.data

