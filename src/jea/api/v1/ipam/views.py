
"""
JEA IPAM

Manage ip and mac addresses.
"""

import logging

from rest_framework import request

from jea.ipam.services import (
    ip_addresses as ip_addresses_svc,
    mac_addresses as mac_addresses_svc,
)
from jea.ipam.filters import IpAddressFilter, MacAddressFilter
from jea.api.v1.errors import problems
from jea.api.v1.permissions import require_customer
from jea.api.v1.viewsets import JEAViewSet
from jea.api.v1.ipam.serializers import (
    IpAddressSerializer,
    MacAddressSerializer,
)
from jea.api.v1.response import (
    ApiSuccess,
    ApiError
)


class IpAddressViewSet(JEAViewSet):
    """
    An `IP` is a IPv4 or 6 addresses, with a given validity period.
    Some services require IP addresses to work.

    When you are joining an `exchange_lan` network service
    for example, addresses on the peering lan will be assigned
    to you.
    """

    filterset_class = IpAddressFilter
    responses = {
        "read": {
            200: IpAddressSerializer(),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
                problems.NotFoundProblem(),
            ],
        },
        "list": {
            200: IpAddressSerializer(many=True),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
            ],
        },
        "create": {
            "request": IpAddressSerializer(),
            201: IpAddressSerializer(),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
                problems.NotFoundProblem(),
                problems.ValidationErrorProblem(),
            ],
        },
        "update": {
            "request": IpAddressSerializer(),
            200: IpAddressSerializer(),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
                problems.NotFoundProblem(),
                problems.ValidationErrorProblem(),
            ],
        },
        "partial_update": {
            "request": IpAddressSerializer(partial=True),
            200: IpAddressSerializer(),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
                problems.NotFoundProblem(),
                problems.ValidationErrorProblem(),
            ],
        },
    }

    @require_customer
    def list(self, request, customer=None):
        """
        List all ip addresses (and prefixes)
        """
        ips = ip_addresses_svc.get_ip_addresses(
            scoping_customer=customer,
            filters=request.query_params)

        serializer = IpAddressSerializer(ips, many=True)

        return serializer.data


    @require_customer
    def retrieve(self, request, customer=None, pk=None):
        """
        Get a signle ip addresses object.
        """
        ip_address = ip_addresses_svc.get_ip_address(
            scoping_customer=customer,
            ip_address=pk)

        serializer = IpAddressSerializer(ip_address)
        return serializer.data


    @require_customer
    def update(self, request, customer=None, pk=None):
        """
        Update an ip address object.

        You can only update
        IP addresses within your current scope. Not all
        addresses you can read you can update.
        """
        ip_address = ip_addresses_svc.get_ip_address(
            scoping_customer=customer,
            ip_address=pk)

        # For now only updating the fqdn is allowed
        serializer = IpAddressSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Perform update
        ip_address = ip_addresses_svc.update_ip_address(
            scoping_customer=customer,
            ip_address=ip_address,
            ip_address_update=serializer.validated_data)

        # Serialize result
        serializer = IpAddressSerializer(ip_address)
        return serializer.data


    @require_customer
    def partial_update(self, request, customer=None, pk=None):
        """
        Update parts of an ip address.

        As with the `PUT` opertaion, IP addresses, where you
        don't have update rights, will yield a `resource access denied`
        error when attempting an update.
        """
        ip_address = ip_addresses_svc.get_ip_address(
            scoping_customer=customer,
            ip_address=pk)

        # For now only updating the fqdn is allowed
        serializer = IpAddressSerializer(data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)

        # Perform update
        ip_address = ip_addresses_svc.update_ip_address(
            scoping_customer=customer,
            ip_address=ip_address,
            ip_address_update=serializer.validated_data)

        # Serialize result
        serializer = IpAddressSerializer(ip_address)
        return serializer.data


class MacAddressViewSet(JEAViewSet):
    """
    A `MAC` is a MAC addresses with a given validity period.

    Some services require MAC addresses to work.

    The address itself can not be updated after creation.
    However: It can be expired by changing the `valid_not_before`
    and `valid_not_after` properties.
    """
    filterset_class = MacAddressFilter

    responses = {
        "read": {
            200: MacAddressSerializer(),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
                problems.NotFoundProblem(),
            ],
        },
        "list": {
            200: MacAddressSerializer(many=True),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
            ],
        },
        "create": {
            "request": MacAddressSerializer(),
            201: MacAddressSerializer(),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
                problems.ValidationErrorProblem(),
            ],
        },
        "delete": {
            200: MacAddressSerializer(),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
                problems.NotFoundProblem(),
            ],
        }
    }

    @require_customer
    def list(self, request, customer=None):
        """List all mac addresses managed by the authorized customer."""
        mac_addresses = mac_addresses_svc.get_mac_addresses(
            scoping_customer=customer,
            filters=request.query_params)


        serializer = MacAddressSerializer(mac_addresses, many=True)

        return serializer.data

    @require_customer
    def retrieve(self, request, customer=None, pk=None):
        """Get a signle mac address by id"""
        mac_address = mac_addresses_svc.get_mac_address(
            scoping_customer=customer,
            mac_address=pk)

        serializer = MacAddressSerializer(mac_address)
        return serializer.data

    @require_customer
    def create(self, request, customer=None):
        """Create a new mac address."""
        serializer = MacAddressSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        # So far so good, create a fresh mac:
        mac_address = mac_addresses_svc.create_mac_address(
            scoping_customer=customer,
            mac_address_input=serializer.validated_data)

        serializer = MacAddressSerializer(mac_address)

        return ApiSuccess(serializer.data, status=201)

    @require_customer
    def destroy(self, request, customer=None, pk=None):
        """Remove a mac address"""
        mac_address = mac_addresses_svc.get_mac_address(
            scoping_customer=customer,
            mac_address=pk)

        mac_address = mac_addresses_svc.remove_mac_address(
            scoping_customer=customer,
            mac_address=mac_address)

        serializer = MacAddressSerializer(mac_address)

        return ApiSuccess(serializer.data)

