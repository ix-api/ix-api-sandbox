
"""
IP Address Managmenet Serializers
"""

from rest_framework import serializers
from django.utils import timezone

from jea.access import models as access_models
from jea.service import models as service_models
from jea.ipam import models as ipam_models
from jea.api.v1.crm.serializers import (
    OwnableObjectSerializer,
    InvoiceableObjectSerializer,
)
from jea.api.v1.serializers import (
    MacAddressField,
    JeaPrimaryKeyRelatedField,
    OneHotPrimaryKeyRelatedField,
)


class InlineIpAddressSerializer(
        OwnableObjectSerializer,
        serializers.Serializer,
    ):
    """IP-Address (Inline)"""
    address = serializers.CharField()
    prefix_length = serializers.IntegerField(min_value=0, max_value=128)
    fqdn = serializers.CharField()

    valid_not_before = serializers.DateTimeField()
    valid_not_after = serializers.DateTimeField()


class IpVersionField(serializers.Field):
    """IP-Version (4, 6)"""
    def to_representation(self, value):
        """
        Serialize ip version.

        :param value: An IpVersion enum
        """
        return value.value


    def to_internal_value(self, data):
        """
        Deserialize ip version

        :param data: IP version integer
        """
        return ipam_models.IpVersion(data)


class IpAddressSerializer(
        OwnableObjectSerializer,
        serializers.Serializer,
    ):
    """IP-Address"""
    id = JeaPrimaryKeyRelatedField(read_only=True)

    version = IpVersionField(read_only=True)
    address = serializers.CharField(
        read_only=True,
        help_text="""
                IPv4 or IPv6 Address in the following format:
                - IPv4: [dot-decimal notation](https://en.wikipedia.org/wiki/Dot-decimal_notation)
                - IPv6:

                Example: "23.142.52.0"
            """)
    prefix_length = serializers.IntegerField(
        read_only=True,
        min_value=0,
        max_value=128,
        help_text="""
            The CIDR ip prefix length

            Example: 23
        """)

    fqdn = serializers.CharField(
        max_length=100,
        required=False,
        allow_null=True,
        allow_blank=False)

    valid_not_before = serializers.DateTimeField(read_only=True)
    valid_not_after = serializers.DateTimeField(read_only=True)


class MacAddressSerializer(
        OwnableObjectSerializer,
        serializers.Serializer,
    ):
    """MAC-Address""" # And cheese address
    id = JeaPrimaryKeyRelatedField(read_only=True)

    address = MacAddressField(
        help_text="""
            MAC Address Value, formatted hexadecimal values with colons.

            Example: 42:23:bc:8e:b8:b0
        """)

    valid_not_before = serializers.DateTimeField(
        allow_null=True,
        default=timezone.now)
    valid_not_after = serializers.DateTimeField(
        default=None,
        allow_null=True)
