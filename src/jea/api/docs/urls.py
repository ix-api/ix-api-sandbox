
from django.urls import path

from jea.api.docs.v1 import views as v1_views
from jea.api.docs import views as docs_views


urlpatterns = [
    path(r"redoc",
         docs_views.serve_redoc,
         name="redoc"),
    path(r"v1/schema.<str:schema_format>",
         v1_views.render_schema,
         name="v1-schema"),
    path(r"v1/problems/",
         v1_views.render_problems_index,
         name="v1-problems"),
    path(r"v1/problems/<str:problem_type>",
         v1_views.render_problems_docs,
         name="v1-problem-details"),
]

