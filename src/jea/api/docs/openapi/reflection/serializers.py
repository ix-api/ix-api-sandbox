
"""
Serializers Introspection

Find all API related serializers classes, 
create schema from them.
"""

import importlib
import pkgutil
import inspect

from rest_framework import serializers

from jea.api.docs.openapi.generators import serializers_meta


def find_serializers(api_base):
    """Find serializer classes"""
    packages = [p.name for p in pkgutil.walk_packages(api_base.__path__)
                if p.ispkg]

    # Convetion: <base>.<package>.serializers
    serializer_classes = set()
    for package in packages:
        module_name = f"{api_base.__name__}.{package}.serializers"
        try:
            module = importlib.import_module(module_name)
        except ImportError:
            continue

        for serializer in _find_module_serializers(module):
            if not serializer.__module__.startswith(api_base.__name__):
                continue
            # if getattr(serializer, "__serializer_base__", False):
            #    continue

            serializer_classes.add(serializer)

    # Sort serializers by module
    serializer_classes = sorted(
        serializer_classes,
        key=lambda s: f"{s.__module__}.{s.__name__}")

    return serializer_classes


def _find_module_serializers(module):
    """Get serializers from a serializer module"""
    return [serializer 
            for _name, serializer
            in inspect.getmembers(module, inspect.isclass)
            if issubclass(serializer, serializers.Serializer)]


def get_schemas(api_base):
    """Get all serializer schemas"""
    serializer_classes = find_serializers(api_base)
    schema = {}
    for serializer in serializer_classes:
        schema.update(serializers_meta.spec_from_serializer(serializer))
        schema.update(serializers_meta.spec_from_partial_serializer(serializer))

    return schema


