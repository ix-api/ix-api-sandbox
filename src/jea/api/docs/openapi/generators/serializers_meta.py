
"""
Generate OpenAPI (3) schemata from serializer metadata
"""

from typing import List, Mapping
import collections
import json

from django.utils import text as text_utils
from rest_framework import serializers

from utils import text as extra_text_utils
from jea.api.v1.auth import serializers as auth_serializers
from jea.api.v1.ipam import serializers as ipam_serializers
from jea.api.v1.serializers import (
    EnumField,
    PolymorphicSerializer,
)


def spec_from_field(field):
    """
    Generate a spec fragment from a field.
    If the field is a serializer, make a reference.

    :param field: A serializer field
    """
    if isinstance(field, serializers.Serializer):
        return _spec_from_serializer_ref(field)
    if isinstance(field, (
        serializers.ListSerializer,
        serializers.ListField)):
            return _spec_from_list_serializer(field)

    fragments = [
        _frag_type,
        _frag_description,
        _frag_example,
        _frag_read_only,
        _frag_nullable,
        _frag_min_length,
        _frag_max_length,
        _frag_min_value,
        _frag_max_value,
    ]

    spec = {}
    for mapper in fragments:
        spec = mapper(spec, field)

    return spec


def _spec_from_serializer_ref(field):
    """
    Reference a serializer

    :param field: A serializer field
    """
    ref_name = serializer_ref_name(type(field))
    return {
        "$ref": f"#/components/schemas/{ref_name}"
    }


def _spec_from_list_serializer(field):
    """The field is a list."""
    spec = {
       "type": "array",
       "items": spec_from_field(field.child)
    }

    if field.help_text:
        help_text = extra_text_utils.trim_docstring(field.help_text)
        spec["description"] = extra_text_utils.description_text(help_text)

    return spec


def _frag_type(frag, field):
    """
    Get field type mapping and base fragment specials
    """
    if isinstance(field, serializers.CharField):
        return _frag_char_field(frag, field)
    if isinstance(field, serializers.IntegerField):
        return _frag_integer_field(frag, field)
    if isinstance(field, serializers.BooleanField):
        return _frag_boolean_field(frag, field)
    if isinstance(field, serializers.JSONField):
        return _frag_json_field(frag, field)
    if isinstance(field, serializers.DateTimeField):
        return _frag_date_time_field(frag, field)
    if isinstance(field, serializers.ManyRelatedField):
        return _frag_many_related_field(frag, field)
    if isinstance(field, serializers.PrimaryKeyRelatedField):
        return _frag_pk_related_field(frag, field)
    if isinstance(field, EnumField):
        return _frag_enum_field(frag, field)
    if isinstance(field, auth_serializers.JWTField):
        return _frag_char_field(frag, field)
    if isinstance(field, ipam_serializers.IpVersionField):
        return _frag_integer_enum_field(frag, field, [4,6])

    raise NotImplementedError("Spec fragment missing for: {}".format(
        type(field)))


def _frag_char_field(frag, field):
    """Fragment for a char field"""
    frag["type"] = "string"
    return frag

def _frag_integer_field(frag, field):
    """Fragment for integeer field"""
    frag["type"] = "integer"
    int_type = _int_format_hint(field.min_value, field.max_value)
    if int_type:
        frag["format"] = int_type

    return frag

def _frag_integer_enum_field(frag, field, values):
    """Fragment for integeer field"""
    frag["type"] = "integer"
    frag["enum"] = values
    return frag

def _frag_boolean_field(frag, field):
    """Fragment for boolean"""
    frag["type"] = "boolean"
    return frag

def _frag_json_field(frag, field):
    """Fragment for json field"""
    frag["type"] = "object"
    return frag

def _frag_date_time_field(frag, field):
    """Fragment for date time field"""
    frag["type"] = "string"
    frag["format"] = "date-time"
    return frag

def _frag_pk_related_field(frag, field):
    """Fragment for primary key related field"""
    frag["type"] = "string"
    return frag

def _frag_many_related_field(frag, field):
    """Fragment for primary key related field"""
    frag["type"] = "array"
    frag["items"] = {
        "type": "string",
    }
    return frag

def _frag_enum_field(frag, field):
    """Fragment for enum field"""
    enum_class = field.enum_class
    values = [e.value for e in enum_class]
    if not values:
        return frag

    frag["type"] = "string"
    if not isinstance(values[0], str):
        frag["type"] = "integer"
    frag["enum"] = values
    return frag

#
# Int format helper
#
def _int_format_hint(min_value, max_value):
    """
    Calculate the integer format when min and max
    values are present.
    """
    # We only do this with explict bounds
    if min_value is None or max_value is None:
        return None

    # Get min / max values for signed integers
    max_signed_int32 = 2**31-1
    min_signed_int32 = -2**31
    max_signed_int64 = 2**63-1
    min_signed_int64 = -2**63

    # Preconditions: As the swagger spec explicitly
    # states that there are no unsigned integers, we 
    # have to fit this into the signed integer range.
    if max_value > max_signed_int64:
        return None
    if min_value < min_signed_int64:
        return None

    if max_value > max_signed_int32:
        return "int64"
    if min_value < min_signed_int32:
        return "int64"

    # I guess we can fit it in an int32
    return "int32"

#
# Commons
#
def _frag_description(frag, field):
    """Get description from field's help_text"""
    if field.help_text:
        help_text = extra_text_utils.trim_docstring(field.help_text)
        frag["description"] = extra_text_utils.description_text(help_text)

    return frag


def _frag_example(frag, field):
    """Get example from field's help_text"""
    if field.help_text:
        example_text = extra_text_utils.example_text(
            extra_text_utils.trim_docstring(field.help_text))

        try:
            example = json.loads(example_text)
        except:
            example = example_text

        if example:
            frag["example"] = example

    return frag


def _frag_read_only(frag, field):
    """Get read only property"""
    if field.read_only:
        frag["readOnly"] = True
    return frag


def _frag_nullable(frag, field):
    """Is nullable?"""
    if field.allow_null:
        frag["nullable"] = True
    return frag


def _frag_min_length(frag, field):
    """
    Update schema fragment with min length.

    :param frag: The current shard
    :param field: A serializer field
    """
    min_length = getattr(field, "min_length", None)
    if min_length is not None:
        frag["minLength"] = min_length
    return frag


def _frag_max_length(frag, field):
    """
    Update schema fragment with max length.

    :param frag: The current shard
    :param field: A serializer field
    """
    max_length = getattr(field, "max_length", None)
    if max_length is not None:
        frag["maxLength"] = max_length
    return frag


def _frag_min_value(frag, field):
    """Optional min value fragment"""
    min_value = getattr(field, "min_value", None)
    if min_value is not None:
        frag["minimum"] = min_value
    return frag


def _frag_max_value(frag, field):
    """Optional max value fragment"""
    max_value = getattr(field, "max_value", None)
    if max_value is not None:
        frag["maximum"] = max_value
    return frag


def spec_from_serializer(serializer):
    """
    Generate a complete spec fragment from a serializer.
    """
    serializer_ref = serializer_ref_name(serializer)
    if serializer_ref.endswith("-base"):
        return {} # By convention we can skipt base serializer

    if issubclass(serializer, PolymorphicSerializer):
        return _spec_from_polymorphic_serializer(serializer)

    # Get the partial base reference
    partial_ref = serializer_ref_name(serializer, partial=True)

    # Generate serializer spec
    ext_serializer_spec = {
        "title": serializer_title(serializer),
        "type": "object",
    }

    # Set required array only if not empty
    required_fields = serializer_required_fieldnames(serializer)
    if required_fields:
        ext_serializer_spec["required"] = required_fields

    serializer_spec = {
        "allOf": [
            {"$ref": f"#/components/schemas/{partial_ref}"},
            ext_serializer_spec,
        ]
    }

    return {
        serializer_ref_name(serializer): serializer_spec,
    }


def spec_from_partial_serializer(serializer):
    """
    Like the above, as we can not just override
    the 'required' property in an allOf: inheritance,
    as required may not be an empty array.
    """
    full_ref = serializer_ref_name(serializer)
    if full_ref.endswith("-base"):
        return {} # By convention we can skipt base serializer
    if issubclass(serializer, PolymorphicSerializer):
        spec = _spec_from_polymorphic_serializer(serializer, partial=True)
        return spec

    partial_ref = serializer_ref_name(serializer, partial=True)

    # Generate normal serializer spec
    serializer_spec = {
        "title": "Partial" + serializer_title(serializer),
        "type": "object",
        "description": serializer_description(serializer),
        "properties": serializer_properties(serializer),
    }

    # But this has no required array.
    return {
        partial_ref: serializer_spec,
    }


def _spec_from_polymorphic_serializer(serializer, partial=False):
    """Create polymorphic serializer spec"""
    subschemas = polymorphic_schemas(serializer, partial)
    if not subschemas:
        return {} # This is not required

    title = serializer_title(serializer)
    if partial:
        title += " (partial)"

    spec = {
        serializer_ref_name(serializer, partial): {
            "title": title,
            "description": serializer_description(serializer),
            "oneOf": subschemas,
            "discriminator": polymorphic_discriminator(serializer, partial),
        }
    }

    return spec


def polymorphic_schemas(serializer, partial=False):
    """Get all subschema references"""
    schema_refs = [{
            "$ref": "#/components/schemas/" + serializer_ref_name(ser, partial),
        } for _, ser in serializer.serializer_classes.items()
        if not getattr(ser, "__hidden_in_docs__", False)]

    return schema_refs


def polymorphic_schema_mapping(serializer, partial=False):
    """Get polymorphic schema mapping"""
    mapping = {
        type_name: "#/components/schemas/" + serializer_ref_name(ser, partial)
        for type_name, ser in serializer.serializer_classes.items()
        if not getattr(ser, "__hidden_in_docs__", False)
    }

    return mapping

def polymorphic_discriminator(serializer, partial=False):
    """Create discriminator mapping"""
    discriminator = {
        "propertyName": "type", # It's always type
        "mapping": polymorphic_schema_mapping(serializer, partial),
    }

    return discriminator


def serializer_ref_name(serializer, partial=False) -> str:
    """
    Get entities reference name for a serializer.
    """
    # Convention: <Entity>Serializer
    entity = serializer.__name__.replace("Serializer", "")
    ref_name = text_utils.camel_case_to_spaces(entity)  \
        .replace(" ", "-")

    if partial:
        ref_name += "-partial"

    return ref_name


def serializer_title(serializer) -> str:
    """Get the title from the serializer"""
    # For now use the class name
    # Convention: <Entity>Serializer
    return serializer.__name__.replace("Serializer", "")


def serializer_description(serializer) -> str:
    """Get the description of the serializer"""
    # For now, just use the docstring.
    return extra_text_utils.trim_docstring(serializer.__doc__)


def serializer_required_fieldnames(serializer) -> List[str]:
    """
    Get all required fields from the serializer.
    """
    instance = serializer()
    fields = [name for name, field in instance.fields.items()
              if field.required]

    if getattr(instance, "__polymorphic_type__", False):
        fields.append("type")

    return fields


def serializer_properties(serializer) -> Mapping[str, dict]:
    """
    Get all field schemas using the serializer fields
    meta information.
    """
    instance = serializer()
    properties = {
        name: spec_from_field(field)
        for name, field in instance.fields.items()
    }

    polymorphic_type = getattr(instance, "__polymorphic_type__", False)
    if polymorphic_type:
        # Use an ordered dict to sort the type to the beginning
        poly_properties = collections.OrderedDict({
            "type": {
                "type": "string",
                "example": polymorphic_type,
            }
        })
        poly_properties.update(properties)
        # Prevent yaml from adding a OrderedDict tag.
        # Fingers crossed, that type stays at the top.
        properties = dict(poly_properties)

    return properties

