
import logging

from django import apps

logger = logging.getLogger(__name__)


class ApiDocsAppConfig(apps.AppConfig):
    name = "jea.api.docs"
    verbose_name = "JEA :: API Documentation"

    def ready(self):
        """Application is ready"""
        logger.info("Initializing app: {}".format(self.name))


