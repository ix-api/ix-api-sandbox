
"""
API Documentation v1 views
"""

import yaml
import json

from django import http
from django.shortcuts import render

from jea.api import v1
from jea.api.v1 import urls as v1_urls
from jea.api.docs.openapi import schema
from jea.api.docs.problems import reflection as problems_reflection


def render_schema(request, schema_format):
    """Render the API schema"""
    # Generate schema
    generator = schema.Generator(urlconf=v1_urls)
    document = generator.get_schema()

    # Render schema
    renderer = schema.Renderer(v1)
    api_schema = renderer.get_structure(document)

    # Render as yaml or json
    if schema_format in ["yaml", "yml"]:
        payload = yaml.dump(api_schema)
        return http.HttpResponse(
            payload, content_type="application/yaml")
    else:
        payload = json.dumps(api_schema)
        return http.HttpResponse(
            payload, content_type="application/json")


def _problem_name(name):
    """Make problem name"""
    name = name.replace("Problem", "")
    if not name.endswith("Error"):
        name += "Error"
    return name


def render_problems_index(request):
    """
    Render a list of all problems.

    :param request: The incoming request
    """
    problems = problems_reflection.find_problems(v1)
    # Prepare for rendering
    problem_links = [{
        "type": p.default_type,
        "name": _problem_name(p.__name__),
        "title": p.default_title
    } for p in problems if p.__name__ != "Problem"]

    return render(request, "docs/problems/index.html", {
        "problems": problem_links,
    })


def render_problems_docs(request, problem_type):
    """Render the problem documentation"""
    problems = problems_reflection.find_problems(v1)
    try:
        problem = [{
            "type": p.default_type,
            "name": _problem_name(p.__name__),
            "description": p.__doc__,
            "detail": p.default_detail,
            "title": p.default_title,
            "status_code": p.default_response_status,
        } for p in problems if p.default_type == problem_type][0]
    except IndexError:
        raise http.Http404

    return render(request, "docs/problems/problem.html", {
        "problem": problem,
    })
