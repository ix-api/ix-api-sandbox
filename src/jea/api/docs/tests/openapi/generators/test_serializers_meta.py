
"""
Test getting metadata from serializer fields
"""

import enum

from rest_framework import serializers

from jea.api.v1.serializers import (
    EnumField,
    PolymorphicSerializer,
)
from jea.api.docs.openapi.generators import serializers_meta


def test__frag_max_value():
    """Get max value from field, if present"""
    field = serializers.IntegerField(max_value=42)
    frag = serializers_meta._frag_max_value({}, field)
    assert frag["maximum"] == 42

    field = serializers.CharField(min_length=42)
    frag = serializers_meta._frag_max_value({}, field)
    assert not frag.get("maximum")


def test__frag_min_value():
    """Get min value from field, if present"""
    field = serializers.IntegerField(min_value=42)
    frag = serializers_meta._frag_min_value({}, field)
    assert frag["minimum"] == 42

    field = serializers.CharField(min_length=42)
    frag = serializers_meta._frag_min_value({}, field)
    assert not frag.get("minimum")


def test__frag_max_length():
    """Get min length from field"""
    field = serializers.CharField(max_length=42)
    frag = serializers_meta._frag_max_length({}, field)
    assert frag["maxLength"] == 42

    field = serializers.IntegerField(min_value=42)
    frag = serializers_meta._frag_max_length({}, field)
    assert not frag.get("maxLength")


def test__frag_min_length():
    """Get min length from field"""
    field = serializers.CharField(min_length=42)
    frag = serializers_meta._frag_min_length({}, field)
    assert frag["minLength"] == 42

    field = serializers.IntegerField(min_value=42)
    frag = serializers_meta._frag_min_length({}, field)
    assert not frag.get("minLength")


def test__frag_nullable():
    """Test nullable"""
    field = serializers.BooleanField(allow_null=True)
    frag = serializers_meta._frag_nullable({}, field)
    assert frag["nullable"]


def test__frag_read_only():
    """Test read only"""
    field = serializers.BooleanField(read_only=True, allow_null=True)
    frag = serializers_meta._frag_read_only({}, field)
    assert frag["readOnly"]


def test__frag_description():
    """Get description from help_text"""
    field = serializers.IntegerField(help_text="help_text_help")
    frag = serializers_meta._frag_description({}, field)
    assert frag["description"] == "help_text_help"


def test__frag_enum_field():
    """Test enum field"""
    class FooEnum(enum.Enum):
        FOO = 42
        BAR = 23

    field = EnumField(FooEnum)
    frag = serializers_meta._frag_enum_field({}, field)
    assert frag["enum"]
    assert 42 in frag["enum"]
    assert not 999 in frag["enum"]


def test__frag_pk_related_field():
    """Test pk related field frag"""
    field = serializers.PrimaryKeyRelatedField(read_only=True)
    frag = serializers_meta._frag_pk_related_field({}, field)
    assert frag["type"] == "string"


def test__frag_pk_related_field__many():
    """Test many pk related field frag"""
    field = serializers.PrimaryKeyRelatedField(read_only=True, many=True)
    frag = serializers_meta._frag_many_related_field({}, field)
    assert frag["type"] == "array"
    assert frag["items"]


def test__frag_date_time_field():
    """Test datetime field frag"""
    field = serializers.DateTimeField()
    frag = serializers_meta._frag_date_time_field({}, field)
    assert frag["type"] == "string"
    assert frag["format"]


def test__frag_json_field():
    """Test free form json field"""
    field = serializers.JSONField()
    frag = serializers_meta._frag_json_field({}, field)
    assert frag["type"] == "object"


def test__frag_boolean_field():
    """Test boolean field"""
    field = serializers.BooleanField()
    frag = serializers_meta._frag_boolean_field({}, field)
    assert frag["type"] == "boolean"


def test__frag_integer_field():
    """Test integer field"""
    field = serializers.IntegerField()
    frag = serializers_meta._frag_integer_field({}, field)
    assert frag["type"] == "integer"


def test__frag__char_field():
    """Test char field"""
    field = serializers.CharField()
    frag = serializers_meta._frag_char_field({}, field)
    assert frag["type"] == "string"


def test__frag_type():
    """Test fragment for field type"""
    fields = [
        serializers.IntegerField(),
        serializers.BooleanField(),
        serializers.CharField(),
        serializers.JSONField(),
        serializers.DateTimeField(),
        serializers.PrimaryKeyRelatedField(read_only=True),
        serializers.PrimaryKeyRelatedField(read_only=True, many=True),
    ]
    expected = [
        "integer", "boolean", "string", "object",
        "string", "string", "array",
    ]
    fragments = [serializers_meta._frag_type({}, field)["type"]
                 for field in fields]
    assert fragments == expected


#
# Integer hints
#
def test__int_format_hint():
    """Test deriving the format from bounds"""
    assert serializers_meta._int_format_hint(None, 23) == None
    assert serializers_meta._int_format_hint(23, None) == None
    assert serializers_meta._int_format_hint(None, None) == None

    assert serializers_meta._int_format_hint(0, 2**32-1) == "int64"
    assert serializers_meta._int_format_hint(0, 2**31-1) == "int32"
    assert serializers_meta._int_format_hint(-2**32, 2**31-1) == "int64"
    assert serializers_meta._int_format_hint(-2**31, 2**31-1) == "int32"
    assert serializers_meta._int_format_hint(-2**31-1, 0) == "int64"


#
# Serializers schema
#

def test_polymorphic_schemas():
    """Test generating schemas from a polymorphic serializer"""
    class FooSerializer(serializers.Serializer):
        a = serializers.CharField()

    class BarSerializer(serializers.Serializer):
        b = serializers.IntegerField()


    class PolySerializer(PolymorphicSerializer):
        serializer_classes = {
            "foo": FooSerializer,
            "bar": BarSerializer,
        }

    schemas = serializers_meta.polymorphic_schemas(PolySerializer)
    assert len(schemas) == 2


def test_polymorphic_schema_mapping():
    """Test generating the schema mapping"""
    class FooSerializer(serializers.Serializer):
        a = serializers.CharField()

    class BarSerializer(serializers.Serializer):
        b = serializers.IntegerField()

    class PolySerializer(PolymorphicSerializer):
        serializer_classes = {
            "foo": FooSerializer,
            "bar": BarSerializer,
        }

    mapping = serializers_meta.polymorphic_schema_mapping(PolySerializer)
    assert mapping["foo"] == "#/components/schemas/foo"
    assert mapping["bar"] == "#/components/schemas/bar"


def test_polymorphic_discriminator():
    """Test generating descriminator"""
    class FooSerializer(serializers.Serializer):
        a = serializers.CharField()

    class BarSerializer(serializers.Serializer):
        b = serializers.IntegerField()

    class PolySerializer(PolymorphicSerializer):
        serializer_classes = {
            "foo": FooSerializer,
            "bar": BarSerializer,
        }

    discriminator = serializers_meta.polymorphic_discriminator(
        PolySerializer)
    assert discriminator["mapping"]["foo"]


def test_serializer_ref_name():
    """Test serializer ref name"""
    class EntitySerializer:
        pass

    class ExchangeFooSerializer:
        pass

    class SomeClass:
        pass

    assert serializers_meta.serializer_ref_name(SomeClass) == "some-class"
    assert serializers_meta.serializer_ref_name(EntitySerializer) == "entity"
    assert serializers_meta.serializer_ref_name(ExchangeFooSerializer) == \
        "exchange-foo"


def test_serializer_title():
    """Test getting serializer title"""
    class FooSerializer:
        """test23"""

    assert serializers_meta.serializer_title(FooSerializer) == "Foo"


def test_serializer_description():
    """Test getting the description"""
    class FooSerializer:
        """test23"""

    assert serializers_meta.serializer_description(FooSerializer) == "test23"


def test_required_fieldnames():
    """Test getting a list of required field names"""
    class FooSerializer(serializers.Serializer):
        """Just a test serializer"""
        a = serializers.CharField(required=False)
        b = serializers.IntegerField(required=True)

    required_fields = serializers_meta.serializer_required_fieldnames(
        FooSerializer)
    assert "b" in required_fields
    assert not "a" in required_fields


def test_field_schemas():
    """Test getting the schema props"""
    class FooSerializer(serializers.Serializer):
        """Just a test serializer"""
        username = serializers.CharField(max_length=23)
        login_count = serializers.IntegerField(read_only=True)
        last_login = serializers.DateTimeField(read_only=True)

    properties = serializers_meta.serializer_properties(FooSerializer)

    assert properties['username']
    assert properties['login_count']
    assert properties['last_login']


