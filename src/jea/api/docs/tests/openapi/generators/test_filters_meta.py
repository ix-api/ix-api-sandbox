
"""
Test filter meta data generator
"""

import django_filters

from jea.access import filters as access_filters
from jea.access import models as access_models
from jea.api.docs.openapi.generators import filters_meta


def test_choices_enum():
    """Test enumerating all choices in a choice field"""
    TYPE_CHOICES = (
        (access_models.NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN,
         access_models.NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN),
        (access_models.NETWORK_SERVICE_CONFIG_TYPE_CLOUD,
         access_models.NETWORK_SERVICE_CONFIG_TYPE_CLOUD),
    )

    # Test with simple tuple choices
    choice_filter = django_filters.ChoiceFilter(
        method="filter_type",
        choices=TYPE_CHOICES)
    choices = choice_filter.extra['choices']
    choices_enum = filters_meta._choices_enum(choices)

    assert choices_enum == [
        access_models.NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN,
        access_models.NETWORK_SERVICE_CONFIG_TYPE_CLOUD]

def test_choice_enum_from_enum():
    """Test enumerating all enum choices"""
    some_filter = django_filters.CharFilter(choices=["foo", "bar"])
    choices = some_filter.extra['choices']
    choices_enum = filters_meta._choices_enum(choices)
    assert type(choices_enum[0]) == str


def test_frag_from_filter():
    """Test getting filter schema fragments"""
    filterset = access_filters.DemarcationPointFilter.get_filters()
    for name, filter_field in filterset.items():
        assert filters_meta.frag_from_filter(name, filter_field)
