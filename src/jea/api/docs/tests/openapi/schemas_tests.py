"""
Test schema generation
"""

from rest_framework import serializers

from jea.api.v1 import urls as v1_urls
from jea.api.docs.openapi import schema


def test_generator_get_schema():
    """Test get schema of schema generator"""
    gen = schema.Generator()
    links = gen.get_schema()
    assert links, "should not crash"


def test_generator_get_response_schemas():
    """Test getting the response schemas from a view"""
    class FooSerializer(serializers.Serializer):
        field = serializers.CharField()

    class View:
        responses = {
            "list": {
                200: FooSerializer(),
            },
        }

    view = View()
    gen = schema.Generator()
    schemas = gen.get_response_schemas(view, "list")
    assert schemas


def test_generator_get_response_schema():
    """Test getting a response schema from serializer"""
    class FooSerializer(serializers.Serializer):
        field = serializers.IntegerField()

    gen = schema.Generator()
    response = gen.get_response_schema(
        FooSerializer(),
        "description")

    assert response["description"]
    assert response["content"]["application/json"]["schema"]
    assert "foo" in response["content"]["application/json"]["schema"]["$ref"]


def test_generator_get_response_description():
    """Test getting the description from a view"""
    class FooSerializer:
        """foo response"""

    class View:
        responses = {
            "read": {
                200: FooSerializer(),
            }
        }

    view = View()
    gen = schema.Generator()

    assert gen.get_response_description(view, "read", 200) == \
        "foo response" 


def test_generator_get_request_schema():
    """Test request schema generation"""
    class FooSerializer(serializers.Serializer):
        field = serializers.CharField()

    class View:
        responses = {
            "create": {
                "request": FooSerializer(),
            },
        }

    view = View()
    gen = schema.Generator()
    request_schema = gen.get_request_schema(view, "create")
    assert request_schema 


