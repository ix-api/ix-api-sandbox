"""
Test serializers reflection
"""

from jea.api import v1
from jea.api.v1.access import serializers as access_serializers
from jea.api.v1.catalog import serializers as catalog_serializers
from jea.api.docs.openapi.reflection import (
    serializers as serializers_reflection,
)


def test__find_module_serializers():
    """Find all serializers in a module"""
    serializers = serializers_reflection._find_module_serializers(
        access_serializers)
    assert access_serializers.NetworkServiceConfigSerializer in serializers

def test_find_serializers():
    """Find all serializers for the api"""
    serializers = serializers_reflection.find_serializers(v1)
    assert access_serializers.NetworkServiceConfigSerializer in serializers
    assert catalog_serializers.FacilitySerializer in serializers

def test_get_schemas():
    """Create openapi v3 schemas"""
    schemas = serializers_reflection.get_schemas(v1)
    assert schemas

