
"""
Test problem reflection
"""

from jea.api.docs.problems import reflection


def test_find_problems():
    """Find all problems in a given package"""
    from jea.api import v1
    problems = reflection.find_problems(v1)
    assert problems

