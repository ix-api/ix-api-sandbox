
"""
Access Event Creators
"""

from jea.eventmachine.models import Event

DEMARC_REQUESTED = "@access/demarc_requested"
DEMARC_ALLOCATED = "@access/demarc_allocated"
DEMARC_RELEASED = "@access/demarc_released"
DEMARC_CONNECTED = "@access/demarc_connected"
DEMARC_DISCONNECTED = "@access/demarc_disconnected"
DEMARC_UPDATED = "@access/demarc_updated"
DEMARC_STATE_CHANGED = "@access/demarc_state_changed"

CONNECTION_CREATED = "@access/connection_created"
CONNECTION_UPDATED = "@access/connection_updated"
CONNECTION_DESTROYED = "@access/connection_destroyed"
CONNECTION_STATE_CHANGED = "@access/connection_state_changed"

NETWORK_SERVICE_CONFIG_CREATED = \
    "@access/network_service_config_created"
NETWORK_SERVICE_CONFIG_UPDATED = \
    "@access/network_service_config_updated"
NETWORK_SERVICE_CONFIG_DESTROYED = \
    "@access/network_service_config_destroyed"
NETWORK_SERVICE_CONFIG_STATE_CHANGED = \
    "@access/network_service_config_state_changed"

NETWORK_FEATURE_CONFIG_CREATED = \
    "@access/network_feature_config_created"
NETWORK_FEATURE_CONFIG_UPDATED = \
    "@access/network_feature_config_updated"
NETWORK_FEATURE_CONFIG_DESTROYED = \
    "@access/network_feature_config_destroyed"
NETWORK_FEATURE_CONFIG_STATE_CHANGED = \
    "@access/network_feature_config_state_changed"


#
# Port Demarcation Points
#
def demarc_requested(demarc):
    """Make demarc requested event"""
    return Event(
        type=DEMARC_REQUESTED,
        payload={
            "consuming_customer_id": demarc.consuming_customer_id,
            "managing_customer_id": demarc.managing_customer_id,
            "demarc_id": demarc.pk,
        },
        ref=demarc,
        customer=demarc.scoping_customer,
    )


def demarc_allocated(demarc):
    """Make demarc allocated event"""
    return Event(
        type=DEMARC_ALLOCATED,
        payload={
            "consuming_customer_id": demarc.consuming_customer_id,
            "managing_customer_id": demarc.managing_customer_id,
            "demarc_id": demarc.pk,
        },
        ref=demarc,
        customer=demarc.scoping_customer,
    )


def demarc_released(demarc):
    """Make port demarc released event"""
    return Event(
        type=DEMARC_RELEASED,
        payload={
            "consuming_customer_id": demarc.consuming_customer_id,
            "managing_customer_id": demarc.managing_customer_id,
            "demarc_id": demarc.pk,
        },
        ref=None,
        customer=demarc.scoping_customer,
    )


def demarc_updated(demarc):
    """Make demarcation point updated event"""
    return Event(
        type=DEMARC_UPDATED,
        payload={
            "consuming_customer_id": demarc.consuming_customer_id,
            "managing_customer_id": demarc.managing_customer_id,
            "demarc_id": demarc.pk,
        },
        ref=demarc,
        customer=demarc.scoping_customer,
    )


def demarc_state_changed(demarc, prev_state, next_state=None):
    """Emit demarc state changed event"""
    if not next_state:
        next_state = demarc.state

    return Event(
        type=DEMARC_STATE_CHANGED,
        payload={
            "demarc_id": demarc.pk,
            "next_state": next_state.value,
            "prev_state": prev_state.value,
        },
        ref=demarc,
        customer=demarc.scoping_customer,
    )


def demarc_connected(demarc, connection):
    """Create port connected event"""
    return Event(
        type=DEMARC_CONNECTED,
        payload={
            "demarc_id": demarc.pk,
            "connection_id": connection.pk,
        },
        ref=demarc,
        customer=demarc.scoping_customer,
    )


def demarc_disconnected(demarc, connection):
    """Create port disconnected event"""
    return Event(
        type=DEMARC_DISCONNECTED,
        payload={
            "demarc_id": demarc.pk,
            "connection_id": connection.pk,
        },
        ref=demarc,
        customer=demarc.scoping_customer,
    )

#
# Connecctions
#
def connection_created(connection):
    """Create connection created event"""
    return Event(
        type=CONNECTION_CREATED,
        payload={
            "connection_id": connection.pk,
        },
        ref=connection,
        customer=connection.scoping_customer,
    )


def connection_updated(connection):
    """Create updated connection event"""
    return Event(
        type=CONNECTION_UPDATED,
        payload={
            "connection_id": connection.pk,
        },
        ref=connection,
        customer=connection.scoping_customer,
    )


def connection_destroyed(connection):
    """Create a connection destroyed event"""
    return Event(
        type=CONNECTION_DESTROYED,
        payload={
            "connection_id": connection.pk,
        },
        ref=None,
        customer=connection.scoping_customer,
    )


def connection_state_changed(connection, prev_state, next_state=None):
    """Create connection state changed event"""
    # Infer next state from object
    if not next_state:
        next_state = connection.state

    return Event(
        type=CONNECTION_STATE_CHANGED,
        payload={
            "connection_id": connection.pk,
            "prev_state": prev_state.value,
            "next_state": next_state.value,
        },
        ref=connection,
        customer=connection.scoping_customer,
    )

#
# Configs
#
def network_service_config_created(config):
    """Service configuration created event"""
    return Event(
        type=NETWORK_SERVICE_CONFIG_CREATED,
        payload={
            "network_service_config_id": config.pk,
        },
        ref=config,
        customer=config.scoping_customer,
    )


def network_service_config_updated(config):
    """Service configuration was updated event"""
    return Event(
        type=NETWORK_SERVICE_CONFIG_UPDATED,
        payload={
            "network_service_config_id": config.pk,
        },
        ref=config,
        customer=config.scoping_customer,
    )


def network_service_config_destroyed(config):
    """Service deconfigured event"""
    return Event(
        type=NETWORK_SERVICE_CONFIG_DESTROYED,
        payload={
            "network_service_config_id": config.pk,
        },
        customer=config.scoping_customer,
    )


def network_service_config_state_changed(config, prev_state, next_state=None):
    """The state of the configuration changed"""
    if not next_state:
        next_state = config.state

    return Event(
        type=NETWORK_SERVICE_CONFIG_STATE_CHANGED,
        payload={
            "network_service_config_id": config.pk,
            "prev_state": prev_state.value,
            "next_state": next_state.value,
        },
        ref=config,
        customer=config.scoping_customer,
    )


def network_feature_config_created(config):
    """Feature configuration created event"""
    return Event(
        type=NETWORK_FEATURE_CONFIG_CREATED,
        payload={
            "network_feature_config_id": config.pk,
        },
        ref=config,
        customer=config.scoping_customer,
    )


def network_feature_config_updated(config):
    """Feature was updated event"""
    return Event(
        type=NETWORK_FEATURE_CONFIG_UPDATED,
        payload={
            "network_feature_config_id": config.pk,
        },
        ref=config,
        customer=config.scoping_customer,
    )


def network_feature_config_destroyed(config):
    """Service deconfigured event"""
    service_config_id = None
    if config.network_service_config:
        service_config_id = config.network_service_config.pk

    return Event(
        type=NETWORK_FEATURE_CONFIG_DESTROYED,
        payload={
            "network_feature_config_id": config.pk,
            "network_service_config_id": service_config_id,
        },
        customer=config.scoping_customer,
    )


def network_feature_config_state_changed(config, prev_state, next_state=None):
    """The state of the configuration changed"""
    if not next_state:
        next_state = config.state

    return Event(
        type=NETWORK_FEATURE_CONFIG_STATE_CHANGED,
        payload={
            "network_feature_config_id": config.pk,
            "prev_state": prev_state.value,
            "next_state": next_state.value,
        },
        ref=config,
        customer=config.scoping_customer,
    )


