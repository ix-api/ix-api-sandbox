
"""
Status Message Creators
"""

from jea.eventmachine.models import StatusMessage, Severity


MAC_ADDRESS_MISSING = "mac_address_missing"
REQUIRED_NETWORK_FEATURE_MISSING = \
    "required_network_feature_missing"

DEMARCS_NOT_PRESENT = "demarcs_not_present"
DEMARCS_NOT_PRODUCTION = "demarcs_not_production"

CONNECTION_NOT_PRODUCTION = "connection_state_not_production"

def mac_address_missing(config):
    """Create mac address missing status"""
    return StatusMessage(
        ref=config,
        severity=Severity.CRITICAL, # Blocker,
        message="At least one active mac address is required",
        tag=MAC_ADDRESS_MISSING)


def route_server_network_feature_missing(config, feature):
    """Create route server network feature missing status"""
    attrs = {
        "network_feature_type": feature.polymorphic_ctype.name,
        "network_feature_name": feature.name,
        "network_feature_id": feature.pk,
    }
    message = ("network-feature-config missing for required feature: "
               "{network_feature_type} {network_feature_name} "
               "id={network_feature_id}").format(**attrs)

    return StatusMessage(
        ref=config,
        severity=Severity.CRITICAL, # Blocker
        tag=REQUIRED_NETWORK_FEATURE_MISSING,
        attrs=attrs,
        message=message)


def connection_has_nonproduction_demarcs(connection, demarcs):
    """Some demarcs in the connection are not in state production"""
    attrs = {
        "demarcs": [d.pk for d in demarcs],
    },
    message = ("Not all demarcs in the connection are "
               "in the PRODUCTION state.")
    return StatusMessage(
        ref=connection,
        severity=Severity.ERROR,
        tag=DEMARCS_NOT_PRODUCTION,
        attrs=attrs,
        message=message)


def connection_has_no_demarcs(connection):
    """The connection just does not have any demarcs (yet)"""
    message = "The connection does not have any demarcs, yet."

    return StatusMessage(
        ref=connection,
        severity=Severity.WARNING,
        tag=DEMARCS_NOT_PRESENT,
        attrs={},
        message=message)


def config_connection_not_production(config, connection=None):
    """The referenced connection is not in production"""
    if not connection:
        connection = config.connection

    message = ("Waiting for the connection state to become `production`")
    return StatusMessage(
        ref=config,
        severity=Severity.ERROR,
        tag=CONNECTION_NOT_PRODUCTION,
        attrs={
            "connection_id": connection.pk,
            "network_service_config_id": config.pk,
        },
        message=message)


