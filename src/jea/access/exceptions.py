
"""
Access Exception
----------------

All exceptions that describe anything that could go
wrong when dealing with access objects.
"""

from jea.exceptions import ValidationError

#
# Port Demarcation Points
#
class DemarcationPointNotReady(ValidationError):
    default_detail = ("The demarcation point's state is not ALLOCATED "
                      "or REQUESTED.")
    default_code = "demarc_not_ready"
    default_field = "connection"


class DemarcationPointUnavailable(ValidationError):
    default_detail = ("The demarcation point can not be "
                      "used in this configuration at this location.")
    default_code = "demarc_unavailable"
    default_field = "connection"


class DemarcationPointInUse(ValidationError):
    default_detail = ("The demarcation point is still in use by "
                      "a connection and can not be released.")
    default_code = "demarc_in_use"
    default_field = "connection"


class DemarcationPointNotConnected(ValidationError):
    default_detail = ("The demarcation point is not connected")
    default_code = "demarc_not_connected"
    default_field = "connection"


class ConnectionModeUnavailable(ValidationError):
    default_detail = ("The connection can not assume the requested mode."),
    default_code = "connection_mode_unavailable"
    default_field = "mode"


class SessionModeInvalid(ValidationError):
    default_detail = ("The session_mode of the config is not valid.")
    default_code = "session_mode_invalid"
    default_field = "session_mode"
