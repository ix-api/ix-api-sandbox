
"""
Network Service Access Configurations
"""

import logging

from jea.eventmachine import active
from jea.eventmachine.models import State, StatusMessage
from jea.access.events import (
    NETWORK_SERVICE_CONFIG_CREATED,
    NETWORK_SERVICE_CONFIG_DESTROYED,
    NETWORK_SERVICE_CONFIG_UPDATED,
    NETWORK_FEATURE_CONFIG_CREATED,
    NETWORK_FEATURE_CONFIG_DESTROYED,
    NETWORK_FEATURE_CONFIG_UPDATED,

    CONNECTION_STATE_CHANGED,

    network_service_config_state_changed,
    network_feature_config_state_changed,
)
from jea.access.status import (
    mac_address_missing,
    route_server_network_feature_missing,
    config_connection_not_production,
)
from jea.access import models as access_models
from jea.ipam import models as ipam_models
from jea.service.services import network as network_svc


logger = logging.getLogger(__name__)
logger.info("Initializing statemachine: access.configs")


def _network_service_configs_from_event(event):
    """
    Resolve network service config(s) from event.
    """
    # Resolve from payload
    network_service_config_id = event.payload.get(
        "network_service_config_id", event.payload.get(
            "network_service_config"))
    if network_service_config_id:
        return [access_models.NetworkServiceConfig.objects.get(
            pk=network_service_config_id)]

    # Resolve by ref
    ref = event.ref
    if not ref:
        return []

    if isinstance(ref, access_models.Connection):
        return ref.network_service_configs.all()

    if isinstance(ref, access_models.NetworkServiceConfig):
        return [ref]

    if isinstance(ref, access_models.NetworkFeatureConfig):
        return [ref.network_service_config]

    return []


#
# Entry point is a created network service config
#
# The state is calculated based on the service config
# type. Implemented is the ExchangeLanNetworkServiceConfig
#
@active.handler(
    NETWORK_SERVICE_CONFIG_CREATED,
    NETWORK_SERVICE_CONFIG_UPDATED,
    NETWORK_FEATURE_CONFIG_CREATED,
    NETWORK_FEATURE_CONFIG_UPDATED,
    NETWORK_FEATURE_CONFIG_DESTROYED,

    CONNECTION_STATE_CHANGED,
)
def next_network_service_config_state(dispatch, event):
    """Initialize network service state"""
    configs = _network_service_configs_from_event(event)
    if not configs:
        return

    logger.info("Processing event:")
    logger.info(event)

    for config in configs:
        if isinstance(config,
                      access_models.ExchangeLanNetworkServiceConfig):
            _next_exchange_lan_network_service_config_state(
                dispatch, config)


def _next_exchange_lan_network_service_config_state(
        dispatch,
        config,
    ):
    """
    Calculate next exchange lan network service config state.
    """
    logger.debug(
        "Calculating next exchange lan network service config state")

    # Reset status
    config.status.clear()
    prev_state = config.state
    next_state = State.PRODUCTION

    # Validate exchange lan config:
    # - Is a mac address present?
    if not config.mac_addresses.all():
        next_state = State.ERROR
        status_message = mac_address_missing(config)
        status_message.save()

    # Get all required network service features
    required_features = network_svc.get_network_features(filters={
        "network_service": config.network_service,
        "required": True,
    })
    for feature in required_features:
        # Create an error message, in case the feature config
        # is missing.
        if not config.network_feature_configs.filter(
            network_feature=feature):
            next_state = State.ERROR
            status_message = route_server_network_feature_missing(
                config, feature)
            status_message.save()

    # Check connection state
    if config.connection and config.connection.state != State.PRODUCTION:
        if next_state != State.ERROR:
            next_state = State.TESTING # IF everything else is allright
        msg = config_connection_not_production(config)
        msg.save()

    # Set next state
    config.state = next_state
    config.save()

    # Inform subscribers
    if next_state != prev_state:
        dispatch(network_service_config_state_changed(
            config, prev_state=prev_state))


@active.handler(
    NETWORK_FEATURE_CONFIG_CREATED,
)
def next_network_feature_config_state(dispatch, event):
    """Initialize network feature state"""
    config = event.ref
    prev_state = config.state
    next_state = State.PRODUCTION
    config.state = next_state
    config.save()

    if prev_state != next_state:
        dispatch(network_feature_config_state_changed(
            config,
            prev_state=prev_state,
            next_state=next_state))


