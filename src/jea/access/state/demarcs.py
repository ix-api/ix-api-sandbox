"""
Demarcation Point Event Handler

This implementes the state machine for (port)
demarcation points.
"""

import logging

from jea.eventmachine import active
from jea.eventmachine.models import State
from jea.access.events import (
    DEMARC_REQUESTED,
    DEMARC_ALLOCATED,
    DEMARC_RELEASED,
    DEMARC_CONNECTED,
    DEMARC_DISCONNECTED,

    DEMARC_STATE_CHANGED,

    demarc_state_changed,
)
from jea.access.tasks import demarc as demarc_tasks


logger = logging.getLogger(__name__)

logger.info("Initializing statemachine: access.demarcs")


# Initialize port demarc state transitions
@active.handler(
    DEMARC_REQUESTED
)
def demarc_state_initialize(dispatch, event):
    """Initializedemarcation point state"""
    # Dispatch async allocation
    demarc = event.ref
    demarc_tasks.set_demarc_state(
        demarc,
        State.ALLOCATED,
        2.0)


@active.handler(
    DEMARC_CONNECTED,
    DEMARC_DISCONNECTED,
)
def demarc_state_update(dispatch, event):
    """
    Update demarc state
    """
    demarc = event.ref
    prev_state = demarc.state

    # Clear status messages, for errors will recreate them
    # during the state update.
    demarc.status.clear()

    # Is the demarc connected?
    if not demarc.connection:
        demarc.state = State.ALLOCATED
    else:
        # Update demarc state deferred.
        demarc_tasks.set_demarc_state(
            demarc,
            State.PRODUCTION,
            1.5)

    # Persist changes
    demarc.save()

    if prev_state != demarc.state:
        dispatch(demarc_state_changed(demarc, prev_state))

