
"""
Connection State Reducers
"""

import logging
from typing import Optional

from jea.eventmachine import active
from jea.eventmachine.models import State
from jea.access import models as access_models
from jea.access import status as status
from jea.access.tasks import connection as connection_tasks
from jea.access.events import (
    CONNECTION_CREATED,
    CONNECTION_DESTROYED,

    DEMARC_CONNECTED,
    DEMARC_DISCONNECTED,
    DEMARC_STATE_CHANGED,

    connection_state_changed,
)


logger = logging.getLogger(__name__)
logger.info("Initializing statemachine: access.connections")


# Helper: Get connection from polymorphic event
def _connection_from_event(event) -> Optional[access_models.Connection]:
    """Try to get a connection from the event"""
    if isinstance(event.ref, access_models.Connection):
        return event.ref # Well. Neat.

    if isinstance(event.ref, access_models.DemarcationPoint):
        if event.ref.connection:
            return event.ref.connection

    connection_id = event.payload.get(
        "connection",
        event.payload.get("connection_id"))
    if not connection_id:
        return None

    return access_models.Connection.objects.get(pk=connection_id)


@active.handler(
    CONNECTION_CREATED,
)
def connection_state_initialize(dispatch, event):
    """
    Update connection state after a connection
    was created.
    """
    connection = event.ref
    prev_state = connection.state

    connection.state = State.REQUESTED
    connection.save()

    msg = status.connection_has_no_demarcs(connection)
    msg.save()

    dispatch(connection_state_changed(connection, prev_state))


@active.handler(
    DEMARC_CONNECTED,
    DEMARC_DISCONNECTED,
    DEMARC_STATE_CHANGED,
)
def connection_state_update(dispatch, event):
    """
    Update connection state.
    """
    connection = _connection_from_event(event)
    if not connection:
        return # Nothing to do here

    prev_state = connection.state
    # Clear status messages
    connection.status.clear()

    # Check if we are production ready
    not_ready_demarcs = connection \
        .demarcation_points \
        .exclude(state=State.PRODUCTION)
    if not_ready_demarcs:
        msg = status.connection_has_nonproduction_demarcs(
            connection, not_ready_demarcs)
        msg.save()

    # Check if there are no demarcs at all
    elif not connection.demarcation_points.exists():
        connection.state = State.REQUESTED
        msg = status.connection_has_no_demarcs(connection)
        msg.save()

    else:
        # Everything seems fine! Let's put it in production.
        # After some waiting time...
        connection_tasks.set_connection_state(
            connection,
            State.PRODUCTION,
            wait=2.23)

    connection.save()
    if connection.state != prev_state:
        dispatch(connection_state_changed(connection, prev_state))

