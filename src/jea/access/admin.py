
"""
Service Access Model Admins
---------------------------

Provide a basic admin interfaces for access models.
"""

from jea.admin import site
from django.contrib import admin

from jea.access.models import (
    Connection,
    DemarcationPoint,
    ExchangeLanNetworkServiceConfig,
    ClosedUserGroupNetworkServiceConfig,
    ELineNetworkServiceConfig,
    CloudNetworkServiceConfig,
    RouteServerNetworkFeatureConfig,
    IXPRouterNetworkFeatureConfig,
    BlackholingNetworkFeatureConfig,
)


#
# Connections and Demarcs
#
class ConnectionAdmin(admin.ModelAdmin):
    pass

class DemarcAdmin(admin.ModelAdmin):
    pass

#
# Inline Feature Config
#
class RouteServerNetworkFeatureConfigInline(admin.StackedInline):
    model = RouteServerNetworkFeatureConfig
    extra = 0


class IXPRouterNetworkFeatureConfigInline(admin.StackedInline):
    model = IXPRouterNetworkFeatureConfig
    extra = 0

class BlackholingNetworkFeatureConfigInline(admin.StackedInline):
    model = BlackholingNetworkFeatureConfig
    extra = 0

#
# Service Config
#
class ExchangeLanNetworkServiceConfigAdmin(admin.ModelAdmin):
    inlines = (
        RouteServerNetworkFeatureConfigInline,
        IXPRouterNetworkFeatureConfigInline,
        BlackholingNetworkFeatureConfigInline,
    )

class ClosedUserGroupNetworkServiceConfigAdmin(admin.ModelAdmin):
    inlines = (
        IXPRouterNetworkFeatureConfigInline,
        BlackholingNetworkFeatureConfigInline,
    )

class ELineNetworkServiceConfigAdmin(admin.ModelAdmin):
    pass

class CloudNetworkServiceConfigAdmin(admin.ModelAdmin):
    pass


# Register models
site.register(Connection, ConnectionAdmin)
site.register(DemarcationPoint, DemarcAdmin)

site.register(ExchangeLanNetworkServiceConfig,
              ExchangeLanNetworkServiceConfigAdmin)
site.register(ClosedUserGroupNetworkServiceConfig,
              ClosedUserGroupNetworkServiceConfigAdmin)
site.register(ELineNetworkServiceConfig,
              ExchangeLanNetworkServiceConfigAdmin)
site.register(CloudNetworkServiceConfig,
              CloudNetworkServiceConfigAdmin)

