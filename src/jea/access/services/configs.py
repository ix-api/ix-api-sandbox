
"""
Configs Service
---------------

Configure service accesses and features like
route servers and blackholing.
"""

import secrets
import random
import itertools
from typing import List

from django.db import transaction

from jea.exceptions import ResourceAccessDenied, ValidationError
from jea.crm.exceptions import RequiredContactTypesInvalid
from jea.access.exceptions import (
    SessionModeInvalid,
)
from jea.eventmachine import active
from jea.eventmachine.models import State
from jea.service import models as service_models
from jea.crm.services import (
    customers as customers_svc,
    contacts as contacts_svc,
)
from jea.crm import models as crm_models
from jea.ipam import models as ipam_models
from jea.ipam.services import (
    ip_addresses as ip_addresses_svc,
    mac_addresses as mac_addresses_svc,
)
from jea.service.services import (
    network as network_svc,
)
from jea.access.services import (
    connections as connections_svc,
)
from jea.access import (
    models as access_models,
    filters as access_filters,
)
from jea.access.models import (
    RouteServerSessionMode,
)
from jea.access.events import (
    network_service_config_created,
    network_service_config_destroyed,
    network_service_config_updated,
    network_service_config_state_changed,

    network_feature_config_created,
    network_feature_config_updated,
    network_feature_config_destroyed,
    network_feature_config_updated,
)
from utils.datastructures import whitelist_filter_fields


#TODO maybe merge with the below method
@transaction.atomic
def assign_network_feature_contacts(
        scoping_customer=None,
        network_feature_config=None,
        contacts=None,
    ):
    """
    Assign contacts to a config object.
    Providing a list of required contacts, will assert the presence
    of at least one of the required contact types.

    :param network_feature_config: A network feature configuration,
        where the contacts can be assigned to.
    :param scoping_customer: Override config managing customer property
        when checking the ownership of the contact.
    :param contacts: A list of contact ids or contact objects.

    :raises RequiredContactTypesMissing:
    :raises ValidationError: In case a config object is missing
    """
    # Apply permission check and resovlve object if required.
    config = get_network_feature_config(
        scoping_customer=scoping_customer,
        network_feature_config=network_feature_config)

    # Now, do we have our config?
    if not config:
        raise ValidationError("A config object is required")

    if not scoping_customer:
        scoping_customer = config.scoping_customer

    if not config.network_feature:
        raise ValidationError(
            "The config requires a network_feature.",
            field="network_service")

    # Load all contacts
    if not contacts:
        contacts = []
    contacts = [contacts_svc.get_contact(
                    contact=c,
                    scoping_customer=scoping_customer)
                for c in contacts]

    # Check the required contacts are included
    contacts_svc.assert_presence_of_contact_types(
        config.network_feature.required_contact_types,
        contacts)

    # Reset contacts
    config.contacts.clear()

    if not contacts:
        return config # Nothing else to do

    # Assign contacts
    for contact in contacts:
        config.contacts.add(contact)

    return config



@transaction.atomic
def assign_network_service_contacts(
        scoping_customer=None,
        network_service_config=None,
        contacts=None,
    ):
    """
    Assign contacts to a config object.
    Providing a list of required contacts, will assert the presence
    of at least one of the required contact types.

    :param network_service_config: A network service configuration,
        the implementation contacts can be assigned to.
    :param scoping_customer: Override config managing customer property
        when checking the ownership of the contact.
    :param contacts: A list of contact ids or contact objects.

    :raises RequiredContactTypesMissing:
    :raises ValidationError: In case a config object is missing
    """
    # Apply permission check and resovlve object if required.
    config = get_network_service_config(
        scoping_customer=scoping_customer,
        network_service_config=network_service_config)

    # Now, do we have our config?
    if not config:
        raise ValidationError("A config object is required")

    if not scoping_customer:
        scoping_customer = config.scoping_customer

    if not config.network_service:
        raise ValidationError(
            "The config requires a network_service.",
            field="network_service")

    # Load all contacts
    if not contacts:
        contacts = []
    contacts = [contacts_svc.get_contact(
                    contact=c,
                    scoping_customer=scoping_customer)
                for c in contacts]

    # Check the required contacts are included
    contacts_svc.assert_presence_of_contact_types(
        config.network_service.required_contact_types,
        contacts)

    # Reset contacts
    config.contacts.clear()

    if not contacts:
        return config # Nothing else to do

    # Assign contacts
    for contact in contacts:
        config.contacts.add(contact)

    return config


@transaction.atomic
def assign_mac_addresses(
        scoping_customer=None,
        network_service_config=None,
        mac_addresses=None
    ):
    """
    Assign mac address to supported service configurations.
    Currently the only supported config types are:

      - ExchangeLanNetworkServiceConfig
      - ClosedUserGroupNetworkServiceConfig

    :param network_service_config: The network service config to assign macs to
    :param mac_addresses: A list of mac address objects.
    """
    if not hasattr(network_service_config, "mac_addresses"):
        raise TypeError("Config object does not have mac_addresses")

    if not scoping_customer:
        scoping_customer = network_service_config.scoping_customer

    # Reset mac addresses
    network_service_config.mac_addresses.clear()

    if not mac_addresses:
        return network_service_config # Nothing left to do here

    # Assign mac addresses
    for mac in mac_addresses:
        mac = mac_addresses_svc.get_mac_address(
            scoping_customer=scoping_customer,
            mac_address=mac)

        network_service_config.mac_addresses.add(mac)

    return network_service_config


def get_network_service_configs(
        scoping_customer=None,
        filters=None,
    ):
    """
    Get all service configurations within a customer context.

    :param scoping_customer: Limit the scope to the managing customer.
    :param filters: A dict of filter params
    """
    configs = access_filters.NetworkServiceConfigFilter(filters).qs

    # Apply ownership filtering
    if scoping_customer:
        configs = configs.filter(scoping_customer=scoping_customer)

    return configs


def get_network_service_config(
        scoping_customer=None,
        network_service_config=None,
    ):
    """
    Get a single network service configuration.
    If billing or owning customer are present, perform an
    ownership test.

    :param config: The config to load. Might be an ID.
    :param managing_customer: The billing customer
    :param consuming_customer: The owning customer of the config.
    """
    if isinstance(network_service_config, access_models.NetworkServiceConfig):
        # Just perform an ownership test
        if scoping_customer and not \
            network_service_config.scoping_customer == scoping_customer:
            raise ResourceAccessDenied(network_service_config)

        return network_service_config

    # Otherwise get configs queryset:
    configs = get_network_service_configs(
        scoping_customer=scoping_customer)

    return configs.get(pk=network_service_config)


@active.command
def create_network_service_config(
        dispatch,
        scoping_customer=None,
        network_service_config_input=None,
    ):
    """
    Create a new network service config.

    :param scoping_customer: The managing customer
    :param config_input: The configuration data
    """
    # Check config input
    if not isinstance(network_service_config_input, dict):
        raise ValidationError("Invalid configuration input")

    # Load networkservice and connection and check access permissions.
    network_service = network_svc.get_network_service(
        scoping_customer=scoping_customer,
        network_service=network_service_config_input["network_service"])
    connection = connections_svc.get_connection(
        scoping_customer=scoping_customer,
        connection=network_service_config_input["connection"])

    # Dispatch network service configuration creation
    if network_service_config_input["type"] == \
        access_models.NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN:
        config = create_exchange_lan_network_service_config(
            scoping_customer,
            network_service,
            connection,
            network_service_config_input)
    elif network_service_config_input["type"] == \
        access_models.NETWORK_SERVICE_CONFIG_TYPE_CLOSED_USER_GROUP:
        config = create_closed_user_group_network_service_config(
            scoping_customer,
            network_service,
            connection,
            network_service_config_input)
    elif network_service_config_input["type"] == \
        access_models.NETWORK_SERVICE_CONFIG_TYPE_ELINE:
        config = create_eline_network_service_config(
            scoping_customer,
            network_service,
            connection,
            network_service_config_input)
    elif network_service_config_input["type"] == \
        access_models.NETWORK_SERVICE_CONFIG_TYPE_CLOUD:
        config = create_cloud_network_service_config(
            scoping_customer,
            network_service,
            connection,
            network_service_config_input)
    else:
        raise ValidationError("Unknown network service: {}".format(
            network_service_config_input["type"]))

    dispatch(network_service_config_created(config))

    # Reload model after state machine transition
    config.refresh_from_db()

    return config


def create_eline_network_service_config(
        scoping_customer,
        network_service,
        connection,
        config_input,
    ):
    """
    Create a new config for the eline service.

    :param managing_customer: The billing customer
    :param consuming_customer: The owning customer of the config.
    :param network_servie: The network service to configure
    :param connection: The connection to configure for the network service
    :param config_input: The configuration data
    """
    # Check if the service type is correct
    if not isinstance(network_service, service_models.ELineNetworkService):
        raise ValidationError(
            "Service type must match config type. {} given, expected {}" \
                .format(network_service.__class__, "ELineNetworkService"))

    # Check permissions on related objects
    managing_customer = customers_svc.get_customer(
        scoping_customer=scoping_customer,
        customer=config_input.get("managing_customer"))
    consuming_customer = customers_svc.get_customer(
        scoping_customer=scoping_customer,
        customer=config_input.get("consuming_customer"))

    # Get outer vlan form config.
    outer_vlan = 42

    # Create the eline config
    with transaction.atomic():
        config = access_models.ELineNetworkServiceConfig(
            state=State.REQUESTED,
            inner_vlan=config_input["inner_vlan"],
            outer_vlan=outer_vlan,
            capacity=config_input["capacity"],
            purchase_order=config_input["purchase_order"],
            external_ref=config_input["external_ref"],
            contract_ref=config_input["contract_ref"],
            network_service=network_service,
            scoping_customer=scoping_customer,
            consuming_customer=consuming_customer,
            managing_customer=managing_customer,
            connection=connection)
        config.save()

        # Assign many to many implementation contacts
        if "contacts" in config_input.keys():
            assign_network_service_contacts(
                scoping_customer=scoping_customer,
                network_service_config=config,
                contacts=config_input["contacts"])

    return config


def create_exchange_lan_network_service_config(
        scoping_customer,
        network_service,
        connection,
        config_input,
    ):
    """
    Create a new config for the exchange lan network service.

    :param network_service: The network service to configure
    :param connection: The connection to configure for the network service
    :param scoping_customer: The owning customer of the config.
    :param config_input: The configuration data
    """
    # Check if the service type is correct
    if not isinstance(network_service,
                      service_models.ExchangeLanNetworkService):
        raise ValidationError(
            "Service type must match config type. {} given, expected {}" \
                .format(network_service.__class__,
                        "ExchangeLanNetworkService"))

    # Check mac addresses
    mac_addresses = config_input.get("mac_addresses", [])

    # Check permissions on related objects
    managing_customer = customers_svc.get_customer(
        scoping_customer=scoping_customer,
        customer=config_input.get("managing_customer"))
    consuming_customer = customers_svc.get_customer(
        scoping_customer=scoping_customer,
        customer=config_input.get("consuming_customer"))

    # Get outer vlan from range set
    outer_vlans = config_input["outer_vlan"]
    if not outer_vlans:
        raise ValidationError(
            "Please provide a non empty set of vlan ranges.",
            field="outer_vlan")

    outer_vlan_values = list(itertools.chain.from_iterable(outer_vlans))
    outer_vlan = outer_vlan_values[
        random.randint(0, len(outer_vlan_values) - 1)]

    # Create the Exchange Lan Network Access Configuration
    with transaction.atomic():
        config = access_models.ExchangeLanNetworkServiceConfig(
            state=State.REQUESTED,
            asns=config_input["asns"],
            inner_vlan=config_input["inner_vlan"],
            outer_vlan=outer_vlan,
            capacity=config_input["capacity"],
            purchase_order=config_input["purchase_order"],
            external_ref=config_input["external_ref"],
            contract_ref=config_input["contract_ref"],
            network_service=network_service,
            scoping_customer=scoping_customer,
            consuming_customer=consuming_customer,
            managing_customer=managing_customer,
            connection=connection)
        config.save()

        # Assign mac address(es)
        if mac_addresses:
            assign_mac_addresses(
                scoping_customer=scoping_customer,
                network_service_config=config,
                mac_addresses=mac_addresses)

        # Allocate ip addresses
        ip_addresses_svc.allocate_ip_address(
            scoping_customer=scoping_customer,
            network_service_config=config,
            version=4)

        if network_service.ip_addresses.filter(version=6):
            ip_addresses_svc.allocate_ip_address(
                scoping_customer=scoping_customer,
                network_service_config=config,
                version=6)

        # Assign many to many contacts
        if "contacts" in config_input.keys():
            assign_network_service_contacts(
                scoping_customer=scoping_customer,
                network_service_config=config,
                contacts=config_input["contacts"])

    return config


def create_closed_user_group_network_service_config(
        scoping_customer,
        network_service,
        connection,
        config_input,
    ):
    """
    Create a new config or a closed user group service.

    :param network_service: The network service to configure
    :param connection: The connection to configure for the network service
    :param scoping_customer: The customer managing the config
    :param config_input: The configuration data
    """
    # Check if the service type is correct
    if not isinstance(network_service,
                      service_models.ClosedUserGroupNetworkService):
        raise ValidationError(
            "Service type must match config type. {} given, expected {}" \
                .format(network_service.__class__,
                        "ClosedUserGroupNetworkService"))

    # Check permissions on related objects
    managing_customer = customers_svc.get_customer(
        scoping_customer=scoping_customer,
        customer=config_input.get("managing_customer"))
    consuming_customer = customers_svc.get_customer(
        scoping_customer=scoping_customer,
        customer=config_input.get("consuming_customer"))

    # Create a config for a closed user group
    config = access_models.ClosedUserGroupNetworkServiceConfig(
        state=State.REQUESTED,
        inner_vlan=config_input["inner_vlan"],
        outer_vlan=config_input["outer_vlan"],
        capacity=config_input["capacity"],
        purchase_order=config_input["purchase_order"],
        external_ref=config_input["external_ref"],
        contract_ref=config_input["contract_ref"],
        network_service=network_service,
        scoping_customer=scoping_customer,
        consuming_customer=consuming_customer,
        managing_customer=managing_customer,
        connection=connection)
    config.save()

    # Assign many to many contacts
    if "contacts" in config_input.keys():
        assign_network_service_contacts(
            scoping_customer=scoping_customer,
            network_service_config=config,
            contacts=config_input["contacts"])

    return config


def create_cloud_network_service_config(
        scoping_customer,
        network_service,
        connection,
        config_input,
    ):
    """
    Create a new config for the cloud network service.

    :param network_service: The network service to configure
    :param connection: The connection to configure for the network service
    :param scoping_customer: The customer managing the service configuration
    :param config_input: The configuration data
    """
    # Check if the service type is correct
    if not isinstance(network_service, service_models.CloudNetworkService):
        raise ValidationError(
            "Service type must match config type. {} given, expected {}" \
                .format(network_service.__class__,
                        "CloudNetworkService"))

    # Check permissions on related objects
    managing_customer = customers_svc.get_customer(
        scoping_customer=scoping_customer,
        customer=config_input["managing_customer"])
    consuming_customer = customers_svc.get_customer(
        scoping_customer=scoping_customer,
        customer=config_input["consuming_customer"])

    # Create the cloud service config
    config = access_models.CloudNetworkServiceConfig(
        state=State.REQUESTED,
        cloud_key=config_input["cloud_key"],
        inner_vlan=config_input["inner_vlan"],
        outer_vlan=config_input["outer_vlan"],
        capacity=config_input["capacity"],
        purchase_order=config_input["purchase_order"],
        external_ref=config_input["external_ref"],
        contract_ref=config_input["contract_ref"],
        network_service=network_service,
        scoping_customer=scoping_customer,
        consuming_customer=consuming_customer,
        managing_customer=managing_customer,
        connection=connection)
    config.save()

    # Assign many to many contacts
    if "contacts" in config_input.keys():
        assign_network_service_contacts(
            scoping_customer=scoping_customer,
            network_service_config=config,
            contacts=config_input["contacts"])

    return config


@active.command
def update_network_service_config(
        dispatch,
        scoping_customer=None,
        network_service_config=None,
        network_service_config_update=None,
    ):
    """
    We allow for updates on the network service config
    objects. However, how much is updated depends on the
    type of network service configuration.

    :param dispatch: A bound dispatch
    :param scoping_customer: A customer managing the network service config.
    :param network_service_config: The network service configuration
        to update.
    :param network_service_config_update: A dict with config updates.
    """
    # Use update method dependent on config type.
    # Ownership is tested in the individual update methods
    if isinstance(
            network_service_config,
            access_models.ExchangeLanNetworkServiceConfig):
        config = _update_exchange_lan_network_service_config(
            scoping_customer,
            network_service_config,
            network_service_config_update)
    elif isinstance(
            network_service_config,
            access_models.ClosedUserGroupNetworkServiceConfig):
        config = _update_closed_user_group_network_service_config(
            scoping_customer,
            network_service_config,
            network_service_config_update)
    elif isinstance(
            network_service_config,
            access_models.ELineNetworkServiceConfig):
        config = _update_eline_network_service_config(
            scoping_customer,
            network_service_config,
            network_service_config_update)
    elif isinstance(
            network_service_config,
            access_models.CloudNetworkServiceConfig):
        config = _update_cloud_network_service_config(
            scoping_customer,
            network_service_config,
            network_service_config_update)
    else:
        raise NotImplementedError(
            ("Update method missing for "
             "network service config type: {}").format(
                 config.__class__.__name__))

    # Inform observers about the update
    dispatch(network_service_config_updated(config))

    # Update configuration from database in case the state was
    # changed by the observers
    config.refresh_from_db()

    return config


def _update_eline_network_service_config(
        scoping_customer,
        config,
        config_update,
    ):
    """
    Update an existing ELine network service configuration.

    CAVEAT: This for now is just a stub / basic implementation
            until we get to this usecase.

    :param scoping_customer: A customer managing the config.
    :param config: An eline network service config
    :param config_update: Update data
    """
    # Load eline network service config and perform ownership test
    config = get_network_service_config(
        scoping_customer,
        network_service_config=config)

    # Clean update data
    update = whitelist_filter_fields(config_update, [
        "inner_vlan",
        "outer_vlan",
        "capacity",
        "purchase_order",
        "external_ref",
        "managing_customer",
        "consuming_customer",
    ])

    # Check permissions on related objects
    customers_svc.get_customer(
        scoping_customer=scoping_customer,
        customer=config_update.get("managing_customer"))
    customers_svc.get_customer(
        scoping_customer=scoping_customer,
        customer=config_update.get("consuming_customer"))

    with transaction.atomic():
        # Update connection if present
        if "connection" in config_update.keys():
            connection = connections_svc.get_connection(
                scoping_customer=scoping_customer,
                connection=config_update["connection"])
            config.connection = connection

        # Update the configuration with other fields
        for attr, value in update.items():
            setattr(config, attr, value)
        config.save()

        # Assign implementation contacts
        if "contacts" in config_update.keys():
            assign_network_service_contacts(
                contacts=config_update["contacts"],
                scoping_customer=scoping_customer,
                network_service_config=config)

    return config


def _update_exchange_lan_network_service_config(
        scoping_customer,
        config,
        config_update,
    ):
    """
    Update an existing Exchange Lan Network Service configuration.

    :param scoping_customer: The customer managing the config
    :param config: An exchange lan network service config
    :param config_update: Update data
    """
    # Load exchange lan network service config with ownership test
    config = get_network_service_config(
        scoping_customer=scoping_customer,
        network_service_config=config)

    # Clean update data
    update = whitelist_filter_fields(config_update, [
        "inner_vlan",
        "capacity",
        "purchase_order",
        "external_ref",
        "asns",
        "managing_customer",
        "consuming_customer",
    ])

    # Check permissions on related objects
    customers_svc.get_customer(
        scoping_customer=scoping_customer,
        customer=config_update.get("managing_customer"))
    customers_svc.get_customer(
        scoping_customer=scoping_customer,
        customer=config_update.get("consuming_customer"))

    with transaction.atomic():
        # Update connection if present
        if "connection" in config_update.keys():
            connection = connections_svc.get_connection(
                scoping_customer=scoping_customer,
                connection=config_update["connection"])
            config.connection = connection

        # Update the configuration with other fields
        for attr, value in update.items():
            setattr(config, attr, value)
        config.save()

        # Assign implementation contacts
        if "contacts" in config_update.keys():
            assign_network_service_contacts(
                scoping_customer=scoping_customer,
                network_service_config=config,
                contacts=config_update["contacts"])

        # Assign mac addresses if present in update
        if "mac_addresses" in config_update.keys():
            assign_mac_addresses(
                scoping_customer=scoping_customer,
                network_service_config=config,
                mac_addresses=config_update["mac_addresses"])

        if "outer_vlan"in config_update.keys():
            # Get outer vlan from range set
            outer_vlans = config_update["outer_vlan"]
            if not outer_vlans:
                raise ValidationError(
                    "Please provide a non empty set of vlan ranges.",
                    field="outer_vlan")

            # Select random vlan from list
            outer_vlan_values = list(itertools.chain.from_iterable(outer_vlans))
            outer_vlan = outer_vlan_values[
                random.randint(0, len(outer_vlan_values) - 1)]
            config.outer_vlan = outer_vlan
            config.save()


    return config


def _update_closed_user_group_network_service_config(
        scoping_customer,
        config,
        config_update,
    ):
    """
    Update a closed user group service configuration.
    This is pretty much the same as for the exchange lan network.
    However, this might change in the future.

    :param managing_customer: The customer paying for the service
    :param consuming_customer: The customer responsible for the config
    :param config: A closed user group network service config
    :param config_update: Update data (might be partial)
    """
    # Load exchange lan network service config with ownership test
    config = get_network_service_config(
        scoping_customer=scoping_customer,
        network_service_config=config)

    # Clean update data
    update = whitelist_filter_fields(config_update, [
        "inner_vlan",
        "outer_vlan",
        "capacity",
        "purchase_order",
        "external_ref",
        "managing_customer",
        "consuming_customer",
    ])

    # Check permissions on related objects
    customers_svc.get_customer(
        scoping_customer=scoping_customer,
        customer=config_update.get("managing_customer"))
    customers_svc.get_customer(
        scoping_customer=scoping_customer,
        customer=config_update.get("consuming_customer"))

    with transaction.atomic():
        # Update connection if present
        if "connection" in config_update.keys():
            connection = connections_svc.get_connection(
                scoping_customer=scoping_customer,
                connection=config_update["connection"])
            config.connection = connection

        # Update the configuration with other fields
        for attr, value in update.items():
            setattr(config, attr, value)
        config.save()

        # Assign implementation contacts
        if "contacts" in config_update.keys():
            assign_network_service_contacts(
                scoping_customer=scoping_customer,
                network_service_config=config,
                contacts=config_update["contacts"])

        # Assign mac addresses if present in update
        if "mac_addresses" in config_update.keys():
            assign_mac_addresses(
                scoping_customer=scoping_customer,
                network_service_config=config,
                mac_addresses=config_update["mac_addresses"])

    return config


def _update_cloud_network_service_config(
        scoping_customer,
        config,
        config_update,
    ):
    """
    Update a cloud network service configuration.

    :param scoping_customer: The customer managing this service configuration
    :param config: A cloud network service config
    :param config_update: Update data
    """
    # Load exchange lan network service config with ownership test
    config = get_network_service_config(
        scoping_customer=scoping_customer,
        network_service_config=config)

    # Clean update data
    update = whitelist_filter_fields(config_update, [
        "inner_vlan",
        "outer_vlan",
        "capacity",
        "purchase_order",
        "external_ref",
        "cloud_key",
        "managing_customer",
        "consuming_customer",
    ])

    # Check permissions on related objects
    customers_svc.get_customer(
        scoping_customer=scoping_customer,
        customer=config_update.get("managing_customer"))
    customers_svc.get_customer(
        scoping_customer=scoping_customer,
        customer=config_update.get("consuming_customer"))

    with transaction.atomic():
        # Update connection if present
        if "connection" in config_update.keys():
            connection = connections_svc.get_connection(
                scoping_customer=scoping_customer,
                connection=config_update["connection"])
            config.connection = connection

        # Update the configuration with other fields
        for attr, value in update.items():
            setattr(config, attr, value)
        config.save()

        # Assign implementation contacts
        if "contacts" in config_update.keys():
            assign_network_service_contacts(
                scoping_customer=scoping_customer,
                network_service_config=config,
                contacts=config_update["contacts"])

    return config


@active.command
def destroy_network_service_config(
        dispatch,
        scoping_customer=None,
        network_service_config=None
    ):
    """
    Destroy a service config.

    :param config: The config to destroy. Might be an ID.
    :param managing: The owning customer of the config.
    """
    config = get_network_service_config(
        scoping_customer=scoping_customer,
        network_service_config=network_service_config)

    # So for now... just dispatch the events and remove the config.
    dispatch(network_service_config_state_changed(
        config,
        prev_state=config.state,
        next_state=State.DECOMMISSIONED))
    dispatch(network_service_config_destroyed(config))

    # Destroy destroy destroy
    config.delete()

    return config

#
# Feature Configurations
#

def get_network_feature_configs(
        scoping_customer=None,
        filters=None,
    ):
    """
    Get all feature configurations within a customer context.

    :param managing_customer: Scope configs to those of this customer
    :param consuming_customer: Scope configs to those of this customer
    :param filters: A dict of filter params
    """
    configs = access_filters.NetworkFeatureConfigFilter(filters).qs

    # Apply ownership filtering
    if scoping_customer:
        configs = configs.filter(scoping_customer=scoping_customer)

    return configs


def get_network_feature_config(
        scoping_customer=None,
        network_feature_config=None,
    ):
    """
    Get a single feature configuration.
    If billing or owning customer are present, perform an ownership test.

    :param config: The config to load. Might be an ID.
    :param managing_customer: The billing customer
    :param consuming_customer: The owning customer of the config.
    """
    if isinstance(network_feature_config, access_models.NetworkFeatureConfig):
        # Just perform an ownership test
        if scoping_customer and not \
            network_feature_config.scoping_customer == scoping_customer:
            raise ResourceAccessDenied(network_feature_config)

        return network_feature_config

    configs = get_network_feature_configs(scoping_customer=scoping_customer)

    return configs.get(pk=network_feature_config)


@active.command
def create_network_feature_config(
        dispatch,
        scoping_customer=None,
        network_feature_config_input=None,
    ):
    """
    Create a new network feature config.

    :param scoping_customer: The customer managing the config.
    :param config_input: The configuration data
    """
    # Load network feature and service config
    network_feature = network_svc.get_network_feature(
        scoping_customer=scoping_customer,
        network_feature=network_feature_config_input["network_feature"])
    service_config = get_network_service_config(
        scoping_customer=scoping_customer,
        network_service_config=network_feature_config_input["network_service_config"])

    # Check permissions on related objects
    managing_customer = customers_svc.get_customer(
        scoping_customer=scoping_customer,
        customer=network_feature_config_input.get("managing_customer"))
    consuming_customer = customers_svc.get_customer(
        scoping_customer=scoping_customer,
        customer=network_feature_config_input.get("consuming_customer"))

    # Check that the feature is available for
    # the service in question.
    network_svc.assert_network_feature_available(
        network_feature, service_config.network_service)

    # Dispatch configuration creation
    if network_feature_config_input["type"] == \
        access_models.NETWORK_FEATURE_CONFIG_TYPE_BLACKHOLING:
        config = _create_blackholing_network_feature_config(
            network_feature, service_config,
            scoping_customer, managing_customer, consuming_customer,
            network_feature_config_input)
    elif network_feature_config_input["type"] == \
        access_models.NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER:
        config = _create_route_server_network_feature_config(
            network_feature, service_config,
            scoping_customer, managing_customer, consuming_customer,
            network_feature_config_input)
    elif network_feature_config_input["type"] == \
        access_models.NETWORK_FEATURE_CONFIG_TYPE_IXPROUTER:
        config = _create_ixprouter_network_feature_config(
            network_feature, service_config,
            scoping_customer, managing_customer, consuming_customer,
            network_feature_config_input)
    else:
        raise ValidationError("Unknown network feature: {}".format(
            config["type"]))

    dispatch(network_feature_config_created(config))

    # Reload model after state machine transition
    config.refresh_from_db()

    return config


def _create_blackholing_network_feature_config(
        network_feature,
        network_service_config,
        scoping_customer,
        managing_customer,
        consuming_customer,
        config_input,
    ):
    """
    Create a blackholing network feature configuration.

    :param network_feature: The network feature to configure
    :param network_service_config: The service the feature will be attached to
    :param scoping_customer: The customer managing the config
    :param managing_customer: The billing customer
    :param consuming_customer: The owning customer of the config.
    :param config_input: Validated configuration data
    """
    if not isinstance(network_feature,
                      service_models.BlackholingNetworkFeature):
        raise ValidationError(
            ("The selected feature ({}) does not match the "
             "feature config for: blackholing").format(
                 network_feature.__class__.__name__))

    # Create the configuration for the blackholing feature
    feature = access_models.BlackholingNetworkFeatureConfig(
        state=State.REQUESTED,
        network_feature=network_feature,
        network_service_config=network_service_config,
        scoping_customer=scoping_customer,
        managing_customer=managing_customer,
        consuming_customer=consuming_customer,
        purchase_order=config_input["purchase_order"],
        external_ref=config_input["external_ref"],
        contract_ref=config_input["contract_ref"],
        activated=config_input["activated"],
        ixp_specific_configuration=config_input["ixp_specific_configuration"])
    feature.save()

    return feature


def _create_route_server_network_feature_config(
        network_feature,
        network_service_config,
        scoping_customer,
        managing_customer,
        consuming_customer,
        config_input,
    ):
    """
    Create a route server feature configuration.

    :param network_feature: The network feature to configure
    :param network_service_config: A network service configuration
    :param scoping_customer: The customer managing the config.
    :param managing_customer: The billing customer.
    :param consuming_customer: The owning customer of the config.
    :param config_input: Validated configuration data.
    """
    if not isinstance(network_feature,
                      service_models.RouteServerNetworkFeature):
        raise ValidationError(
            ("The selected feature ({}) does not match the "
             "feature config for: route_server").format(
                 network_feature.__class__.__name__))

    # Create password if not exists
    password = config_input["password"]
    if not password:
        password = secrets.token_hex(23)

    # Check if the session mode matches
    session_mode = config_input["session_mode"]
    if network_feature.session_mode != session_mode:
        raise SessionModeInvalid(
            ("The session_mode of the config must match the "
             "mode of the feature to configue."))

    # AS sets:
    as_set_v4 = config_input.get("as_set_v4")
    as_set_v6 = config_input.get("as_set_v6")
    if session_mode == RouteServerSessionMode.MODE_PUBLIC:
        # check network feature address families
        address_families = network_feature.address_families
        af_inet = ipam_models.AddressFamilies.AF_INET
        af_inet6 = ipam_models.AddressFamilies.AF_INET6

        if af_inet in address_families and not as_set_v4:
            raise SessionModeInvalid(
                ("The session mode requires the `as_set_v4`."),
                field="as_set_v4")
        if af_inet6 in address_families and not as_set_v6:
            raise SessionModeInvalid(
                ("The session mode requires the `as_set_v6`."),
                field="as_set_v6")

    # Check if session type is valid
    bgp_session_type = config_input["bgp_session_type"]
    if not bgp_session_type in network_feature.available_bgp_session_types:
        raise ValidationError(
            ("The bgp_session_type `{}` is not available on the "
             "route server.").format(bgp_session_type.value),
            field="bgp_session_type")

    # Create the Route Server access configuration:
    config = access_models.RouteServerNetworkFeatureConfig(
        state=State.REQUESTED,
        network_feature=network_feature,
        network_service_config=network_service_config,
        scoping_customer=scoping_customer,
        managing_customer=managing_customer,
        consuming_customer=consuming_customer,
        asn=config_input["asn"],
        purchase_order=config_input["purchase_order"],
        external_ref=config_input["external_ref"],
        contract_ref=config_input["contract_ref"],
        password=password,
        as_set_v4=as_set_v4,
        as_set_v6=as_set_v6,
        session_mode=session_mode,
        bgp_session_type=config_input["bgp_session_type"],
        max_prefix_v4=config_input.get("max_prefix_v4"),
        max_prefix_v6=config_input.get("max_prefix_v6"),
        insert_ixp_asn=config_input["insert_ixp_asn"])
    config.save()

    # Get contacts and check if the required contact types
    # of the network feature are present
    contacts = config_input["contacts"]
    assign_network_feature_contacts(
        network_feature_config=config,
        contacts=contacts,
        scoping_customer=scoping_customer)

    return config


def _create_ixprouter_network_feature_config(
        network_feature,
        network_service_config,
        scoping_customer,
        managing_customer,
        consuming_customer,
        config_input,
    ):
    """
    Create an ixprouter network feature configuration.
    The IXP router requires a password. The password can be either
    supplied in the config_input. In case no password is present,
    a password is generated.

    :param network_feature: The network feature to configure
    :param network_service_config: The service the feature will configured for
    :param managing_customer: The billing customer
    :param consuming_customer: The owning customer of the config.
    :param config_input: The configuration data
    """
    if not isinstance(network_feature,
                      service_models.IXPRouterNetworkFeature):
        raise ValidationError(
            ("The selected feature ({}) does not match the "
             "feature config for: ixprouter").format(
                 network_feature.__class__.__name__))

    # Create password if not exists
    password = config_input["password"]
    if not password:
        password = secrets.token_hex(23)

    # Create the IXP Router configuration:
    feature = access_models.IXPRouterNetworkFeatureConfig(
        state=State.REQUESTED,
        network_feature=network_feature,
        network_service_config=network_service_config,
        scoping_customer=scoping_customer,
        managing_customer=managing_customer,
        consuming_customer=consuming_customer,
        purchase_order=config_input["purchase_order"],
        external_ref=config_input["external_ref"],
        contract_ref=config_input["contract_ref"],
        password=password,
        max_prefix=config_input.get("max_prefix"),
        bgp_session_type=config_input["bgp_session_type"])
    feature.save()

    return feature


@active.command
def update_network_feature_config(
        dispatch,
        scoping_customer=None,
        network_feature_config=None,
        network_feature_config_update=None,
    ):
    """
    Update a network feature configuration.
    The correct update method is determined by the type of
    config passed. Pattern matching would be fun.

    :param dispatch: A bound event dispatcher
    :param scoping_customer: The customer managing the feature config
    :param consuming_customer: The customer owning the configuration
    :param config: The network feature config
    """
    if not network_feature_config_update:
        raise ValidationError("Missing config update")

    # Load the network feature configuration (and test ownership)
    config = get_network_feature_config(
        scoping_customer=scoping_customer,
        network_feature_config=network_feature_config)

    # Check permissions on related objects
    managing_customer = customers_svc.get_customer(
        scoping_customer=scoping_customer,
        customer=network_feature_config_update.get("managing_customer"))
    consuming_customer = customers_svc.get_customer(
        scoping_customer=scoping_customer,
        customer=network_feature_config_update.get("consuming_customer"))

    # Choose update method based on feature config type
    if isinstance(config, access_models.BlackholingNetworkFeatureConfig):
        config = _update_blackholing_network_feature_config(
            network_feature_config, network_feature_config_update)
    elif isinstance(config, access_models.RouteServerNetworkFeatureConfig):
        config = _update_route_server_network_feature_config(
            config, network_feature_config_update)
    elif isinstance(config, access_models.IXPRouterNetworkFeatureConfig):
        config = _update_ixp_router_network_feature_config(
            config, network_feature_config_update)
    else:
        raise NotImplementedError(
            ("The provided network feature config: '{}' "
             "has no update capabilities.").format(
                 config.__class__.__name__))

    # Done? Inform our observers
    dispatch(network_feature_config_updated(config))

    return config


def _update_blackholing_network_feature_config(
        config,
        config_update,
    ):
    """
    Update an existing blackholing network feature configuration.

    :param config: The network feature config
    :param config_update: The update to apply. Might be partial.
    """
    # Clean update data
    update = whitelist_filter_fields(config_update, [
        "purchase_order",
        "external_ref",
        "ixp_specific_configuration",
        "activated",
        "scoping_customer",
        "consuming_customere",
    ])

    with transaction.atomic():
        for attr, value in update.items():
            setattr(config, attr, value)
        config.save()

    # TODO: This is a stub. Add IP address assignment.

    return config


@transaction.atomic
def _update_route_server_network_feature_config(
        config,
        config_update,
    ):
    """
    Update a route server network feature configuration.

    :param config: The route server config
    :param config_update: The update to apply. Might be partial.
    """
    # Clean update data
    update = whitelist_filter_fields(config_update, [
        "purchase_order",
        "external_ref",
        "password",
        "max_prefix_v4",
        "max_prefix_v6",
        "insert_ixp_asn",
        "bgp_session_type",
        "managing_customer",
        "consuming_customer",
    ])

    # Get network service feature
    feature = config.network_feature

    # Set whitelisted attributes
    for attr, value in update.items():
        setattr(config, attr, value)

    # Set session mode
    if "session_mode" in config_update.keys():
        session_mode = config_update["session_mode"]

        # Check if the session mode matches
        if feature.session_mode != session_mode:
            raise SessionModeInvalid(
                ("The session_mode of the config must match the "
                 "mode of the feature to configue."))

    # Handle dependent as set
    as_set_v4 = config_update.get("as_set_v4", config.as_set_v4)
    as_set_v6 = config_update.get("as_set_v6", config.as_set_v6)

    if config.session_mode == RouteServerSessionMode.MODE_PUBLIC:
        families = feature.address_families
        af_inet = ipam_models.AddressFamilies.AF_INET
        af_inet6 = ipam_models.AddressFamilies.AF_INET6

        if af_inet in families and not as_set_v4:
            raise SessionModeInvalid(
                ("The session mode requires the `as_set_v4`"),
                field="as_set_v4")

        if af_inet6 in families and not as_set_v6:
            raise SessionModeInvalid(
                ("The session mode requires the `as_set_v6`"),
                field="as_set_v6")

    config.as_set_v4 = as_set_v4
    config.as_set_v6 = as_set_v6

    # Update contacts
    if "contacts" in config_update.keys():
        contacts = config_update["contacts"]
        assign_network_feature_contacts(
            network_feature_config=config,
            contacts=contacts,
            scoping_customer=scoping_customer)

    # Persist changes
    config.save()

    return config


def _update_ixp_router_network_feature_config(
        config,
        config_update,
    ):
    """
    Update an existing ixp router feature configuration.

    :param config: The network feature config
    :param config_update: The update to apply. Might be partial.
    """
    # Clean update data
    update = whitelist_filter_fields(config_update, [
        "purchase_order",
        "external_ref",
        "password",
        "max_prefix",
        "bgp_session_type",
        "managing_customer",
        "consuming_customer",
    ])

    with transaction.atomic():
        for attr, value in update.items():
            setattr(config, attr, value)
        config.save()

    return config


@active.command
def destroy_network_feature_config(
        dispatch,
        scoping_customer=None,
        network_feature_config=None
    ):
    """
    Destroy a feature config.

    :param network_feature_config: The config to destroy. Might be an ID.
    :param scoping_customer: The customer managing the config. 
    """
    config = get_network_feature_config(
        scoping_customer=scoping_customer,
        network_feature_config=network_feature_config)

    config.delete()
    dispatch(network_feature_config_destroyed(config))

    return config

