
"""
Connections Service
-------------------

Create new connections with multiple port demarcation points
or disconnect port demarcs.

In general: Configure connections.
"""

from typing import Iterable, Optional, Any

from django.utils import text as text_utils
from django.db import transaction

from jea.exceptions import (
    ResourceAccessDenied,
    ValidationError,
)
from jea.eventmachine import active
from jea.eventmachine.models import State
from jea.crm.services import (
    customers as customers_svc,
    contacts as contacts_svc,
)
from jea.access import models as access_models
from jea.access.models import (
    Connection,
    DemarcationPoint,
)
from jea.access.services import demarcs as demarcs_svc
from jea.access.events import (
    demarc_connected,
    demarc_disconnected,
    demarc_state_changed,
    connection_created,
    connection_updated,
    connection_destroyed,
    connection_state_changed,
)
from jea.access.filters import (
    ConnectionFilter,
)
from jea.access.exceptions import (
    ConnectionModeUnavailable,
    DemarcationPointUnavailable,
    DemarcationPointInUse
)
from jea.crm import models as crm_models


def make_connection_name(customer=None) -> str:
    """Make connection name"""
    # When a customer is present, limit to customer connections and
    # include customer in the naming schema: conn-customer-name-32
    if customer:
        counter = customer.consumed_connections.count() + 1
        tag = text_utils.slugify(customer.name)[:42]
        return f"conn-{tag}-{counter}"

    # Fallback
    counter = Connection.objects.count() + 1
    return f"conn-{counter}"


def get_connections(
        scoping_customer=None,
        filters=None
    ) -> Iterable[Connection]:
    """
    Get connections scoped to managing customer.
    Either or both might be None, in which case the filter is
    not applied and all connection objects are considered.

    :param scoping_customer: Manages connections
    :param filters: A mapping of string filter params
    """
    if filters:
        connections = ConnectionFilter(filters).qs
    else:
        connections = access_models.Connection.objects.all()

    # Apply ownership filteres
    if scoping_customer:
        connections = connections.filter(scoping_customer=scoping_customer)

    return connections


def get_connection(
        connection=None,
        scoping_customer=None,
    ) -> Connection:
    """
    Get a connection and perform ownership check on it,
    in case billing- or owning customer are provided.

    :param connection: The connection to load
    :param scoping_customer: The customer managing the connection

    :raises ResourceAccessDenied: When the managing customer
        lacks access rights.
    """
    if not connection:
        raise ValidationError("A connection needs to be provided")

    if not isinstance(connection, access_models.Connection):
        connection = Connection.objects.get(pk=connection)

    # We don't need to fetch the connection from the
    # database, however we need to perform a ownership test
    if scoping_customer and not \
        connection.scoping_customer == scoping_customer:
        raise ResourceAccessDenied(connection)

    # Everythings fine, just pass back the connection
    return connection


@active.command
def connect_demarcation_point(
        dispatch,
        connection=None,
        demarcation_point=None,
        scoping_customer=None,
    ) -> Connection:
    """
    Add a port demarcation point to a connection.

    :param connection: The connection the port demarc should be attached to.
    :param demarcation_point: The allocated port demarcation point to
        include in the connection.
    :returns: The connection the port was attached to.
    """
    connection = get_connection(
        connection=connection,
        scoping_customer=scoping_customer)

    # We require port demarcs provided from our user and we
    # need to check the ownership of these. So, we only allow
    # port demarcs assigned to the billing customer.
    # This will fail and raise an error if a port demarc is unavilable
    # to a given customer.
    demarcation_point = demarcs_svc.get_demarcation_point(
        demarcation_point=demarcation_point,
        scoping_customer=scoping_customer)

    # Check if there is anything to do
    if demarcation_point.connection == connection:
        return connection # Nothing to do here!

    # Check if we can join the connection
    demarcs_svc.demarcation_point_can_join_connection(
        scoping_customer=scoping_customer,
        connection=connection,
        demarcation_point=demarcation_point)

    # Looking good. Let's assign this port to this connection
    with transaction.atomic():
        demarcation_point.connection = connection
        demarcation_point.save()

        # Emit events
        dispatch(demarc_connected(
            demarcation_point,
            connection))

    # Refersh model after state transition
    connection.refresh_from_db()

    return connection


@active.command
def disconnect_demarcation_point(
        dispatch,
        scoping_customer=None,
        demarcation_point=None,
    ) -> DemarcationPoint:
    """
    Remove a port demarcation point from a connection.

    :param scoping_customer: The customer used for scope checking
    :param demarcation_point: The port demarcation point to disconnect
    """
    # Get demarc
    demarcation_point = demarcs_svc.get_demarcation_point(
        demarcation_point=demarcation_point,
        scoping_customer=scoping_customer)

    # Check if demarc is connected
    connection = demarcation_point.connection
    if not connection:
        raise DemarcationPointNotConnected

    # Disconnect
    demarcation_point.connection = None
    demarcation_point.save()

    # Emit event
    dispatch(demarc_disconnected(
        demarcation_point,
        connection))

    # Refresh model after state transition
    demarcation_point.refresh_from_db()

    return demarcation_point


@transaction.atomic
@active.command
def create_connection(
        dispatch,
        scoping_customer=None,
        connection_input=None,
    ) -> Connection:
    """
    Create a new connection based on user data supplied
    via `connection_input` and assigned to the billing and
    owning customer.

    Reseller connections might be used from a subcustomer.

    :param scoping_customer: The customer managing the connection

    :returns: A new connection.
    """
    if not scoping_customer:
        raise ValidationError("A `scoping_customer` is required.")

    # Check permissions on related objects
    managing_customer = customers_svc.get_customer(
        scoping_customer=scoping_customer,
        customer=connection_input.get("managing_customer"))
    consuming_customer = customers_svc.get_customer(
        scoping_customer=scoping_customer,
        customer=connection_input.get("consuming_customer"))

    if not managing_customer:
        raise ValidationError("A `managing_customer` is required")
    if not consuming_customer:
        raise ValidationError("A `consuming_customer` is required")

    # Load contacts
    contacts = connection_input["contacts"]
    contacts_svc.assert_presence_of_contact_types(
        required_contact_types=["billing", "implementation"],
        contacts=contacts,
        scoping_customer=scoping_customer)

    # Check ownership on implementation contacts
    contacts = [
        contacts_svc.get_contact(
            scoping_customer=scoping_customer,
            contact=c)
        for c in contacts]

    # Prepare mode and timeout
    connection_mode = connection_input["mode"]
    lacp_timeout = None
    if connection_mode == access_models.ConnectionMode.MODE_LACP:
        lacp_timeout = connection_input.get(
            "lacp_timeout",
            access_models.LACPTimeout.TIMEOUT_SLOW)

    # Connections are named by us
    connection_name = make_connection_name(customer=consuming_customer)

    # Get the ratelimiting on the connection, if there is any.
    connection_speed = connection_input.get("connection_speed", None)

    # We not have everything required to create a new connection,
    # but in case anything goes wrong with assigning the port demarc
    # to a connection, we want to be able to rollback.
    connection = access_models.Connection(
        name=connection_name,
        state=State.REQUESTED,
        mode=connection_mode,
        lacp_timeout=lacp_timeout,
        speed=connection_speed,

        # Ownable
        scoping_customer=scoping_customer,
        managing_customer=managing_customer,
        consuming_customer=consuming_customer,
        external_ref=connection_input.get("external_ref"),
        contract_ref=connection_input.get("contract_ref"),
        purchase_order=connection_input.get("purchase_order", ""),
    )
    connection.save()

    # Assign contacts
    connection.contacts.set(contacts)

    # Emit events and refresh model after state transition
    dispatch(connection_created(connection))
    connection.refresh_from_db()

    return connection


def assert_connection_can_assume_mode(
        connection: Connection,
        mode: access_models.ConnectionMode,
    ):
    """
    Check that a connection is able to assume a mode:
    E.g. a lag can only be change to standalone, if there
    is only one demarc connected.

    :raises ConnectionModeUnavailable:
    """
    # Check modes:
    if mode == access_models.ConnectionMode.MODE_STANDALONE:
        if connection.demarcation_points.count() > 1:
            raise ConnectionModeUnavailable(
                "There is more than one demarc attached to the connection")

    # All other should be ok


@transaction.atomic
@active.command
def update_connection(
        dispatch,
        scoping_customer=None,
        connection=None,
        connection_update=None,
    ) -> Connection:
    """Update a given connection"""
    if not connection_update:
        raise ValidationError("update data for connection is missing")

    # Load connection and check permissions
    connection = get_connection(
        connection=connection,
        scoping_customer=scoping_customer)

    # Allright. Let's see what we can update
    # Ownership: Billing and owning customer 
    managing_customer = connection_update.get("managing_customer")
    if managing_customer:
        managing_customer = customers_svc.get_customer(
            customer=managing_customer,
            scoping_customer=scoping_customer)
        connection.managing_customer = managing_customer

    consuming_customer = connection_update.get("consuming_customer")
    if consuming_customer:
        consuming_customer = customers_svc.get_customer(
            customer=consuming_customer,
            scoping_customer=scoping_customer)
        connection.consuming_customer = consuming_customer

    # Ratelimiting speed
    if "speed" in connection_update.keys():
        speed = connection_update.get("speed")
        connection.speed = speed

    # Connection Mode and LACP timeout
    mode = connection_update.get("mode")
    lacp_timeout = connection_update.get("lacp_timeout",
        connection.lacp_timeout)

    if "mode" in connection_update.keys():
        assert_connection_can_assume_mode(connection, mode)
        if mode == access_models.ConnectionMode.MODE_LACP:
            if not lacp_timeout:
                raise ValidationError(
                    "A valid lacp timeout (slow, fast) is required",
                    field="lacp_timeout")
        else:
            lacp_timeout = None

        connection.mode = mode
        connection.lacp_timeout = lacp_timeout

    if "lacp_timeout" in connection_update.keys():
        connection.lacp_timeout = lacp_timeout

    # Implementation contacts:
    # Check ownership on implementation contacts
    if "contacts" in connection_update.keys():
        contacts = connection_update["contacts"]
        contacts = [
            contacts_svc.get_contact(
                scoping_customer=scoping_customer,
                contact=c)
            for c in contacts]
 
        # Check for presence of required customers
        contacts_svc.assert_presence_of_contact_types(
            required_contact_types=["implementation", "billing"],
            contacts=contacts,
            scoping_customer=scoping_customer)

        connection.contacts.set(contacts)

    # Purchase Order / External Ref / Contract Ref
    # TODO: Assigning these should be refactored and generalized.
    if "purchase_order" in connection_update.keys():
        connection.purchase_order = connection_update["purchase_order"]
    if "external_ref" in connection_update.keys():
        connection.external_ref = connection_update["external_ref"]
    if "contract_ref" in connection_update.keys():
        connection.contract_ref = connection_update["contract_ref"]

    # Persist changes and trigger state changes
    connection.save()
    dispatch(connection_updated(connection))
    connection.refresh_from_db()

    return connection


@transaction.atomic
@active.command
def archive_connection(
        dispatch,
        scoping_customer=None,
        connection=None,
    ):
    """
    Archive a connection.

    Release all connected demarcation points and then archive
    this connection.

    When customers are provided, they will be used for scope checking.

    :param scoping_customer: The customer managing the connection
    :param connection: The connection to dissolve.
    """
    # Perform scope checking. In case the customers do not match,
    # the object does not exist in the requested scope.
    connection = get_connection(
        scoping_customer=scoping_customer,
        connection=connection)

    # Disconnect all port demarcs
    for demarc in connection.demarcation_points.all():
        disconnect_demarcation_point(
            scoping_customer=scoping_customer,
            demarcation_point=demarc)

    # Archive connection
    prev_state = connection.state
    connection.state = State.ARCHIVED
    connection.save()

    dispatch(connection_state_changed(connection, prev_state))
    dispatch(connection_destroyed(connection))

    connection.refresh_from_db()

    return connection

