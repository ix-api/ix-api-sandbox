
"""
JEA Access Models Tests
"""

import pytest
from django.db.models import ProtectedError, ObjectDoesNotExist
from model_mommy import mommy

from jea.access.models import (
    Connection,
    DemarcationPoint,
    NetworkServiceConfig,
    ExchangeLanNetworkServiceConfig,
    ClosedUserGroupNetworkServiceConfig,
    ELineNetworkServiceConfig,
    CloudNetworkServiceConfig,
    NetworkFeatureConfig,
    RouteServerNetworkFeatureConfig,
    IXPRouterNetworkFeatureConfig,
    BlackholingNetworkFeatureConfig,
)


@pytest.mark.django_db
def test_connection():
    """Test connection model"""
    customer = mommy.make("crm.Customer")
    implementation_contact = mommy.make("crm.ImplementationContact")
    billing_contact = mommy.make("crm.BillingContact")

    connection = mommy.make(Connection,
                            scoping_customer=customer,
                            managing_customer=customer,
                            consuming_customer=customer,
                            contacts=[implementation_contact, billing_contact])

    # We now have a connection, let's check our relations
    # - Contacts
    assert connection in implementation_contact.connections.all()

    # - Customers
    managing = connection.scoping_customer
    assert managing

    billing = connection.managing_customer
    assert billing
    assert connection in billing.managed_connections.all()

    owning = connection.consuming_customer
    assert owning
    assert connection in owning.consumed_connections.all()


def test_connection_representations():
    """Test __str__ and __repr__"""
    connection = mommy.prepare(Connection)
    assert isinstance(str(connection), str), "Should return a string"
    assert isinstance(repr(connection), str), "Should return a string"


@pytest.mark.django_db
def test_demarcation_point():
    """Test the demarcation point model"""
    pop = mommy.make("catalog.PointOfPresence")
    customer = mommy.make("crm.Customer")
    implementation_contact = mommy.make("crm.ImplementationContact")
    billing_contact = mommy.make("crm.BillingContact")
    connection = mommy.make("access.Connection")
    demarc = mommy.make(
        DemarcationPoint,
        connection=connection,
        point_of_presence=pop,
        managing_customer=customer,
        consuming_customer=customer,
        scoping_customer=customer,
        contacts=[implementation_contact, billing_contact])

    # Test relations
    assert demarc in pop.demarcation_points.all()
    assert demarc in customer.managed_demarcationpoints.all()
    assert demarc in customer.consumed_demarcationpoints.all()
    assert demarc in implementation_contact.demarcationpoints.all()

    # A demarcation point needs to be released before the
    # connection is removed.
    with pytest.raises(ProtectedError):
        connection.delete()


def test_demarcation_point_representation():
    """Test to string and repr methods"""
    demarc = mommy.prepare(DemarcationPoint)
    assert str(demarc), "Should return a string"
    assert repr(demarc), "Should return a string"


#
# Test Access Configuration Objects
#

@pytest.mark.django_db
def test_network_service_config():
    """Test polymorphic network service config model"""
    config = mommy.make(NetworkServiceConfig)
    billing = config.managing_customer
    owning = config.consuming_customer
    connection = config.connection

    contact = mommy.make("crm.ImplementationContact")
    config.contacts.add(contact)

    # Check references
    assert config in set(billing.managed_networkserviceconfigs.all())
    assert config in set(owning.consumed_networkserviceconfigs.all())
    assert config in set(connection.network_service_configs.all())
    assert config in set(contact \
        .networkserviceconfigs.all())


@pytest.mark.django_db
def test_exchange_lan_network_service_config():
    """Test exchange lan network service config model"""
    config = mommy.make(ExchangeLanNetworkServiceConfig)
    ip_address = mommy.make("ipam.IpAddress")
    config.ip_addresses.add(ip_address)

    # Check ip address relation
    assert ip_address in set(config.ip_addresses.all())
    assert config == ip_address.exchange_lan_network_service_config

    # Check mac address list
    mac_address = mommy.make("ipam.MacAddress",
        managing_customer=config.managing_customer,
        consuming_customer=config.consuming_customer)
    config.mac_addresses.add(mac_address)

    assert mac_address in config.mac_addresses.all()


def test_exchange_lan_network_service_config_representation():
    """Test exchange lan network service config representation"""
    config = mommy.prepare(ExchangeLanNetworkServiceConfig)
    assert str(config), "Should return a string"
    assert repr(config), "Should return a string"


@pytest.mark.django_db
def test_closed_user_group_network_service_config():
    """Test closed usergroup config model"""
    config = mommy.make(ClosedUserGroupNetworkServiceConfig)

    # Closed usere groups have a mac address list
    mac_address = mommy.make("ipam.MacAddress",
        managing_customer=config.managing_customer,
        consuming_customer=config.consuming_customer)
    config.mac_addresses.add(mac_address)

    assert mac_address in set(config.mac_addresses.all())


def test_closed_user_group_network_service_config_representation():
    """Test closed user group config representation"""
    config = mommy.prepare(ClosedUserGroupNetworkServiceConfig)
    assert str(config), "Should return a string"
    assert repr(config), "Should return a string"


@pytest.mark.django_db
def test_eline_network_service_config():
    """Test eline network service config model"""
    # Nothing special with this model, but it should be
    # at least creatable.
    config = mommy.make(ELineNetworkServiceConfig)
    assert config.pk, "Config Object should have been saved"


def test_eline_network_serivce_config_representation():
    """Test Eline network service config representation"""
    config = mommy.prepare(ELineNetworkServiceConfig)
    assert str(config), "Should return a (non empty) string"
    assert repr(config), "Should return a (non empty) string"


@pytest.mark.django_db
def test_cloud_network_service_config():
    """Test eline network service config model"""
    # Nothing special with this model, but it should be
    # at least creatable and check the preesence of the cloud_key
    config = mommy.make(CloudNetworkServiceConfig)
    assert config.pk, "Config Object should have been saved"
    assert config.cloud_key


def test_cloud_network_service_config_representation():
    """Test representation of config object"""
    config = mommy.prepare(CloudNetworkServiceConfig)
    assert str(config), "Should return a (non empty) string"
    assert repr(config), "Should return a (non empty) string"


#
# Test Feature Configuration
#

@pytest.mark.django_db
def test_network_feature_config():
    """Test polymorphic feature config base"""
    customer = mommy.make("crm.Customer")
    service_config = mommy.make(ExchangeLanNetworkServiceConfig)
    config = mommy.make(NetworkFeatureConfig,
                        network_service_config=service_config,
                        managing_customer=customer,
                        consuming_customer=customer)

    # Check relations and reverse relations
    assert config in service_config.network_feature_configs.all()
    assert config in customer.managed_networkfeatureconfigs.all()
    assert config in customer.consumed_networkfeatureconfigs.all()


@pytest.mark.django_db
def test_route_server_network_feature_config():
    """Test route server feature config model"""
    config = mommy.make(RouteServerNetworkFeatureConfig)

    # All relations have been checked in the base model,
    # so we should make sure, that this just saves
    assert config.pk, "A primary key should have been assigned."


def test_route_server_network_feature_config_representation():
    """Test make representation of feature config"""
    config = mommy.prepare(RouteServerNetworkFeatureConfig)
    assert str(config), "Should return a (non empty) string"
    assert repr(config), "Should return a (non empty) string"


@pytest.mark.django_db
def test_ixp_router_network_feature_config():
    """Test IXP router feature config model"""
    config = mommy.make(IXPRouterNetworkFeatureConfig)
    assert config.pk, "A primary key should have been assigned."


def test_ixp_router_network_feature_config_representation():
    """Test make representation of the feature config"""
    config = mommy.prepare(IXPRouterNetworkFeatureConfig)
    assert str(config), "Should return a (non empty) string"
    assert repr(config), "Should return a (non empty) string"


@pytest.mark.django_db
def test_blackholing_network_feature_config():
    """Test route server feature config model"""
    config = mommy.make(BlackholingNetworkFeatureConfig)
    assert config.pk, "A primary key should have been assigned."

    # Blackholing has a list of prefices:
    prefix_a = mommy.make("ipam.IpAddress")
    prefix_b = mommy.make("ipam.IpAddress")

    config.filtered_prefixes.add(prefix_a)
    config.filtered_prefixes.add(prefix_b)

    # Check relations
    assert prefix_a in set(config.filtered_prefixes.all())
    assert prefix_b.blackholing_network_feature_config == config

    # Check cleaning up models
    config.delete()
    with pytest.raises(ObjectDoesNotExist):
        prefix_a.refresh_from_db()


def test_blackholing_feature_config_representation():
    """Test make representation of blackholing feature config"""
    config = mommy.prepare(BlackholingNetworkFeatureConfig)
    assert str(config), "Should return a (non empty) string"
    assert repr(config), "Should return a (non empty) string"

