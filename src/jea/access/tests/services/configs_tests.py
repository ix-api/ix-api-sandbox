
"""
Test config service
"""

import pytest
from model_mommy import mommy

from jea.exceptions import ResourceAccessDenied
from jea.crm.exceptions import (
    RequiredContactTypesInvalid,
)
from jea.access.services import (
    configs as configs_svc,
)
from jea.access.models import (
    NetworkServiceConfig,
    BGPSessionType,
    RouteServerSessionMode,
)
from jea.service.exceptions import NetworkFeatureNotAvailable


@pytest.mark.django_db
def test_assign_network_service_contacts():
    """Test (implementation) contact assignment"""
    service = mommy.make(
        "service.ExchangeLanNetworkService",
        required_contact_types=["implementation"])
    config = mommy.make(
        "access.ExchangeLanNetworkServiceConfig",
        network_service=service)
    impl = mommy.make(
        "crm.ImplementationContact",
        scoping_customer=config.scoping_customer)

    assert not impl in config.contacts.all()

    # Assign network service config
    configs_svc.assign_network_service_contacts(
        network_service_config=config,
        contacts=[impl])
    assert impl in config.contacts.all()

    # We currently don't have network feature configs
    # with implementation_contacts.


@pytest.mark.django_db
def test_assign_network_service_contacts__ownership_fails():
    """
    Test (implementation) contact assignment
    with a failing ownership test.
    """
    # Create a config and an implementation contact.
    # Both have different scoping_customers.
    c1 = mommy.make("crm.Customer")
    c2 = mommy.make("crm.Customer")
    service = mommy.make(
        "service.ExchangeLanNetworkService",
        required_contact_types=["implementation"])
    config = mommy.make(
        "access.ExchangeLanNetworkServiceConfig",
        network_service=service,
        scoping_customer=c1)
    impl = mommy.make(
        "crm.ImplementationContact",
        scoping_customer=c2)
    assert config.scoping_customer != impl.scoping_customer

    # Assign config
    with pytest.raises(ResourceAccessDenied):
        configs_svc.assign_network_service_contacts(
            scoping_customer=config.scoping_customer,
            network_service_config=config,
            contacts=[impl])


@pytest.mark.django_db
def test_assign_network_service_contacts__ownership_override():
    """
    Test (implementation) contact assignment with
    ownership override.
    """
    service = mommy.make(
        "service.ExchangeLanNetworkService",
        required_contact_types=["implementation"])
    config = mommy.make(
        "access.ExchangeLanNetworkServiceConfig",
        network_service=service)
    impl = mommy.make(
        "crm.ImplementationContact",
        scoping_customer=config.scoping_customer)

    assert not impl in config.contacts.all()

    # Assign config
    configs_svc.assign_network_service_contacts(
        scoping_customer=impl.scoping_customer,
        network_service_config=config,
        contacts=[impl])

    assert impl in config.contacts.all()


@pytest.mark.django_db
def test_assign_network_service_contacts__clear():
    """Test removal of all (implementation) contacts from object"""
    impl = mommy.make(
        "crm.ImplementationContact")
    service = mommy.make(
        "service.ExchangeLanNetworkService",
        required_contact_types=[])
    config = mommy.make(
        "access.ExchangeLanNetworkServiceConfig",
        network_service=service,
        scoping_customer=impl.scoping_customer,
        contacts=[impl])

    assert impl in config.contacts.all()

    # Assign config
    configs_svc.assign_network_service_contacts(
        scoping_customer=config.scoping_customer,
        network_service_config=config)

    assert not impl in config.contacts.all()
    assert not config.contacts.count()


@pytest.mark.django_db
def test_assign_mac_addresses():
    """Test assigning mac addresses to a service config"""
    config = mommy.make(
        "access.ExchangeLanNetworkServiceConfig")
    mac = mommy.make(
        "ipam.MacAddress",
        scoping_customer=config.scoping_customer,
        managing_customer=config.managing_customer,
        consuming_customer=config.consuming_customer)

    assert not mac in config.mac_addresses.all()

    # Assign mac
    configs_svc.assign_mac_addresses(
        network_service_config=config,
        mac_addresses=[mac])

    assert mac in config.mac_addresses.all()


@pytest.mark.django_db
def test_assign_mac_addresses__invalid_config_type():
    """Test assigning a mac address to an unsupport network service config"""
    config = mommy.make("access.CloudNetworkServiceConfig")
    mac = mommy.make("ipam.MacAddress")

    with pytest.raises(TypeError):
        configs_svc.assign_mac_addresses(
            network_service_config=config,
            mac_addresses=[mac])


@pytest.mark.django_db
def test_assign_mac_addresses__ownership():
    """Test mac addrss assignment with ownership fail and override"""
    c1 = mommy.make("crm.Customer")
    c2 = mommy.make("crm.Customer")
    config = mommy.make(
        "access.ExchangeLanNetworkServiceConfig",
        scoping_customer=c1)
    mac = mommy.make(
        "ipam.MacAddress",
        scoping_customer=c2)

    # Preflight check
    assert mac not in config.mac_addresses.all()

    with pytest.raises(ResourceAccessDenied):
        configs_svc.assign_mac_addresses(
            network_service_config=config,
            mac_addresses=[mac])

    # Override config ownership
    configs_svc.assign_mac_addresses(
        scoping_customer=mac.scoping_customer,
        network_service_config=config,
        mac_addresses=[mac])

    assert mac in config.mac_addresses.all()


@pytest.mark.django_db
def test_assign_mac_addresses__clear():
    """Test removal of mac addresses"""
    mac = mommy.make("ipam.MacAddress")
    config = mommy.make(
        "access.ExchangeLanNetworkServiceConfig",
        mac_addresses=[mac])

    # Clear
    configs_svc.assign_mac_addresses(
        network_service_config=config)

    assert not mac in config.mac_addresses.all()
    assert not config.mac_addresses.count()


@pytest.mark.django_db
def test_get_network_service_configs():
    """Test getting network service configs"""
    c1 = mommy.make("crm.Customer")
    c2 = mommy.make("crm.Customer")

    s1 = mommy.make(
        "access.ExchangeLanNetworkServiceConfig",
        scoping_customer=c1)
    s2 = mommy.make(
        "access.ClosedUserGroupNetworkServiceConfig",
        scoping_customer=c2)

    configs = configs_svc.get_network_service_configs(
        scoping_customer=c1)

    assert s1 in configs
    assert not s2 in configs


@pytest.mark.django_db
def test_get_network_service_configs__filters__type():
    """Test getting network service configs with filtering"""
    c1 = mommy.make("crm.Customer")

    # Make some configs
    s1 = mommy.make(
        "access.ExchangeLanNetworkServiceConfig",
        scoping_customer=c1)
    s2 = mommy.make(
        "access.ClosedUserGroupNetworkServiceConfig",
        scoping_customer=c1)

    # query configs
    configs = configs_svc.get_network_service_configs(
        scoping_customer=c1,
        filters={
            "type": "exchange_lan",
        })

    assert s1 in configs
    assert not s2 in configs


@pytest.mark.django_db
def test_get_network_service_config__lookup():
    """Test getting a single network service config"""
    c1 = mommy.make("crm.Customer")
    c2 = mommy.make("crm.Customer")

    # Make some configs
    s1 = mommy.make("access.ExchangeLanNetworkServiceConfig",
        scoping_customer=c2)

    # Check lookup
    config = configs_svc.get_network_service_config(
        network_service_config=str(s1.pk),
        scoping_customer=c2)

    assert config.pk == s1.pk

    # Check invalid pk
    with pytest.raises(NetworkServiceConfig.DoesNotExist):
        configs_svc.get_network_service_config(
            network_service_config="123459012345690",
            scoping_customer=c1)


@pytest.mark.django_db
def test_get_network_service_config_ownership():
    """Test ownership test of single network service config"""
    c1 = mommy.make("crm.Customer")
    c2 = mommy.make("crm.Customer")

    # Make some configs
    s1 = mommy.make(
        "access.ExchangeLanNetworkServiceConfig",
        scoping_customer=c1)
    s2 = mommy.make(
        "access.ExchangeLanNetworkServiceConfig",
        scoping_customer=c2)

    # Check lookup
    config = configs_svc.get_network_service_config(
        network_service_config=s1,
        scoping_customer=c1)

    assert config.pk == s1.pk
    assert id(config) == id(s1)

    # Check invalid ownership
    with pytest.raises(ResourceAccessDenied):
        configs_svc.get_network_service_config(
            network_service_config=s2,
            scoping_customer=c1)


@pytest.mark.django_db
def test_create_eline_network_service_config():
    """Test creating an eline network service config"""
    c1 = mommy.make("crm.Customer")

    # We need a connection
    connection = mommy.make("access.Connection",
        scoping_customer=c1)

    # Implementation contact
    impl = mommy.make("crm.ImplementationContact",
        scoping_customer=c1)

    # Create a network service
    service = mommy.make(
        "service.ELineNetworkService",
        required_contact_types=[])

    config_input = {
        "type": "eline",
        "inner_vlan": None,
        "outer_vlan": [[0, 4095]],
        "capacity": None,
        "purchase_order": "PO2342-1111",
        "external_ref": "23199",
        "contract_ref": None,
        "network_service": service,
        "connection": connection,
        "managing_customer": c1,
        "consuming_customer": c1,
    }

    # Create the config
    config = configs_svc.create_network_service_config(
        network_service_config_input=config_input,
        scoping_customer=c1)

    assert config
    assert config.pk
    assert not impl in config.contacts.all()


@pytest.mark.django_db
def test_create_exchange_lan_network_service_config():
    """Test creating an exchange lan network service config"""
    c1 = mommy.make("crm.Customer")

    # We need a connection
    connection = mommy.make("access.Connection",
        scoping_customer=c1)

    # Implementation contact
    impl = mommy.make("crm.ImplementationContact",
        scoping_customer=c1)

    # A mac address
    mac = mommy.make("ipam.MacAddress",
        scoping_customer=c1)

    # Create a network service
    service = mommy.make("service.ExchangeLanNetworkService",
        required_contact_types=["implementation"],
        scoping_customer=c1)

    net4 = mommy.make("ipam.IpAddress",
        version=4,
        address="123.42.42.0",
        prefix_length=24,
        exchange_lan_network_service=service)

    net6 = mommy.make("ipam.IpAddress",
        version=6,
        address="1000:23:42::0",
        prefix_length=64,
        exchange_lan_network_service=service)

    config_input = {
        "type": "exchange_lan",
        "asns": [1234],
        "mac_addresses": [mac],
        "inner_vlan": None,
        "outer_vlan": [[0, 4095]],
        "capacity": None,
        "purchase_order": "PO2342-1111",
        "external_ref": "23199",
        "contract_ref": None,
        "network_service": service,
        "contacts": [impl],
        "connection": connection,
        "managing_customer": c1,
        "consuming_customer": c1,
    }

    # Create the config
    config = configs_svc.create_network_service_config(
        network_service_config_input=config_input,
        scoping_customer=c1)

    assert config
    assert config.pk
    assert impl in config.contacts.all()
    assert mac in config.mac_addresses.all()


@pytest.mark.django_db
def test_create_closed_user_group_network_service_config():
    """Test creating a closed user group network service config"""
    c1 = mommy.make("crm.Customer")

    # We need a connection
    connection = mommy.make("access.Connection",
        scoping_customer=c1)

    # Implementation contact
    impl = mommy.make("crm.ImplementationContact",
        scoping_customer=c1)

    # Create a network service
    service = mommy.make("service.ClosedUserGroupNetworkService",
        required_contact_types=["implementation"],
        scoping_customer=c1)

    config_input = {
        "type": "closed_user_group",
        "inner_vlan": None,
        "outer_vlan": None,
        "capacity": None,
        "purchase_order": "PO2342-1111",
        "external_ref": "23199",
        "contract_ref": None,
        "network_service": service,
        "contacts": [impl],
        "connection": connection,
        "managing_customer": c1,
        "consuming_customer": c1,
    }

    # Create the config
    config = configs_svc.create_network_service_config(
        network_service_config_input=config_input,
        scoping_customer=c1)

    assert config
    assert config.pk
    assert impl in config.contacts.all()


@pytest.mark.django_db
def test_create_cloud_network_service_config():
    """Test creating a cloud network service config"""
    c1 = mommy.make("crm.Customer")

    # We need a connection
    connection = mommy.make("access.Connection",
        scoping_customer=c1)

    # Implementation contact
    impl = mommy.make("crm.ImplementationContact",
        scoping_customer=c1)

    # Create a network service
    service = mommy.make("service.CloudNetworkService",
        required_contact_types=["implementation"],
        scoping_customer=c1)

    config_input = {
        "type": "cloud",
        "cloud_key": "FF00FF00AAAAAA==",
        "inner_vlan": None,
        "outer_vlan": None,
        "capacity": None,
        "purchase_order": "PO2342-1111",
        "external_ref": "23199",
        "contract_ref": None,
        "network_service": service,
        "contacts": [impl],
        "connection": connection,
        "managing_customer": c1,
        "consuming_customer": c1,
    }

    # Create the config
    config = configs_svc.create_network_service_config(
        network_service_config_input=config_input,
        scoping_customer=c1)

    assert config
    assert config.pk
    assert impl in config.contacts.all()


@pytest.mark.django_db
def test_update_eline_network_service_config():
    """Update an existing eline config"""
    customer = mommy.make("crm.Customer")
    network_service = mommy.make(
        "service.ELineNetworkService",
        required_contact_types=["implementation"])
    config = mommy.make(
        "access.ELineNetworkServiceConfig",
        network_service=network_service,
        scoping_customer=customer)
    contact = mommy.make(
        "crm.ImplementationContact",
        scoping_customer=customer)
    mac = mommy.make(
        "ipam.MacAddress",
        scoping_customer=config.scoping_customer,
        managing_customer=config.managing_customer,
        consuming_customer=config.consuming_customer)

    # Prepare update
    po_num = "FOO-23-2000-01"
    inner_vlan = 23
    managing_customer = "FAIL"

    update = {
        "purchase_order": po_num,
        "inner_vlan": inner_vlan,
        "managing_customer": customer,
        "consuming_customer": customer,
        "contacts": [contact],
    }

    # Update exchange lan network service config
    config = configs_svc.update_network_service_config(
        scoping_customer=customer,
        network_service_config=config,
        network_service_config_update=update)

    assert contact in config.contacts.all()
    assert config.purchase_order == po_num
    assert config.inner_vlan == inner_vlan
    assert config.managing_customer == customer
    assert config.consuming_customer == customer


@pytest.mark.django_db
def test_update_exchange_lan_network_service_config():
    """Update an exchange lan network service config"""
    customer = mommy.make("crm.Customer")
    service = mommy.make(
        "service.ExchangeLanNetworkService",
        required_contact_types=["implementation"])
    config = mommy.make(
        "access.ExchangeLanNetworkServiceConfig",
        network_service=service,
        scoping_customer=customer)
    contact = mommy.make(
        "crm.ImplementationContact",
        scoping_customer=customer)
    mac = mommy.make(
        "ipam.MacAddress",
        scoping_customer=customer,
        managing_customer=config.managing_customer,
        consuming_customer=config.consuming_customer)

    # Prepare update
    po_num = "FOO-23-2000-01"
    inner_vlan = 23
    managing_customer = mommy.make(
        "crm.Customer", scoping_customer=customer)
    consuming_customer = mommy.make(
        "crm.Customer", scoping_customer=customer)

    update = {
        "purchase_order": po_num,
        "inner_vlan": inner_vlan,
        "managing_customer": managing_customer,
        "consuming_customer": consuming_customer,
        "contacts": [contact],
        "mac_addresses": [mac],
        "asns": [42],
    }

    # Update exchange lan network service config
    config = configs_svc.update_network_service_config(
        scoping_customer=customer,
        network_service_config=config,
        network_service_config_update=update)

    assert mac in config.mac_addresses.all()
    assert contact in config.contacts.all()
    assert config.purchase_order == po_num
    assert config.inner_vlan == inner_vlan
    assert config.managing_customer == managing_customer
    assert config.consuming_customer == consuming_customer


@pytest.mark.django_db
def test_update_closed_user_group_network_service_config():
    """Update a closed user group service config"""
    customer = mommy.make(
        "crm.Customer")
    service = mommy.make(
        "service.ClosedUserGroupNetworkService",
        required_contact_types=["implementation"])
    config = mommy.make(
        "access.ClosedUserGroupNetworkServiceConfig",
        network_service=service,
        scoping_customer=customer)
    contact = mommy.make(
        "crm.ImplementationContact",
        scoping_customer=customer)
    mac = mommy.make(
        "ipam.MacAddress",
        scoping_customer=customer)

    # Prepare update
    po_num = "FOO-23-2000-01"
    inner_vlan = 23
    managing_customer = mommy.make(
        "crm.Customer",
        scoping_customer=customer)
    consuming_customer = mommy.make(
        "crm.Customer",
        scoping_customer=customer)

    update = {
        "purchase_order": po_num,
        "inner_vlan": inner_vlan,
        "managing_customer": managing_customer,
        "consuming_customer": consuming_customer,
        "contacts": [contact],
        "mac_addresses": [mac],
    }

    # Update exchange lan network service config
    config = configs_svc.update_network_service_config(
        scoping_customer=customer,
        network_service_config=config,
        network_service_config_update=update)

    assert mac in config.mac_addresses.all()
    assert contact in config.contacts.all()
    assert config.purchase_order == po_num
    assert config.inner_vlan == inner_vlan
    assert config.managing_customer == managing_customer
    assert config.consuming_customer == consuming_customer


@pytest.mark.django_db
def test_update_cloud_network_service_config():
    """Update a cloud service config"""
    customer = mommy.make(
        "crm.Customer")
    service = mommy.make(
        "service.CloudNetworkService",
        required_contact_types=["implementation"])
    config = mommy.make(
        "access.CloudNetworkServiceConfig",
        network_service=service,
        scoping_customer=customer)
    contact = mommy.make(
        "crm.ImplementationContact",
        scoping_customer=customer)

    # Prepare update
    po_num = "FOO-23-2000-01"
    managing_customer = customer
    consuming_customer = mommy.make(
        "crm.Customer",
        scoping_customer=customer)
    cloud_key = "CCC-123456"

    update = {
        "purchase_order": po_num,
        "managing_customer": managing_customer,
        "consuming_customer": consuming_customer,
        "contacts": [contact],
        "cloud_key": cloud_key
    }

    # Update exchange lan network service config
    config = configs_svc.update_network_service_config(
        scoping_customer=customer,
        network_service_config=config,
        network_service_config_update=update)

    assert contact in config.contacts.all()
    assert config.purchase_order == po_num
    assert config.cloud_key == cloud_key
    assert config.managing_customer == managing_customer
    assert config.consuming_customer == consuming_customer


@pytest.mark.django_db
def test_destroy_network_service_config():
    """Test destruction of a network service config"""
    config = mommy.make("access.ExchangeLanNetworkServiceConfig",
        _fill_optional=True)

    # Destroy this config
    configs_svc.destroy_network_service_config(
        scoping_customer=config.scoping_customer,
        network_service_config=config)


@pytest.mark.django_db
def test_create_invalid_network_feature_config():
    """Test creating a network feature config for an incompatible service"""
    customer = mommy.make(
        "crm.Customer")
    managing_customer = mommy.make(
        "crm.Customer",
        scoping_customer=customer)
    consuming_customer = mommy.make(
        "crm.Customer",
        scoping_customer=customer)
    service = mommy.make(
        "service.ExchangeLanNetworkService")
    feature = mommy.make(
        "service.BlackholingNetworkFeature")
    service_config = mommy.make(
        "access.ExchangeLanNetworkServiceConfig",
        network_service=service,
        scoping_customer=customer,
        managing_customer=managing_customer,
        consuming_customer=consuming_customer)

    with pytest.raises(NetworkFeatureNotAvailable):
        configs_svc.create_network_feature_config(
            scoping_customer=customer,
            network_feature_config_input={
                "network_feature": feature,
                "network_service_config": service_config,
                "managing_customer": managing_customer,
                "consuming_customer": consuming_customer,
            })


@pytest.mark.django_db
def test_create_blackholing_network_feature_config():
    """Test feature configuration creation: Blackholing"""
    customer = mommy.make(
        "crm.Customer")
    managing_customer = mommy.make(
        "crm.Customer",
        scoping_customer=customer)
    consuming_customer = mommy.make(
        "crm.Customer",
        scoping_customer=customer)
    service = mommy.make(
        "service.ExchangeLanNetworkService")
    feature = mommy.make(
        "service.BlackholingNetworkFeature",
        network_service=service)
    service_config = mommy.make(
        "access.ExchangeLanNetworkServiceConfig",
        network_service=service,
        scoping_customer=customer,
        managing_customer=managing_customer,
        consuming_customer=consuming_customer)

    config_input = {
        "type": "blackholing",
        "network_service_config": service_config,
        "network_feature": feature,
        "purchase_order": "PO2222-1111",
        "external_ref": "CX000000420",
        "contract_ref": None,
        "activated": True,
        "ixp_specific_configuration": None,
        "managing_customer": managing_customer,
        "consuming_customer": consuming_customer,
    }

    config = configs_svc.create_network_feature_config(
        scoping_customer=customer,
        network_feature_config_input=config_input)

    # Assertions - at lest it should be created without error.
    assert config
    assert config.pk


@pytest.mark.django_db
def test_create_ixp_router_network_feature_config():
    """Test feature configuration creation: IXPRouter"""
    customer = mommy.make(
        "crm.Customer")
    managing_customer = mommy.make(
        "crm.Customer",
        scoping_customer=customer)
    consuming_customer = mommy.make(
        "crm.Customer",
        scoping_customer=customer)
    service = mommy.make(
        "service.ExchangeLanNetworkService")
    feature = mommy.make(
        "service.IXPRouterNetworkFeature",
        network_service=service)
    service_config = mommy.make(
        "access.ExchangeLanNetworkServiceConfig",
        network_service=service,
        scoping_customer=customer,
        managing_customer=managing_customer,
        consuming_customer=consuming_customer)


    config_input = {
        "type": "ixp_router",
        "network_service_config": service_config,
        "network_feature": feature,
        "purchase_order": "PO2222-1111",
        "external_ref": "CX000000420",
        "contract_ref": None,
        "password": None,
        "bgp_session_type": BGPSessionType.TYPE_ACTIVE,
        "max_prefix": 5000,
        "managing_customer": managing_customer,
        "consuming_customer": consuming_customer,
    }

    config = configs_svc.create_network_feature_config(
        scoping_customer=customer,
        network_feature_config_input=config_input)

    # Assertions
    assert config
    assert config.pk
    assert config.password
    assert isinstance(config.password, str)


@pytest.mark.django_db
def test_create_route_server_network_feature_config():
    """Test feature configuration creation: RouteServer"""
    customer = mommy.make(
        "crm.Customer")
    managing_customer = mommy.make(
        "crm.Customer",
        scoping_customer=customer)
    consuming_customer = mommy.make(
        "crm.Customer",
        scoping_customer=customer)

    service = mommy.make("service.ExchangeLanNetworkService")
    feature = mommy.make("service.RouteServerNetworkFeature",
                         available_bgp_session_types=[
                            BGPSessionType.TYPE_ACTIVE,
                            BGPSessionType.TYPE_PASSIVE,
                         ],
                         required_contact_types=[],
                         network_service=service,
                         address_families=['af_inet'])

    service_config = mommy.make(
        "access.ExchangeLanNetworkServiceConfig",
        network_service=service,
        scoping_customer=customer,
        managing_customer=managing_customer,
        consuming_customer=consuming_customer)

    # AS-SET is dependent on the mode
    as_set_v4 = None
    if feature.session_mode == RouteServerSessionMode.MODE_PUBLIC:
        as_set_v4 = "AS-FOO@RIPE"

    config_input = {
        "type": "route_server",
        "network_service_config": service_config,
        "network_feature": feature,
        "purchase_order": "PO2222-1111",
        "external_ref": "CX000000420",
        "contract_ref": None,
        "password": None,
        "asn": 2342,
        "as_set_v4": as_set_v4,
        "bgp_session_type": BGPSessionType.TYPE_ACTIVE,
        "session_mode": feature.session_mode,
        "insert_ixp_asn": False,
        "max_prefix_v4": 5000,
        "managing_customer": managing_customer,
        "consuming_customer": consuming_customer,
        "contacts": [],
    }

    config = configs_svc.create_network_feature_config(
        scoping_customer=customer,
        network_feature_config_input=config_input)

    # Assertions
    assert config
    assert config.pk
    assert config.password
    assert isinstance(config.password, str)


@pytest.mark.django_db
def test_update_blackholing_network_feature_config():
    """Test updating a blackholing network feature configuration"""
    config = mommy.make("access.BlackholingNetworkFeatureConfig")

    # Prepare update
    purchase_order = "FOO-23-42-1"
    external_ref = "11111-2"
    activated = True
    ixp_specific_configuration = "for something do icmp_ping_of_death();"

    update = {
        "purchase_order": purchase_order,
        "external_ref": external_ref,
        "activated": activated,
        "ixp_specific_configuration": ixp_specific_configuration,
    }

    # Perform update
    config = configs_svc.update_network_feature_config(
        scoping_customer=config.scoping_customer,
        network_feature_config=config,
        network_feature_config_update=update)

    # Assertions
    assert config.purchase_order == purchase_order
    assert config.external_ref == external_ref
    assert config.activated == activated
    assert config.ixp_specific_configuration == ixp_specific_configuration


@pytest.mark.django_db
def test_update_route_server_network_feature_config():
    """Test updating a routeserver network feature configuration"""
    feature = mommy.make("service.RouteServerNetworkFeature",
        address_families=["af_inet6"])
    config = mommy.make("access.RouteServerNetworkFeatureConfig",
        network_feature=feature)

    # Prepare update
    purchase_order = "FOO-23-42-1"
    external_ref = "11111-2"
    insert_ixp_asn = True
    password = "password"

    update = {
        "purchase_order": purchase_order,
        "external_ref": external_ref,
        "insert_ixp_asn": insert_ixp_asn,
        "password": password,
        "as_set_v6": "AS-FOO@BAR",
    }

    # Perform update
    config = configs_svc.update_network_feature_config(
        network_feature_config=config,
        network_feature_config_update=update)

    # Assertions
    assert config.purchase_order == purchase_order
    assert config.external_ref == external_ref
    assert config.insert_ixp_asn == insert_ixp_asn
    assert config.password == password


@pytest.mark.django_db
def test_update_ixp_router_network_feature_config():
    """Test updating an ixprouter network feature configuration"""
    config = mommy.make("access.IXPRouterNetworkFeatureConfig")

    # Prepare update
    purchase_order = "FOO-23-42-1"
    external_ref = "11111-2"
    password = "password"

    update = {
        "purchase_order": purchase_order,
        "external_ref": external_ref,
        "password": password,
    }

    # Perform update
    config = configs_svc.update_network_feature_config(
        scoping_customer=config.scoping_customer,
        network_feature_config=config,
        network_feature_config_update=update)

    # Assertions
    assert config.purchase_order == purchase_order
    assert config.external_ref == external_ref
    assert config.password == password

