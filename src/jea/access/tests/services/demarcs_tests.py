
"""
Tests for Demarcs Service
"""

import pytest
from model_mommy import mommy

from jea.exceptions import ResourceAccessDenied
from jea.eventmachine.models import (
    State,
)
from jea.access.models import  (
    MediaType,
    DemarcationPoint,
)
from jea.access.services import demarcs as demarcs_svc
from jea.access.exceptions import (
    DemarcationPointUnavailable,
    DemarcationPointInUse,
    DemarcationPointNotReady,
)


def test_max_speed_for_media_type():
    """Test get max connection speed"""
    # Check properties
    for c_type in MediaType:
        speed = demarcs_svc.max_speed_for_media_type(c_type.value)
        assert speed > 0

    # Unknown type
    with pytest.raises(demarcs_svc.UnknownMediaTypeError):
        demarcs_svc.max_speed_for_media_type("FOO")


@pytest.mark.django_db
def test_allocate_demarcation_point():
    """Test port demarc allocation"""
    customer = mommy.make(
        "crm.Customer")
    managing_customer = mommy.make(
        "crm.Customer",
        scoping_customer=customer)
    consuming_customer = mommy.make(
        "crm.Customer",
        scoping_customer=customer)
    pop = mommy.make(
        "catalog.PointOfPresence")
    device = mommy.make(
        "catalog.Device",
        physical_point_of_presence=pop)
    mommy.make(
        "catalog.DeviceCapability",
        device=device,
        media_type=MediaType.TYPE_10GBASE_LR.value,
        availability_count=1)
    impl_contact = mommy.make(
        "crm.ImplementationContact",
        scoping_customer=customer,
        consuming_customer=consuming_customer)
    billing_contact = mommy.make(
        "crm.BillingContact",
        scoping_customer=customer,
        consuming_customer=consuming_customer)

    demarc_input = {
        "name": "demarc-aaa",
        "media_type": MediaType.TYPE_10GBASE_LR.value,
        "external_ref": "refref2000",
        "point_of_presence": pop,
        "contacts": [impl_contact, billing_contact],
        "managing_customer": managing_customer,
        "consuming_customer": consuming_customer,
    }

    demarc = demarcs_svc.allocate_demarcation_point(
        scoping_customer=customer,
        demarcation_point_input=demarc_input)

    assert demarc
    assert demarc.pk



@pytest.mark.django_db
def test_allocate_demarcation_point_no_capacity():
    """Test port demarc allocation"""
    customer = mommy.make(
        "crm.Customer")
    managing_customer = mommy.make(
        "crm.Customer",
        scoping_customer=customer)
    consuming_customer = mommy.make(
        "crm.Customer",
        scoping_customer=customer)
    pop = mommy.make("catalog.PointOfPresence")
    device = mommy.make(
        "catalog.Device",
        physical_point_of_presence=pop)
    mommy.make(
        "catalog.DeviceCapability",
        device=device,
        media_type=MediaType.TYPE_10GBASE_LR.value,
        availability_count=0)

    demarc_input = {
        "name": "port-demarc-bbb",
        "media_type": MediaType.TYPE_10GBASE_LR.value,
        "external_ref": "refref2001",
        "point_of_presence": pop,
        "managing_customer": managing_customer,
        "consuming_customer": consuming_customer,
    }

    with pytest.raises(demarcs_svc.DemarcationPointUnavailable):
        demarcs_svc.allocate_demarcation_point(
            scoping_customer=customer,
            demarcation_point_input=demarc_input)


@pytest.mark.django_db
def test_release_demarcation_point():
    """Release a port demarc"""
    demarc = mommy.make("access.DemarcationPoint")
    demarcs_svc.release_demarcation_point(
        demarcation_point=demarc)

    assert demarc.state == State.DECOMMISSIONED


@pytest.mark.django_db
def test_release_demarcation_point_in_use():
    """Release a port demarc while in use"""
    connection = mommy.make(
        "access.Connection")
    demarc = mommy.make(
        "access.DemarcationPoint",
        connection=connection)

    with pytest.raises(demarcs_svc.DemarcationPointInUse):
        demarcs_svc.release_demarcation_point(
            demarcation_point=demarc)


@pytest.mark.django_db
def test_get_demarcation_points():
    """Test get all port demarcs for customers"""
    customer_a = mommy.make(
        "crm.Customer")
    customer_b = mommy.make(
        "crm.Customer")

    demarc_a = mommy.make(
        "access.DemarcationPoint",
        scoping_customer=customer_a)
    demarc_b = mommy.make(
        "access.DemarcationPoint",
        scoping_customer=customer_b,
        _fill_optional=True)

    demarcs = demarcs_svc.get_demarcation_points(
        scoping_customer=customer_a)

    assert demarc_a in demarcs
    assert not demarc_b in demarcs


@pytest.mark.django_db
def test_get_demarcation_points_filtered():
    """Test get all port demarcs for customers"""
    customer = mommy.make(
        "crm.Customer")
    demarc_a = mommy.make(
        "access.DemarcationPoint",
        scoping_customer=customer,
        _fill_optional=True)

    demarc_b = mommy.make(
        "access.DemarcationPoint",
        scoping_customer=customer,
        _fill_optional=True)


    # Filter by connection id:
    demarcs = demarcs_svc.get_demarcation_points(
        scoping_customer=customer,
        filters={"connection": demarc_b.connection.id})

    assert demarc_b in demarcs
    assert demarc_a not in demarcs

    # Or external ref...
    demarcs = demarcs_svc.get_demarcation_points(
        scoping_customer=customer,
        filters={"external_ref": demarc_a.external_ref})

    assert demarc_a in demarcs
    assert demarc_b not in demarcs


@pytest.mark.django_db
def test_get_demarcation_points_exclude_archived():
    """Test get all port demarcs for customers"""
    customer = mommy.make(
        "crm.Customer")
    demarc_a = mommy.make(
        "access.DemarcationPoint",
        state=State.ARCHIVED,
        scoping_customer=customer,
        _fill_optional=True)

    demarc_b = mommy.make(
        "access.DemarcationPoint",
        state=State.PRODUCTION,
        scoping_customer=customer)

    # Filter by connection id:
    demarcs = demarcs_svc.get_demarcation_points(
        scoping_customer=customer,
        filters={"state__is_not": "archived"})

    assert not demarc_a in demarcs
    assert demarc_b in demarcs


@pytest.mark.django_db
def test_get_dmearcation_point():
    """Test getting a signle port demarc"""
    customer_a = mommy.make("crm.Customer")
    customer_b = mommy.make("crm.Customer")

    demarc_a = mommy.make(
        "access.DemarcationPoint",
        scoping_customer=customer_a)
    demarc_b = mommy.make(
        "access.DemarcationPoint",
        scoping_customer=customer_b)

    # Resolve demarcs
    demarc = demarcs_svc.get_demarcation_point(
        scoping_customer=customer_a,
        demarcation_point=str(demarc_a.id))
    assert demarc == demarc_a

    with pytest.raises(ResourceAccessDenied):
        demarcs_svc.get_demarcation_point(
            scoping_customer=customer_a,
            demarcation_point=demarc_b.id)


@pytest.mark.django_db
def test_get_dmearcation_point_scope_check():
    customer_a = mommy.make("crm.Customer")
    customer_b = mommy.make("crm.Customer")

    demarc_a = mommy.make(
        "access.DemarcationPoint",
        scoping_customer=customer_a)

    demarc_b = mommy.make(
        "access.DemarcationPoint",
        scoping_customer=customer_b)

    # We pass a port demarc object to skip lookup:
    # - Managing customers matches.
    demarc = demarcs_svc.get_demarcation_point(
        demarcation_point=demarc_a,
        scoping_customer=customer_a)
    assert demarc == demarc_a

    # - Managing customers missmatch
    with pytest.raises(ResourceAccessDenied):
        demarcs_svc.get_demarcation_point(
            scoping_customer=customer_a,
            demarcation_point=demarc_b)


@pytest.mark.django_db
def test_update_demarcation_point():
    customer = mommy.make("crm.Customer")
    contact = mommy.make("crm.ImplementationContact",
        scoping_customer=customer,
        consuming_customer=customer)
    connection = mommy.make("access.Connection",
        scoping_customer=customer)
    demarc = mommy.make("access.DemarcationPoint",
        scoping_customer=customer)

    # Make Update
    update = {
        "connection": connection,
        "speed": 23,
        "contacts": [contact],
    }

    demarc = demarcs_svc.update_demarcation_point(
        scoping_customer=customer,
        demarcation_point=demarc,
        demarcation_point_update=update)

    assert demarc.connection == connection
    assert demarc.speed == 23
    assert contact in demarc.contacts.all()


@pytest.mark.django_db
def test_demarcation_point_can_join_connection():
    """Test connect demarc preflight check"""
    connection = mommy.make("access.Connection")
    demarc = mommy.make("access.DemarcationPoint",
        state=State.ALLOCATED)

    # This should work
    demarcs_svc.demarcation_point_can_join_connection(
        demarcation_point=demarc,
        connection=connection)


@pytest.mark.django_db
def test_demarcation_point_can_join_connection__in_use():
    """Test connect demarc preflight check"""
    connection = mommy.make("access.Connection")
    demarc = mommy.make("access.DemarcationPoint",
        state=State.ALLOCATED,
        connection=mommy.make("access.Connection"))

    # We should get demarc in use
    with pytest.raises(DemarcationPointInUse):
        demarcs_svc.demarcation_point_can_join_connection(
            demarcation_point=demarc,
            connection=connection)


@pytest.mark.django_db
def test_demarcation_point_can_join_connection__not_ready():
    """Test connect demarc preflight check: state error"""
    connection = mommy.make("access.Connection")
    demarc = mommy.make("access.DemarcationPoint",
        state=State.PRODUCTION)

    with pytest.raises(DemarcationPointNotReady):
        demarcs_svc.demarcation_point_can_join_connection(
            demarcation_point=demarc,
            connection=connection)


@pytest.mark.django_db
def test_demarcation_point_can_join_connection__incompatible():
    """Test connect demarc preflight check: incompatible with connection"""
    connection = mommy.make("access.Connection")
    demarc = mommy.make("access.DemarcationPoint",
        state=State.ALLOCATED)

    # Create connected demarc with different pop
    mommy.make("access.DemarcationPoint",
        connection=connection)

    with pytest.raises(DemarcationPointUnavailable):
        demarcs_svc.demarcation_point_can_join_connection(
            demarcation_point=demarc,
            connection=connection)

