

"""
Filters for Access Models
"""

import django_filters
from django.db import models

from utils import filters
from jea.crm import filters as crm_filters
from jea.access import models as access_models
from jea.access.models import (
    DemarcationPoint,
    Connection,
    NetworkServiceConfig,
    NetworkFeatureConfig,
)


class DemarcationPointFilter(
        crm_filters.OwnableFilterMixin,
        django_filters.FilterSet,
    ):
    """Filters for Port Demarcation Points"""
    id = filters.BulkIdFilter()

    state = django_filters.CharFilter(
        field_name="state",
        lookup_expr="iexact",
        label="""
            Filter by state

            Example: production
        """)

    media_type = django_filters.CharFilter(
        field_name="media_type",
        lookup_expr="iexact",
        label="""
            Filter by media type (e.g. 10GBASE-LR")

            Example: 10GBASE-LR
        """)

    state__is_not = django_filters.CharFilter(
        field_name="state",
        lookup_expr="iexact",
        exclude=True,
        label="""
            Filter elements not in a specific state.

            Example: error
        """)

    pop = django_filters.CharFilter(
        field_name="point_of_presence",
        label="""
            Filter by point of presence (PoP).

            Example: 23
        """)


    class Meta:
        model = DemarcationPoint

        fields = [
            "name",
            "external_ref",
            "speed",
            "connection",
        ]

        filter_overrides = {
            models.CharField: {
                'filter_class':
                    django_filters.CharFilter,
                'extra': lambda f: {
                    'lookup_expr': 'iexact',
                },
            },
        }


class ConnectionFilter(
        crm_filters.OwnableFilterMixin,
        django_filters.FilterSet,
    ):
    """Filters for Connections"""
    id = filters.BulkIdFilter()

    state = django_filters.CharFilter(
        field_name="state", lookup_expr="iexact")

    state__is_not = django_filters.CharFilter(
        field_name="state", lookup_expr="iexact", exclude=True)

    mode = django_filters.CharFilter(
        field_name="mode", lookup_expr="iexact")

    mode__is_not = django_filters.CharFilter(
        field_name="mode", lookup_expr="iexact", exclude=True)

    demarc = django_filters.CharFilter(
        method="filter_demarcs")
    def filter_demarcs(self, queryset, _name, value):
        """Filter by demarcs"""
        demarc_ids = filters.must_list(value)

        return queryset.filter(demarcation_points__in=demarc_ids)


    class Meta:
        model = Connection

        fields = [
            "name",
            "external_ref",
        ]

        filter_overrides = {
            models.CharField: {
                'filter_class':
                    django_filters.CharFilter,
                'extra': lambda f: {
                    'lookup_expr': 'iexact',
                },
            },
        }


class NetworkServiceConfigFilter(
        crm_filters.OwnableFilterMixin,
        django_filters.FilterSet,
    ):
    """Filter polymorphic network service configs"""
    id = filters.BulkIdFilter()

    state = django_filters.CharFilter(
        field_name="state", lookup_expr="iexact")

    state__is_not = django_filters.CharFilter(
        field_name="state", lookup_expr="iexact", exclude=True)

    service = django_filters.CharFilter(
        field_name="network_service")

    # Filter by type
    """
    TYPE_CHOICES = (
        (access_models.NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN,
         access_models.NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN),
        (access_models.NETWORK_SERVICE_CONFIG_TYPE_CLOSED_USER_GROUP,
         access_models.NETWORK_SERVICE_CONFIG_TYPE_CLOSED_USER_GROUP),
        (access_models.NETWORK_SERVICE_CONFIG_TYPE_ELINE,
         access_models.NETWORK_SERVICE_CONFIG_TYPE_ELINE),
        (access_models.NETWORK_SERVICE_CONFIG_TYPE_CLOUD,
         access_models.NETWORK_SERVICE_CONFIG_TYPE_CLOUD),
    )
    """
    TYPE_CHOICES = (
        (access_models.NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN,
         access_models.NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN),
    )

    type = django_filters.ChoiceFilter(
        method="filter_type",
        choices=TYPE_CHOICES)

    def filter_type(self, queryset, _name, value):
        """Filter polymorphic by type"""
        service_class = access_models.INV_NETWORK_SERVICE_CONFIG_TYPES  \
            .get(value)
        if not service_class:
            return queryset.none()

        # Apply filter
        return queryset.instance_of(service_class)


    class Meta:
        model = NetworkServiceConfig

        fields = [
            "inner_vlan",
            "outer_vlan",
            "capacity",
            "external_ref",
            "connection",
        ]

        filter_overrides = {
            models.CharField: {
                'filter_class':
                    django_filters.CharFilter,
                'extra': lambda f: {
                    'lookup_expr': 'iexact',
                },
            },
        }


class NetworkFeatureConfigFilter(
        crm_filters.OwnableFilterMixin,
        django_filters.FilterSet,
    ):
    """Filter polymorphic network service feature configs"""
    """
    TYPE_CHOICES = (
        (access_models.NETWORK_FEATURE_CONFIG_TYPE_BLACKHOLING,
         access_models.NETWORK_FEATURE_CONFIG_TYPE_BLACKHOLING),
        (access_models.NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER,
         access_models.NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER),
        (access_models.NETWORK_FEATURE_CONFIG_TYPE_IXPROUTER,
         access_models.NETWORK_FEATURE_CONFIG_TYPE_IXPROUTER),
    )
    """
    TYPE_CHOICES = (
        (access_models.NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER,
         access_models.NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER),
    )

    id = filters.BulkIdFilter()

    state = django_filters.CharFilter(
        field_name="state", lookup_expr="iexact")

    state__is_not = django_filters.CharFilter(
        field_name="state", lookup_expr="iexact", exclude=True)

    service_config = django_filters.CharFilter(
        field_name="network_service_config")

    # Polymorphic type filtering
    # TODO: Generalize.
    type = django_filters.ChoiceFilter(
        method="filter_type",
        choices=TYPE_CHOICES)

    def filter_type(self, queryset, _name, value):
        """Filter polymorphic by type"""
        service_class = access_models.INV_NETWORK_FEATURE_CONFIG_TYPES  \
            .get(value)
        if not service_class:
            return queryset.none()

        # Apply filter
        return queryset.instance_of(service_class)


    class Meta:
        model = NetworkFeatureConfig

        fields = [
            "network_feature",
            "purchase_order",
            "contract_ref",
            "external_ref",
            "managing_customer",
            "consuming_customer",
        ]

        filter_overrides = {
            models.CharField: {
                'filter_class':
                    django_filters.CharFilter,
                'extra': lambda f: {
                    'lookup_expr': 'iexact',
                },
            },
        }
