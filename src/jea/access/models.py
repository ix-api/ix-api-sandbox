
"""
Service Access Models
---------------------

Access to services is granted to customers through
these models.
"""

import enumfields
from django.db import models
from django.contrib.postgres import fields as postgres_fields
from polymorphic.models import PolymorphicModel

from utils.datastructures import reverse_mapping
from jea.ipam.models import AddressFamilies
from jea.crm.models import (
    OwnableMixin,
    InvoiceableMixin,
    CustomerScopedMixin,
)
from jea.eventmachine.models import StatefulMixin
from jea.catalog.models import (
    MediaType,
)

class BGPSessionType(enumfields.Enum):
    """BGP session type"""
    TYPE_ACTIVE = "active"
    TYPE_PASSIVE = "passive"


class RouteServerSessionMode(enumfields.Enum):
    """RouteServer session mode"""
    MODE_PUBLIC = "public"
    MODE_COLLECTOR = "collector"


class ConnectionMode(enumfields.Enum):
    """The mode of a connection"""
    MODE_LACP = "lag_lacp"
    MODE_STATIC = "lag_static"
    MODE_FLEX_ETHERNET = "flex_ethernet"
    MODE_STANDALONE = "standalone"


class LACPTimeout(enumfields.Enum):
    """The LACP timeout"""
    TIMEOUT_SLOW = "slow"
    TIMEOUT_FAST = "fast"


class Connection(
        StatefulMixin,
        OwnableMixin,
        InvoiceableMixin,
        CustomerScopedMixin,
        models.Model,
    ):
    """
    A model for connections.

    Connections describe the combination of several demarcs
    into a, well, connection. This connection can for example
    be a LAG.

    Remember to require a `billing` and `implementation` contact
    in the contacts array when creating.
    """
    name = models.CharField(max_length=255)

    lacp_timeout = enumfields.EnumField(
        LACPTimeout,
        max_length=4,
        null=True,
        blank=True)

    mode = enumfields.EnumField(ConnectionMode, max_length=16)
    speed = models.PositiveIntegerField(null=True, blank=True)

    def __str__(self):
        """Connection to String"""
        return (f"Connection: {self.name}, "
                f"Mode: {self.mode} "
                f"LACP: {self.lacp_timeout}")

    def __repr__(self):
        """Connection representation"""
        return (f"<Connection id='{self.pk}' name='{self.name}' "
                f"mode='{self.mode}' lacp='{self.lacp_timeout}'>")


class DemarcationPoint(
        StatefulMixin,
        OwnableMixin,
        InvoiceableMixin,
        CustomerScopedMixin,
        models.Model,
    ):
    """
    A model for the (port) demarcation point.
    This represents the demarc assigned to a customer.
    """
    name = models.CharField(max_length=255)

    media_type = models.CharField(max_length=20)
    speed = models.PositiveIntegerField(null=True, blank=True)

    # Other Relations
    point_of_presence = models.ForeignKey(
        "catalog.PointOfPresence",
        on_delete=models.PROTECT,
        related_name="demarcation_points")

    connection = models.ForeignKey(
        Connection,
        on_delete=models.PROTECT,
        null=True, blank=True,
        related_name="demarcation_points")


    def __str__(self):
        """Demarc as String"""
        return f"PortDemarc: {self.name}"

    def __repr__(self):
        """Demarc representation"""
        return (f"<DemarcationPoint id='{self.pk}' "
                f"name='{self.name}' "
                f"consumed_by='{self.consuming_customer_id}' "
                f"managed_by='{self.managing_customer_id}'>")

    class Meta:
        verbose_name = "Demarc"

#
# Service Access Models
#

class NetworkServiceConfig(
        StatefulMixin,
        OwnableMixin,
        InvoiceableMixin,
        CustomerScopedMixin,
        PolymorphicModel,
    ):
    """
    Polymorphic NetworkService access model.

    Network access is granted by creating an access object
    associated with a billing and owning customer.

    The model references `service.NetworkService`.
    """
    inner_vlan = models.PositiveSmallIntegerField(null=True, blank=True) 
    outer_vlan = models.PositiveSmallIntegerField(null=True, blank=True)

    capacity = models.PositiveIntegerField(null=True, blank=True)

    # Relations
    connection = models.ForeignKey(
        Connection,
        related_name="network_service_configs",
        on_delete=models.PROTECT)

    network_service = models.ForeignKey(
        "service.NetworkService",
        related_name="configs",
        on_delete=models.PROTECT)


class ExchangeLanNetworkServiceConfig(NetworkServiceConfig):
    """
    An ExchangeLAN network service config model.

    Grant access to a an exchange lan and configure features.
    """
    asns = postgres_fields.ArrayField(
        models.PositiveIntegerField(),
        default=list,
        size=20)

    def __str__(self):
        """String from exchange lan network service access"""
        return f"ExchangeLAN NetworkService Configuration for ASNs: {self.asns}"

    def __repr__(self):
        """ExchangeLanNetworkServiceAccess representation"""
        return (f"<ExchangeLanNetworkServiceConfig id='{self.pk}' "
                f"asns='{self.asns}'>")

    class Meta:
        verbose_name = "ExchangeLAN NetworkService Config"


class ClosedUserGroupNetworkServiceConfig(NetworkServiceConfig):
    """
    A config model for closed user groups.

    The closed user group only references mac addresses.
    """
    def __str__(self):
        """String from closed user group service access"""
        return f"ClosedUserGroup NetworkService Configuration (Id: {self.pk})"

    def __repr__(self):
        """ClosedUserGroupNetworkServiceAccess representation"""
        return f"<ClosedUserGroupNetworkServiceConfig id='{self.pk}'>"

    class Meta:
        verbose_name = "ClosedUserGroup NetworkService Config"


class ELineNetworkServiceConfig(NetworkServiceConfig):
    """
    A config model for an e-line network service.
    """
    def __str__(self):
        """String from e-line service config"""
        return f"E-Line NetworkService Configuration (Id: {self.pk})"

    def __repr__(self):
        """E-Line Access representation"""
        return f"<ELineNetworkServiceConfig id='{self.pk}'>"

    class Meta:
        verbose_name = "E-Line NetworkService Config"


class CloudNetworkServiceConfig(NetworkServiceConfig):
    """
    A cloud network service config model.
    """
    cloud_key = models.CharField(max_length=512)

    def __str__(self):
        """String from cloud service config"""
        return f"Cloud NetworkService Configuration (Id: {self.pk})"

    def __repr__(self):
        """E-Line Access representation"""
        return f"<CloudNetworkServiceConfig id='{self.pk}'>"

    class Meta:
        verbose_name = "Cloud NetworkService Config"


NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN = "exchange_lan"
NETWORK_SERVICE_CONFIG_TYPE_CLOSED_USER_GROUP = "closed_user_group"
NETWORK_SERVICE_CONFIG_TYPE_CLOUD = "cloud"
NETWORK_SERVICE_CONFIG_TYPE_ELINE = "eline"

NETWORK_SERVICE_CONFIG_TYPES = {
    ExchangeLanNetworkServiceConfig:
        NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN,
    ClosedUserGroupNetworkServiceConfig:
        NETWORK_SERVICE_CONFIG_TYPE_CLOSED_USER_GROUP,
    ELineNetworkServiceConfig:
        NETWORK_SERVICE_CONFIG_TYPE_ELINE,
    CloudNetworkServiceConfig:
        NETWORK_SERVICE_CONFIG_TYPE_CLOUD,
}

INV_NETWORK_SERVICE_CONFIG_TYPES = reverse_mapping(
    NETWORK_SERVICE_CONFIG_TYPES,
)

#
# Feature Access Configuration
#
class NetworkFeatureConfig(
        StatefulMixin,
        OwnableMixin,
        InvoiceableMixin,
        CustomerScopedMixin,
        PolymorphicModel,
    ):
    """
    The polymorphic feature access base model.

    Access to features like route servers are granted here.
    The configuration is stored in the specific feature
    access object.
    """
    # Relations
    network_feature = models.ForeignKey(
        "service.NetworkFeature",
        on_delete=models.PROTECT,
        related_name="configs")

    network_service_config = models.ForeignKey(
        NetworkServiceConfig,
        on_delete=models.CASCADE,
        related_name="network_feature_configs")


class RouteServerNetworkFeatureConfig(NetworkFeatureConfig):
    """
    Configure RouteServer Access

    This model contains everything required for granting
    customers access to a RS.
    """
    asn = models.PositiveIntegerField()
    password = models.CharField(max_length=128, blank=True)
    as_set_v4 = models.CharField(max_length=100, null=True)
    as_set_v6 = models.CharField(max_length=100, null=True)
    max_prefix_v4 = models.PositiveIntegerField(
        null=True,
        default=None)
    max_prefix_v6 = models.PositiveIntegerField(
        null=True,
        default=None)
    insert_ixp_asn = models.BooleanField()

    session_mode = enumfields.EnumField(
        RouteServerSessionMode, max_length=20)

    bgp_session_type = enumfields.EnumField(
        BGPSessionType, max_length=10)

    def __str__(self):
        """Routeserver Feature Access Configuration as String"""
        return f"RouteServer FeatureConfiguration for AS{self.asn}"

    def __repr__(self):
        """Representation of feature config"""
        return (f"<RouteServerFeatureConfig id='{self.pk}' "
                f"consuming_customer_id='{self.consuming_customer_id}'>")

    class Meta:
        verbose_name = "RouteServer FeatureAccess"
        verbose_name_plural = "RouteServer FeatureAccesses"


class IXPRouterNetworkFeatureConfig(NetworkFeatureConfig):
    """
    Configure IXPRouter Access

    Grants a customer access to the IXP Router feature.
    """
    password = models.CharField(max_length=128, blank=True)
    max_prefix = models.PositiveIntegerField(default=5000)
    bgp_session_type = enumfields.EnumField(
        BGPSessionType, max_length=10)

    def __str__(self):
        """IXP Router Feature Configuration as String"""
        return f"IXPRouter NetworkFeatureConfig, Customer: {self.consuming_customer_id}"

    def __repr__(self):
        """Representation of feature config"""
        return (f"<IXPRouterNetworkFeatureConfig id='{self.pk}' "
                f"consuming_customer_id='{self.consuming_customer_id}'>")

    class Meta:
        verbose_name = "IXPRouter Feature Config"


class BlackholingNetworkFeatureConfig(NetworkFeatureConfig):
    """
    Blackholing Feature Config

    Configure the blackholing feature
    """
    activated = models.BooleanField(default=False)
    ixp_specific_configuration = models.TextField(
        max_length=2048, blank=True, null=True)

    def __str__(self):
        """Blackholing Feature Configuration as String"""
        return (f"Blackholing NetworkFeatureConfig, "
                f"Customer: {self.consuming_customer_id}")

    def __repr__(self):
        """Representation of feature access"""
        return (f"<BlackholingNetworkFeatureConfig id='{self.pk}' "
                f"consuming_customer_id='{self.consuming_customer_id}'>")

    class Meta:
        verbose_name = "Blackholing Feature Config"


NETWORK_FEATURE_CONFIG_TYPE_BLACKHOLING = "blackholing"
NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER = "route_server"
NETWORK_FEATURE_CONFIG_TYPE_IXPROUTER = "ixp_router"

NETWORK_FEATURE_CONFIG_TYPES = {
    BlackholingNetworkFeatureConfig:
        NETWORK_FEATURE_CONFIG_TYPE_BLACKHOLING,
    RouteServerNetworkFeatureConfig:
        NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER,
    IXPRouterNetworkFeatureConfig:
        NETWORK_FEATURE_CONFIG_TYPE_IXPROUTER,
}

INV_NETWORK_FEATURE_CONFIG_TYPES = reverse_mapping(
    NETWORK_FEATURE_CONFIG_TYPES)

