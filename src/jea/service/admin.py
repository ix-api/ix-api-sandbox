
from django.contrib import admin

from jea.admin import site
from jea.service.models import (
    ExchangeLanNetworkService,
    ClosedUserGroupNetworkService,
    ELineNetworkService,
    CloudNetworkService,

    BlackholingNetworkFeature,
    RouteServerNetworkFeature,
    IXPRouterNetworkFeature,
)
from jea.ipam.models import (
    IpAddress,
)

#
# ExchangeLan Extra
#
class ExchangeLanIpAddressInline(admin.TabularInline):
    model = IpAddress
    extra = 1

    fields = ("version", "address", "prefix_length", "fqdn",
              "valid_not_before", "valid_not_after")

#
# Inline Model Admins: Features
#
class BlackholingNetworkFeatureInline(admin.TabularInline):
    model = BlackholingNetworkFeature
    extra = 0

class RouteServerNetworkFeatureInline(admin.TabularInline):
    model = RouteServerNetworkFeature
    extra = 0

class IXPRouterNetworkFeatureInline(admin.TabularInline):
    model = IXPRouterNetworkFeature
    extra = 0

#
# Model Admins: Services
#
class ExchangeLanNetworkServiceAdmin(admin.ModelAdmin):
    inlines = (ExchangeLanIpAddressInline,
               BlackholingNetworkFeatureInline,
               RouteServerNetworkFeatureInline,
               IXPRouterNetworkFeatureInline)

class ClosedUserGroupNetworkServiceAdmin(admin.ModelAdmin):
    pass


class ELineNetworkServiceAdmin(admin.ModelAdmin):
    pass


class CloudNetworkServiceAdmin(admin.ModelAdmin):
    pass





# Register model admins
site.register(ExchangeLanNetworkService,
              ExchangeLanNetworkServiceAdmin)
site.register(ClosedUserGroupNetworkService,
              ClosedUserGroupNetworkServiceAdmin)
site.register(ELineNetworkService,
              ELineNetworkServiceAdmin)
site.register(CloudNetworkService,
              CloudNetworkServiceAdmin)

