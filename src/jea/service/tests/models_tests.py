
"""
Services Models Tests
"""

import pytest
from model_mommy import mommy
from django.db.models import ProtectedError

from jea.service.models import (
    NetworkService,
    ExchangeLanNetworkService,
    ClosedUserGroupNetworkService,
    ELineNetworkService,
    CloudNetworkService,
    IXPSpecificFeatureFlag,
    NetworkFeature,
    BlackholingNetworkFeature,
    RouteServerNetworkFeature,
    IXPRouterNetworkFeature,
)

from jea.ipam.models import (
    IpAddress,
)


@pytest.mark.django_db
def test_network_services():
    """Test polymorphic network services"""
    s_1 = mommy.make(ExchangeLanNetworkService)
    s_2 = mommy.make(ClosedUserGroupNetworkService)
    s_3 = mommy.make(ELineNetworkService)
    s_4 = mommy.make(ClosedUserGroupNetworkService)

    # Find polymorphic by id
    assert NetworkService.objects.get(pk=s_1.id)
    assert NetworkService.objects.get(pk=s_2.id)
    assert NetworkService.objects.get(pk=s_3.id)
    assert NetworkService.objects.get(pk=s_4.id)


@pytest.mark.django_db
def test_network_service_protected_relations():
    """Check protected relations"""
    s_1 = mommy.make(ExchangeLanNetworkService)

    consuming_customer = s_1.consuming_customer
    managing_customer = s_1.managing_customer
    product = s_1.product

    assert consuming_customer, \
        "There should be an assigned owning customer"
    assert managing_customer, \
        "There should be an assigned billing customer"
    assert product, \
        "There should be an assigned product"

    # Deletion should not be possible unless the product referenced
    # service is gone
    with pytest.raises(ProtectedError):
        product.delete()

    with pytest.raises(ProtectedError):
        managing_customer.delete()

    with pytest.raises(ProtectedError):
        consuming_customer.delete()

    # After deleting the service, we should be able
    # to remove the refences.
    s_1.delete()
    product.delete()
    managing_customer.delete()
    consuming_customer.delete()


@pytest.mark.django_db
def test_exchange_lan_network_service():
    """Test Exchange Lan Network Service"""
    # Check, that we can assign IP addresses
    service = mommy.make(ExchangeLanNetworkService)
    ip = mommy.make(IpAddress)
    service.ip_addresses.add(ip)
    mommy.make(IpAddress, exchange_lan_network_service=service)

    assert service.ip_addresses.count() == 2 # one is already assigned


@pytest.mark.django_db
def test_exchange_lan_network_service_network_addresses_access():
    """Test Exchange Lan Network Service network address properties"""
    service = mommy.make(ExchangeLanNetworkService)
    ip4 = mommy.make(IpAddress, version=4)
    ip6 = mommy.make(IpAddress, version=6)

    service.ip_addresses.add(ip4)
    service.ip_addresses.add(ip6)

    assert service.ip_addresses.count() == 2

    assert ip4 == service.network_addresses_v4.first()
    assert ip6 == service.network_addresses_v6.first()


@pytest.mark.django_db
def test_cloud_network_service():
    """Test cloud network service"""
    service = mommy.make(CloudNetworkService)
    provider = service.cloud_provider

    # Test reference protection
    with pytest.raises(ProtectedError):
        provider.delete()


@pytest.mark.django_db
def test_polymorphic_features():
    """Test polymorphic features model"""
    s = mommy.make(NetworkService)

    f_1 = mommy.make(BlackholingNetworkFeature, network_service=s)
    f_2 = mommy.make(RouteServerNetworkFeature, network_service=s)
    f_3 = mommy.make(IXPRouterNetworkFeature, network_service=s)

    assert s.network_features.get(pk=f_1.pk)
    assert s.network_features.get(pk=f_2.pk)
    assert s.network_features.get(pk=f_3.pk)


@pytest.mark.django_db
def test_service_feature_relation():
    """Test feature to service relation"""
    s = mommy.make(NetworkService)
    f_1 = mommy.make(BlackholingNetworkFeature, network_service=s)

    assert f_1 in list(s.network_features.all())

    # Delete network service, feature should be gone aswell.
    s.delete()

    with pytest.raises(NetworkFeature.DoesNotExist):
        f_1.refresh_from_db()


@pytest.mark.django_db
def test_feature_routes_erver_ipaddresses():
    """Test Routeserver Feature with IP addresses"""
    feature = mommy.make(RouteServerNetworkFeature)
    ip_addr = mommy.make(IpAddress)
    feature.ip_addresses.add(ip_addr) # Alternative assignement

    assert feature.ip_addresses.count() == 1
    assert ip_addr.route_server_network_feature.pk == feature.pk

@pytest.mark.django_db
def test_feature_route_server_ipaddress_properties():
    """Test ip address property access"""
    feature = mommy.make(RouteServerNetworkFeature)
    ip4 = mommy.make(IpAddress, version=4)
    ip6 = mommy.make(IpAddress, version=6)
    feature.ip_addresses.add(ip4)
    feature.ip_addresses.add(ip6)

    assert feature.ip_addresses_v4.first() == ip4
    assert feature.ip_addresses_v6.first() == ip6

@pytest.mark.django_db
def test_feature_ixp_router():
    """Test Routeserver Feature with IP addresses"""
    ip_addr = mommy.make(IpAddress)
    feature = mommy.make(IXPRouterNetworkFeature, ip_addresses=[ip_addr])

    assert feature.ip_addresses.count() == 1
    assert ip_addr.ixp_router_network_feature.pk == feature.pk


@pytest.mark.django_db
def test_feature_ixprouter_ipaddress_properties():
    """Test ip address property access"""
    feature = mommy.make(IXPRouterNetworkFeature)
    ip4 = mommy.make(IpAddress, version=4)
    ip6 = mommy.make(IpAddress, version=6)
    feature.ip_addresses.add(ip4)
    feature.ip_addresses.add(ip6)

    assert feature.ip_addresses_v4.first() == ip4
    assert feature.ip_addresses_v6.first() == ip6

