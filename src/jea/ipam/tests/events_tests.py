
"""
Test events
"""

import pytest
from model_mommy import mommy

from jea.ipam import events

@pytest.mark.django_db
def test_ip_address_allocated():
    """Test ip address allocation event creator"""
    c1 = mommy.make("crm.Customer")
    s1 = mommy.make("service.ExchangeLanNetworkService")
    ip_address = mommy.make("ipam.IpAddress",
        scoping_customer=c1,
        exchange_lan_network_service=s1)
    event = events.ip_address_allocated(ip_address)

    assert event
    assert event.customer == c1


@pytest.mark.django_db
def test_ip_address_released():
    """Test ip address deallocation event creator"""
    ip_address = mommy.make("ipam.IpAddress")
    event = events.ip_address_released(ip_address)
    assert event


@pytest.mark.django_db
def test_mac_address_assigned():
    """Test mac address assigned event"""
    c1 = mommy.make("crm.Customer")
    mac_address = mommy.make("ipam.MacAddress",
        scoping_customer=c1)

    event = events.mac_address_assigned(mac_address)
    assert event
    assert event.customer == c1


@pytest.mark.django_db
def test_mac_address_removed():
    """Test mac address removal event"""
    mac_address = mommy.make("ipam.MacAddress")
    event = events.mac_address_removed(mac_address)
    assert event


@pytest.mark.django_db
def test_mac_address_created():
    """Test mac address created event"""
    mac_address = mommy.make("ipam.MacAddress")
    event = events.mac_address_created(mac_address)
    assert event

