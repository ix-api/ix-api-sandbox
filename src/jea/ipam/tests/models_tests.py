
"""
Test ip-address models
"""

import pytest
from model_mommy import mommy

from jea.ipam import models


def test_address_families_enum():
    """Test address families"""
    assert models.AddressFamilies.AF_INET
    assert models.AddressFamilies.AF_INET6


@pytest.mark.django_db
def test_host_ip_storage_v4():
    """Test saving a host ip"""
    customer = mommy.make("crm.Customer")
    ip_address = models.IpAddress(
        scoping_customer=customer,
        managing_customer=customer,
        consuming_customer=customer,
        version=models.IpVersion.IPV4,
        address="192.168.0.1",
        prefix_length=32)

    ip_address.save()

    # Test auto fields
    assert ip_address.assigned_at, \
        "assigned_at should be set and not falsy"
    assert ip_address.valid_not_before, \
        "valid_not_before should be set"

    # Some optional fields
    assert ip_address.fqdn is None


@pytest.mark.django_db
def test_host_ip_storage_v6():
    """Test saving a host ip"""
    # Test with an ipv6 address
    customer = mommy.make("crm.Customer")
    ip_address = models.IpAddress(
        scoping_customer=customer,
        managing_customer=customer,
        consuming_customer=customer,
        version=models.IpVersion.IPV6,
        address="fd23::42ec:ffff:bbbb:a001/64",
        fqdn="foobar.example.net",
        prefix_length=64)
    ip_address.save()

    assert ip_address.pk, \
        "IpAddress should have been saved and assigned a pk"


@pytest.mark.django_db
def test_network_address_v4():
    """Test saving a network address"""
    customer = mommy.make("crm.Customer")
    ip_net = models.IpAddress(
        managing_customer=customer,
        consuming_customer=customer,
        scoping_customer=customer,
        version=models.IpVersion.IPV6,
        address="10.0.0.0",
        fqdn="net.example.net",
        prefix_length=8)

    ip_net.save()
    assert ip_net.pk, \
        "IpAddress should have been saved and assigned a pk"

    assert ip_net.address == "10.0.0.0"


@pytest.mark.django_db
def test_network_address_v6():
    """Test saving a v6 ip"""
    # Test with an ipv6 address
    customer = mommy.make("crm.Customer")
    ip_net = models.IpAddress(
        scoping_customer=customer,
        consuming_customer=customer,
        managing_customer=customer,
        version=models.IpVersion.IPV6,
        address="fd42::0",
        fqdn="net6.example.net",
        prefix_length=32)

    ip_net.save()
    assert ip_net.pk, \
        "IpAddress should have been saved and assigned a pk"

    assert ip_net.address == "fd42::0"


@pytest.mark.django_db
def test_mac_address():
    """Test mac address model"""
    c = mommy.make("crm.Customer")
    mac = models.MacAddress(
        address="23:42:42:42:23:23",
        scoping_customer=c,
        managing_customer=c,
        consuming_customer=c)
    mac.save()

    assert mac.assigned_at, "Should be autofilled"
    assert mac.valid_not_before, "Should be autofilled"


def test_mac_address_representations():
    """Test to string an repr"""
    mac = models.MacAddress(address="23:42:42:42:23:23")

    assert str(mac), "__str__ should return a string"
    assert repr(mac), "__repr__ should return a string"

