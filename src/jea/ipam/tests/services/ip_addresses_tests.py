
"""
IP Address Service Tests
"""

import pytest
from model_mommy import mommy

from jea.exceptions import ResourceAccessDenied 
from jea.ipam.services import (
    ip_addresses as ip_addresses_svc,
)


@pytest.mark.django_db
def test_get_ip_addresses__base_queryset__service_features():
    """Test base queryset construction"""
    s1 = mommy.make("service.ExchangeLanNetworkService")
    f1 = mommy.make("service.RouteServerNetworkFeature")
    f3 = mommy.make("service.IXPRouterNetworkFeature")

    # Baseline IP
    ip0_manager = mommy.make("crm.Customer")
    ip0 = mommy.make("ipam.IpAddress", scoping_customer=ip0_manager)

    # Assign ips
    ip1 = mommy.make("ipam.IpAddress",
        exchange_lan_network_service=s1)
    ip2 = mommy.make("ipam.IpAddress",
        route_server_network_feature=f1)
    ip3 = mommy.make("ipam.IpAddress",
        ixp_router_network_feature=f3)


    # Test queryset construction
    qs = ip_addresses_svc.get_ip_addresses()

    assert not ip0 in qs, "Unrelated address should be filtered"

    # All other should be included.
    assert ip1 in qs
    assert ip2 in qs
    assert ip3 in qs


@pytest.mark.django_db
def test_get_ip_addresses__base_queryset__configs():
    """Test base queryset construction with service and feature configs."""
    c1 = mommy.make("crm.Customer")
    c2 = mommy.make("crm.Customer", scoping_customer=c1)

    sc1 = mommy.make("access.ExchangeLanNetworkServiceConfig",
        scoping_customer=c2,
        managing_customer=c1,
        consuming_customer=c2)

    fc1 = mommy.make("access.BlackholingNetworkFeatureConfig",
        scoping_customer=c2,
        managing_customer=c1,
        consuming_customer=c2)

    # Assign ips
    ip0_manager = mommy.make("crm.Customer")
    ip0 = mommy.make("ipam.IpAddress", scoping_customer=ip0_manager)

    ip1 = mommy.make("ipam.IpAddress",
        exchange_lan_network_service_config=sc1)
    ip2 = mommy.make("ipam.IpAddress",
        blackholing_network_feature_config=fc1)

    # Make queryset
    qs = ip_addresses_svc.get_ip_addresses()

    assert not ip0 in qs, "Unrelated ip should be filtered"

    # Again, all other should be included
    assert ip1 in qs
    assert ip2 in qs


@pytest.mark.django_db
def test_get_ip_address():
    """Get a ip address, test ownership"""
    customer = mommy.make("crm.Customer")
    service_config = mommy.make(
        "access.ExchangeLanNetworkServiceConfig",
        scoping_customer=customer,
        managing_customer=customer,
        consuming_customer=customer)
    ip1 = mommy.make(
        "ipam.IpAddress",
        scoping_customer=customer,
        exchange_lan_network_service_config=service_config)
    other_customer = mommy.make("crm.Customer")
    ip2 = mommy.make("ipam.IpAddress",
        scoping_customer=other_customer)

    assert ip_addresses_svc.get_ip_address(
        scoping_customer=customer,
        ip_address=ip1)

    assert ip_addresses_svc.get_ip_address(
        ip_address=ip2.pk)

    with pytest.raises(ResourceAccessDenied):
        ip_addresses_svc.get_ip_address(
            scoping_customer=customer,
            ip_address=ip2)


@pytest.mark.django_db
def test_get_assigned_exchange_lan_network_service_ips():
    """
    Test listing all ip addresses for a given exchange
    lan network service.
    """
    service = mommy.make("service.ExchangeLanNetworkService")
    service_config = mommy.make(
        "access.ExchangeLanNetworkServiceConfig",
        network_service=service)
    ip1 = mommy.make(
        "ipam.IpAddress",
        exchange_lan_network_service_config=service_config)
    ip2 = mommy.make("ipam.IpAddress")

    ips = ip_addresses_svc.get_assigned_exchange_lan_network_service_ips(
        exchange_lan_network_service=service)

    assert ip1 in ips
    assert not ip2 in ips


@pytest.mark.django_db
def test_allocate_exchange_lan_config_ip_address_v4():
    """Test allocating ip addresses for an excange lan"""
    customer = mommy.make("crm.Customer")
    exchange_lan = mommy.make("service.ExchangeLanNetworkService")
    net_ip = mommy.make(
        "ipam.IpAddress",
        exchange_lan_network_service=exchange_lan,
        version=4,
        address="100.100.100.0", prefix_length=24)

    config = mommy.make(
        "access.ExchangeLanNetworkServiceConfig",
        scoping_customer=customer,
        network_service=exchange_lan)

    # Allocate address
    ip4 = ip_addresses_svc \
        ._allocate_exchange_lan_network_service_config_ip_address(
            config, 4, customer)

    assert ip4
    assert ip4.pk


@pytest.mark.django_db
def test_allocate_exchange_lan_config_ip_address():
    """Test allocating ip addresses for an excange lan"""
    customer = mommy.make("crm.Customer")
    exchange_lan = mommy.make("service.ExchangeLanNetworkService")
    net_ip = mommy.make("ipam.IpAddress",
        exchange_lan_network_service=exchange_lan,
        version=6,
        address="1000:23:42::0", prefix_length=64)

    config = mommy.make("access.ExchangeLanNetworkServiceConfig",
        scoping_customer=customer,
        network_service=exchange_lan)

    # Allocate address
    ip6 = ip_addresses_svc \
        ._allocate_exchange_lan_network_service_config_ip_address(
            config, 6, customer)

    assert ip6
    assert ip6.pk


@pytest.mark.django_db
def test_update_ip_address():
    """Test updating an ip address object"""
    next_fqdn = "foo.bar42.net"
    customer = mommy.make("crm.Customer")
    ip = mommy.make("ipam.IpAddress")
    ip = ip_addresses_svc.update_ip_address(
        ip_address=ip,
        ip_address_update={
            "fqdn": next_fqdn,
            "managing_customer": customer,
            "consuming_customer": customer,
            "purchase_order": "foo23",
        })

    assert ip.fqdn == next_fqdn

