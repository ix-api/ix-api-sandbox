
"""
Test IP and Mac address Filters
"""

import pytest
from model_mommy import mommy

from jea.ipam.filters import (
    IpAddressFilter,
    MacAddressFilter,
)


@pytest.mark.django_db
def test_ip_address_filter__network_service():
    """
    Test filtering by polymorphic network service
    """
    s1 = mommy.make("service.ExchangeLanNetworkService")
    s2 = mommy.make("service.ExchangeLanNetworkService")
    s3 = mommy.make("service.ExchangeLanNetworkService")

    ip0 = mommy.make("ipam.IpAddress")
    ip1 = mommy.make("ipam.IpAddress",
        exchange_lan_network_service=s1)
    ip2 = mommy.make("ipam.IpAddress",
        exchange_lan_network_service=s2)
    ip3 = mommy.make("ipam.IpAddress",
        exchange_lan_network_service=s3)

    filtered = IpAddressFilter({
        "network_service": f"{s1.pk},{s2.pk}",
    })

    # Assertions

    assert ip1 in filtered.qs
    assert ip2 in filtered.qs

    assert not ip0 in filtered.qs
    assert not ip3 in filtered.qs


@pytest.mark.django_db
def test_ip_address_filter__network_service_config():
    """
    Test filtering by network service config
    """
    c1 = mommy.make("access.ExchangeLanNetworkServiceConfig")
    c2 = mommy.make("access.ExchangeLanNetworkServiceConfig")

    ip1 = mommy.make("ipam.IpAddress",
        exchange_lan_network_service_config=c1)
    ip2 = mommy.make("ipam.IpAddress",
        exchange_lan_network_service_config=c2)


    filtered = IpAddressFilter({
        "network_service_config": c1.pk,
    })

    assert ip1 in filtered.qs
    assert not ip2 in filtered.qs


@pytest.mark.django_db
def test_ip_address_filter__network_feature():
    """
    Test filtering by polymorphic network feature
    """
    f1 = mommy.make("service.RouteServerNetworkFeature")
    f2 = mommy.make("service.RouteServerNetworkFeature")
    f3 = mommy.make("service.IXPRouterNetworkFeature")

    ip1 = mommy.make("ipam.IpAddress")
    ip2 = mommy.make("ipam.IpAddress", route_server_network_feature=f1)
    ip3 = mommy.make("ipam.IpAddress", ixp_router_network_feature=f3)

    # Filter by feature:
    filtered = IpAddressFilter({
        "network_feature": f"{f1.pk},{f2.pk}"
    })

    assert not ip1 in filtered.qs
    assert not ip3 in filtered.qs

    assert ip2 in filtered.qs

    # Filter by other features
    filtered = IpAddressFilter({
        "network_feature": f3.pk
    })
    assert ip3 in filtered.qs
    assert not ip2 in filtered.qs
    assert not ip1 in filtered.qs


@pytest.mark.django_db
def test_ip_address_filter__network_feature_config():
    """
    Test filtering by network feature config
    """
    c1 = mommy.make("access.BlackholingNetworkFeatureConfig")
    c2 = mommy.make("access.BlackholingNetworkFeatureConfig")

    ip1 = mommy.make("ipam.IpAddress", blackholing_network_feature_config=c1)
    ip2 = mommy.make("ipam.IpAddress", blackholing_network_feature_config=c2)

    filtered = IpAddressFilter({
        "network_feature_config": f"{c1.pk}",
    })

    assert ip1 in filtered.qs
    assert not ip2 in filtered.qs


#
# MacAddresses
#


@pytest.mark.django_db
def test_mac_address_filter__network_service_config():
    """Test filter mac addresses by network service config"""
    sc1 = mommy.make("access.ExchangeLanNetworkServiceConfig")
    sc2 = mommy.make("access.ClosedUserGroupNetworkServiceConfig")

    # Assign mac addresses
    mac1 = mommy.make("ipam.MacAddress")
    mac1.exchange_lan_network_service_configs.add(sc1)

    mac2 = mommy.make("ipam.MacAddress")
    mac2.exchange_lan_network_service_configs.add(sc1)

    mac3 = mommy.make("ipam.MacAddress")
    mac3.closed_user_group_network_service_configs.add(sc2)
    

    # Filter mac address queryset
    filtered = MacAddressFilter({
        "network_service_config": f"{sc1.pk}",
    })

    assert mac1 in filtered.qs
    assert mac2 in filtered.qs
    assert not mac3 in filtered.qs

    # Filter including closed user group
    filtered = MacAddressFilter({
        "network_service_config": sc2.pk,
    })

    assert mac3 in filtered.qs
    assert not mac1 in filtered.qs
    assert not mac2 in filtered.qs
