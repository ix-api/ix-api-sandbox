
"""
Ip Address Management
---------------------

Define models for managing ip address across products,
services, features, and so on.
"""

import enumfields
from django.db import models
from django.utils import timezone
from jea.crm.models import (
    OwnableDeprecatedMixin,
    CustomerScopedMixin,
)


class IpVersion(enumfields.Enum):
    IPV4 = 4
    IPV6 = 6


class AddressFamilies(enumfields.Enum):
    AF_INET = "af_inet"
    AF_INET6 = "af_inet6"


class IpAddress(
        OwnableDeprecatedMixin,
        CustomerScopedMixin,
        models.Model,
    ):
    """
    An ip address model.

    Stores the ip, a assigned fqdn and
    the prefix length of the address.

    Ip addresses can be owned by a customer. And can be managed.
    """
    address = models.GenericIPAddressField()
    prefix_length = models.PositiveSmallIntegerField()
    version = enumfields.EnumIntegerField(IpVersion)

    fqdn = models.CharField(max_length=100, null=True, blank=False)

    assigned_at = models.DateTimeField(default=timezone.now)

    valid_not_before = models.DateTimeField(default=timezone.now)
    valid_not_after = models.DateTimeField(null=True, blank=False)

    # Relations
    #  - Services
    exchange_lan_network_service = models.ForeignKey(
        "service.ExchangeLanNetworkService",
        null=True, blank=True,
        related_name="ip_addresses",
        on_delete=models.CASCADE)

    # - Features
    route_server_network_feature = models.ForeignKey(
        "service.RouteServerNetworkFeature",
        null=True, blank=True,
        related_name="ip_addresses",
        on_delete=models.CASCADE)

    ixp_router_network_feature = models.ForeignKey(
        "service.IXPRouterNetworkFeature",
        null=True, blank=True,
        related_name="ip_addresses",
        on_delete=models.CASCADE)

    # - Service Access
    exchange_lan_network_service_config = models.ForeignKey(
        "access.ExchangeLanNetworkServiceConfig",
        null=True, blank=True,
        related_name="ip_addresses",
        on_delete=models.CASCADE)

    # - Feature Access
    blackholing_network_feature_config = models.ForeignKey(
        "access.BlackholingNetworkFeatureConfig",
        null=True, blank=True,
        related_name="filtered_prefixes",
        on_delete=models.CASCADE)


    def __str__(self):
        """IP address as string"""
        return self.address

    def __repr__(self):
        """Mac representation"""
        return f"<IpAddress id='{self.pk}' ip='{self.address}'>"


    @property
    def assigned_rel(self):
        """
        Get the assigned relation, if there is any.
        """
        if self.exchange_lan_network_service:
            return self.exchange_lan_network_service
        if self.exchange_lan_network_service_config:
            return self.exchange_lan_network_service_config
        if self.route_server_network_feature:
            return self.route_server_network_feature
        if self.ixp_router_network_feature:
            return self.ixp_router_network_feature
        if self.blackholing_network_feature_config:
            return self.blackholing_network_feature_config

    class Meta:
        verbose_name = "IP Address"
        verbose_name_plural = "IP Addresses"


class MacAddress(OwnableDeprecatedMixin, CustomerScopedMixin, models.Model):
    """
    A MAC address model: This model has ownership traits and
    is manageable.

    The ownership is defined by the billing and owning customer,
    which might be a reseller / subcustomer.
    """
    address = models.CharField(max_length=17)

    assigned_at = models.DateTimeField(default=timezone.now)
    valid_not_before = models.DateTimeField(default=timezone.now)
    valid_not_after = models.DateTimeField(null=True, blank=False)

    # Relations
    # - Service Access
    exchange_lan_network_service_configs = models.ManyToManyField(
        "access.ExchangeLanNetworkServiceConfig",
        related_name="mac_addresses")

    closed_user_group_network_service_configs = models.ManyToManyField(
        "access.ClosedUserGroupNetworkServiceConfig",
        related_name="mac_addresses")


    def __str__(self):
        """Mac address as string"""
        return self.address

    def __repr__(self):
        """Mac representation"""
        return f"<MacAddress id='{self.pk}' mac='{self.address}'>"


    class Meta:
        verbose_name = "MAC Address"
        verbose_name_plural = "MAC Addresses"

