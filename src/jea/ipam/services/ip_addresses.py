
"""
IP Addresses Services
---------------------

Allocate ip addresses and find all ip addresses relevant
for the customer.
"""
from typing import Optional, Iterable
import ipaddress

from django.core.exceptions import ValidationError, PermissionDenied

from jea.exceptions import ResourceAccessDenied
from jea.ipam.models import IpAddress
from jea.ipam.events import (
    ip_address_allocated,
)
from jea.crm.services import customers as customers_svc
from jea.access import models as access_models
from jea.service import models as service_models
from jea.service.services import network as network_svc
from jea.access.services import configs as configs_svc
from jea.ipam.filters import IpAddressFilter
from jea.eventmachine import active


def get_ip_addresses(
        scoping_customer=None,
        filters=None,
    ) -> Iterable[IpAddress]:
    """
    Get all ip addresses the managing customer can access.

    :param scoping_customer: The managing customer of the session
    :param filters: A mapping of filter attributes and values
    """
    # For a managing customer this includes all of
    # the managed ip addressed + all where scoping_customer 
    # is None
    queryset = IpAddress.objects.filter(
        scoping_customer__isnull=True)

    if scoping_customer:
        queryset |= scoping_customer.scoped_ipaddresss.all()

    filtered = IpAddressFilter(filters, queryset=queryset)

    return filtered.qs


def get_ip_address(
        scoping_customer=None,
        ip_address=None,
    ) -> Optional[IpAddress]:
    """
    Resolve a single ip address object. Perform ownership test
    in case a `scoping_customer` is present.

    :param scoping_customer: A customer managing the ip address object
    :param ip_address: An ip address identifier or object

    :raises ResourceAccessDenied: When ownership test fails.
    """
    if not ip_address:
        return None

    if not isinstance(ip_address, IpAddress):
        ip_address = IpAddress.objects.get(pk=ip_address)

    # Check permission
    if scoping_customer and \
        ip_address.scoping_customer and \
        ip_address.scoping_customer != scoping_customer:
        raise ResourceAccessDenied(ip_address)

    return ip_address


def get_assigned_exchange_lan_network_service_ips(
        scoping_customer=None,
        exchange_lan_network_service=None
    ):
    """Get all ip addresses in an exchange lan"""
    network_service = network_svc.get_network_service(
        scoping_customer=scoping_customer,
        network_service=exchange_lan_network_service)

    return IpAddress.objects.filter(
        exchange_lan_network_service_config__network_service= \
            network_service)


@active.command
def allocate_ip_address(
        dispatch,
        scoping_customer=None,
        network_service_config=None,
        version=4,
    ):
    """
    Allocate an ip address for a network service configuration
    or network feature configuration.
    """
    if scoping_customer and \
        network_service_config.scoping_customer != scoping_customer:
        raise ResourceAccessDenied(network_service_config)

    if isinstance(network_service_config,
                  access_models.ExchangeLanNetworkServiceConfig):
        ip_addr = _allocate_exchange_lan_network_service_config_ip_address(
            network_service_config, version,
            scoping_customer=scoping_customer)
    else:
        raise NotImplementedError

    if ip_addr:
        # Trigger state updates
        dispatch(ip_address_allocated(ip_addr))
        ip_addr.refresh_from_db()


    return ip_addr


def _allocate_exchange_lan_network_service_config_ip_address(
        exchange_lan_network_service_config,
        version,
        scoping_customer=None,
    ):
    """
    Allocate an ip address from the exchange lan service ip range

    TODO: The alogrithm can be optimized.
    """
    network_service = exchange_lan_network_service_config.network_service


    # Get the network address
    ip = network_service.ip_addresses.filter(version=version).first()
    if not ip:
        raise ValidationError(
            "Network prefix is not configured for exchange lan")

    net = ipaddress.ip_network(f"{ip.address}/{ip.prefix_length}")

    # get all allocated ips for the exchange lan
    allocated = get_assigned_exchange_lan_network_service_ips(
        exchange_lan_network_service=network_service)

    allocated_hosts = {ipaddress.ip_address(f"{ip.address}")
                       for ip in allocated}

    if version == 4:
        net_hosts = {host for host in net.hosts()}
        prefix_len = 32
    else:
        gen = net.hosts()
        net_hosts = {gen.send(None) for _ in range(1000)}
        prefix_len = 128

    available = net_hosts - allocated_hosts
    if not available:
        raise ValidationError(
            "IP pool for exchange lan exhausted")

    addr = available.pop()

    # Allocate address
    ip_address = IpAddress(
        version=version,
        address=str(addr),
        prefix_length=prefix_len,
        scoping_customer=scoping_customer,
        managing_customer=exchange_lan_network_service_config.managing_customer,
        consuming_customer=exchange_lan_network_service_config.consuming_customer,
        exchange_lan_network_service_config=\
            exchange_lan_network_service_config)
    ip_address.save()

    return ip_address


def update_ip_address(
        scoping_customer=None,
        ip_address=None,
        ip_address_update=None,
    ):
    """
    Update a ip address:
     - for now only updating the fqdn is implemented

    :param scoping_customer: A customer managing the ip address
    :param ip_address: The ip address to update
    :param ip_address_input: The update data
    """
    ip_address = get_ip_address(
        scoping_customer=scoping_customer,
        ip_address=ip_address)

    # Check for write capabilities
    if ip_address.scoping_customer != scoping_customer:
        raise ResourceAccessDenied(ip_address)

    update_keys = ip_address_update.keys()

    # Update ownership attributes
    if "purchase_order" in update_keys:
        ip_address.purchase_order = ip_address_update["purchase_order"]
    if "managing_customer" in update_keys:
        managing_customer = customers_svc.get_customer(
            customer=ip_address_update["managing_customer"],
            scoping_customer=scoping_customer)
        ip_address.managing_customer = managing_customer
    if "consuming_customer" in update_keys:
        consuming_customer = customers_svc.get_customer(
            customer=ip_address_update["consuming_customer"],
            scoping_customer=scoping_customer)
        ip_address.consuming_customer = consuming_customer 

    # Update FQDN
    if "fqdn" in update_keys:
        ip_address.fqdn = ip_address_update["fqdn"]

    ip_address.save()

    return ip_address

