

"""
Test events services
"""

import pytest
from model_mommy import mommy

from jea.eventmachine.services import events as events_svc

@pytest.mark.django_db
def test_get_events():
    """Test getting a list of customer events"""
    customer = mommy.make("crm.Customer")

    # Make events
    event = mommy.make("eventmachine.Event", customer=customer)

    customer_events = events_svc.get_events(customer=customer)
    assert event in customer_events


@pytest.mark.django_db
def test_get_events_limit():
    """Test getting events with a limit"""
    customer = mommy.make("crm.Customer")

    # Make events
    mommy.make("eventmachine.Event", customer=customer)
    mommy.make("eventmachine.Event", customer=customer)

    events = events_svc.get_events(customer=customer, limit=1)
    assert events.count() == 1

