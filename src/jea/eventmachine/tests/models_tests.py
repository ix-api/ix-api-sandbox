
"""
Models Tests
"""

import pytest
from model_mommy import mommy

from jea.eventmachine.models import (
    Task,
    Event,
    StatusMessage,
    Severity,
)


@pytest.mark.django_db
def test_event___repr__():
    """Test representation of events"""
    event = mommy.prepare(Event)
    assert isinstance(repr(event), str)


@pytest.mark.django_db
def test_event___str__():
    """Test representation of events"""
    event = mommy.prepare(Event)
    assert isinstance(str(event), str)


@pytest.mark.django_db
def test_stateful_mixin_status():
    """Test mixin with task"""
    # Create task and status
    task = mommy.make(Task)
    status = mommy.make(StatusMessage, ref=task)

    # Check if message relation holds
    messages = task.status.all()
    assert status in messages


@pytest.mark.django_db
def test_stateful_mixin_events():
    """Test mixin with events"""
    task = mommy.make(Task)

    # Create a status and a event
    event = mommy.make(Event, ref=task)

    # Check presence of event
    events = task.events.all()
    assert event in events


def test_severity_levels():
    """Test severity"""
    assert Severity.EMERGENCY == 0
    assert Severity.DEBUG == 7

