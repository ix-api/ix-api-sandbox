
"""
Test ActivePy
"""

import pytest
from mock import patch, MagicMock
from model_mommy import mommy

from jea.eventmachine.models import (
    Event,
)
from jea.eventmachine.active import (
    command,
    make_dispatch,
    handler,
)


@patch("jea.eventmachine.active.make_dispatch")
def test_command_decorating(make_dispatch):
    """Test decoraing a function"""

    @command
    def cmd(dispatch, request=None):
        assert isinstance(dispatch, MagicMock)

    # Check that our dispatch is created
    cmd(request=None)
    assert make_dispatch.called


@pytest.mark.django_db
def test_event_dispatching():
    """Test making a dispatched event"""

    event = mommy.make(Event)
    event.type = "TEST_EVENT"

    handle_foo = MagicMock()
    handler("TEST_EVENT")(handle_foo)

    dispatch = make_dispatch(None)
    dispatch(event)

    assert handle_foo.called


@pytest.mark.django_db
def test_handler_filter():
    """Test handler decoration"""
    event_a = mommy.make(Event)
    event_a.type = "TEST_EVENT_A"

    event_b = mommy.make(Event)
    event_b.type = "TEST_EVENT_B"

    @handler(
        "TEST_EVENT_A"
    )
    def some_handler(dispatch, event):
        assert isinstance(event, Event)
        print("Handler called with event: {}".format(event))

    some_handler(event_a)
    some_handler(event_b) # ignored.


@patch("jea.eventmachine.active.register_handler")
def test_handler_registration(register_handler):
    """Test handler registration"""

    @handler()
    def some_handler(dispatch, action):
        pass

    assert register_handler.called
