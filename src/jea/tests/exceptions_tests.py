
"""
Test core exceptions
"""
import pytest
from model_mommy import mommy

from jea import exceptions as jea_exceptions
from jea.access import models as access_models

def test_resource_access_denied__resource_name():
    """Test ResourceAccessDenied"""
    exc = jea_exceptions.ResourceAccessDenied("foo")
    assert str(exc)
    print(str(exc))


def test_resource_access_denied__model_resource():
    """Teest ResourceAccessDenied with model"""
    exc = jea_exceptions.ResourceAccessDenied(
        access_models.NetworkServiceConfig, 23)
    assert str(exc)
    print(str(exc))


@pytest.mark.django_db
def test_resource_access_denied__model_instance():
    """Test access denied with model object"""
    obj = mommy.make("crm.Customer")
    exc = jea_exceptions.ResourceAccessDenied(obj)
    assert str(exc)
    print(str(exc))


def test_validation_error():
    """Test validation error"""
    field = "test"
    exc = jea_exceptions.ValidationError(field=field)
    assert exc.field


def test_validation_error_default_field():
    """Test default field behaviour of exception"""
    class Exc(jea_exceptions.ValidationError):
        default_field = "some field"

    exc = Exc()
    assert exc.field == "some field"


def test_condition_assertion_error():
    """Test assertion error"""
    class Exc(jea_exceptions.ConditionAssertionError):
        pass

    exc = Exc()
    assert exc
