
"""
Custom Filters, not only JEA specific
"""

import django_filters


class BulkIdFilter(django_filters.Filter):
    """
    Treat value as a csv list of ids and match inclusion in set.
    """
    def filter(self, qs, value):
        """
        Filter by list of ids

        :param qs: A queryset
        :param value: The passed value
        """
        if not value:
            return qs

        field_name = self.field_name
        if not field_name:
            field_name = "pk"

        lookup = f"{field_name}__in"
        pk_set = [pk.strip() for pk in value.split(",")]

        return qs.filter(**{lookup: pk_set})


def must_list(value):
    """
    We require a parameters list.
    If the value is a CSV-string, we will unpack
    the content as a list.

    :param value: The incoming query value
    :returns: A list.
    """
    if isinstance(value, str):
        value = [v.strip() for v in value.split(",")]
    if not isinstance(value, list):
        value = [value]

    return value
