
"""
Prompt Toolkit Helpers and Validators
-------------------------------------

Here you can find some helpers for printing
a help toolbar with the default value or validate
inputs.
"""

from functools import wraps

from prompt_toolkit.validation import (
    Validator,
    ValidationError,
)
from prompt_toolkit.formatted_text import HTML
from prompt_toolkit.shortcuts.progress_bar import ProgressBar
from prompt_toolkit.shortcuts.progress_bar import formatters
from prompt_toolkit import prompt, print_formatted_text

#
# Widgets
#
def toolbar_help(default=None, description=None):
    """
    Show a small toolbar with the default value
    and a small description if provided.
    """
    def toolbar():
        """Create the toolbar"""
        markup = ""
        if default:
            markup += f"Default: <b>{default}</b>    "

        if description:
            markup += f"<i>{description}</i>"

        return HTML(markup)

    return toolbar


class task:
    def __init__(self, result_key=None):
        """Initialize Task"""
        self.result_key = result_key

    def __call__(self, fn):
        """Wrap function"""
        @wraps(fn)
        def _wrapper(options, state):
            if fn.__code__.co_argcount == 1:
                return (self.result_key, fn(options))
            else:
                return (self.result_key, fn(options, state))


        return _wrapper


def progress_exec_tasks(task_set, options):
    """
    Create customized progress bar and execute tasks.
    Datastructure:
        (label, [task, task, task, ...)

    """
    progress_formatters = [
        formatters.Rainbow(formatters.Text(' (')),
        formatters.Rainbow(formatters.SpinningWheel()),
        formatters.Rainbow(formatters.Text(') ')),
        formatters.Label(),
    ]

    # Execute tasks and store results
    results = {}
    for i in range(0, len(task_set), 2):
        label = task_set[i]
        tasks = task_set[i+1]

        with ProgressBar(formatters=progress_formatters) as pb:
            for task in pb(tasks, label=label):
                result_key, result = task(options, results)

                if result_key:
                    results[result_key] = result

    return results

#
# Formatters
#
def print_value_table(table):
    """
    Print a table of key value pairs
        Key 1:      Value
        My Key 2:   Value 2
    """
    for kv in table:
        if len(kv) == 1 or not isinstance(kv, tuple):
            print("")
        else:
            key, value = kv
            markup = HTML(f"\t<grey>{key}</grey>: \t <b>{value}</b>")
            print_formatted_text(markup)

#
# Validators
#
class NumberValidator(Validator):
    """Validate numeric input"""
    def validate(self, document):
        text = document.text

        if text and not text.isdigit():
            i = 0
            # Get index of fist non numeric character.
            # We want to move the cursor here.
            for i, c in enumerate(text):
                if not c.isdigit():
                    break

            raise ValidationError(
                message="This input contains non-numeric characters.",
                cursor_position=i)

