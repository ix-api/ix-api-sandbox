
"""
Datastructure Utilities
"""

from typing import List
from copy import deepcopy



def reverse_mapping(mapping: dict) -> dict:
    """
    Creates the inverse mapping of a dict type
    """
    return {v: k for k, v in mapping.items()}


def whitelist_filter_fields(data: dict, fields : List[str]) -> dict:
    """
    Remove fields from data, if fields are not whitelisted.
    Created a copy to prevent mutations

    :param data: The data dict
    :param fields: A list of allowed fields
    """
    filtered = deepcopy(data)

    for key in data.keys():
        if not key in fields:
            del filtered[key]

    return filtered

