#!/usr/bin/env bash

USER_ID=${LOCAL_USER_ID:-9999}
GROUP_ID=${LOCAL_GROUP_ID:-9999}

echo "Current user: $(id -u app)"
echo "Starting with UID : $USER_ID"

if [[ "$USER_ID" != "$(id -u app)" ]]; then
    usermod -u $USER_ID app;
    groupmod -g $GROUP_ID app;
    chown -R -v $USER_ID:$GROUP_ID /home/app
    usermod -g $GROUP_ID app;
#    chmod 0600 /home/app/.ssh/*;
#    chmod 0644 /home/app/.ssh/*.pub /home/app/.ssh/authorized_keys /home/app/.ssh/known_hosts;

fi

echo "Polishing with UID: $USER_ID"
