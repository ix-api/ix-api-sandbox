
# Joint External API - Sandbox

This is a reference implementation for the joint external api effort.

## Running the Sandbox 

To run either the production or development version you will need a local checkout of the codebase:

    git clone https://gitlab.com/ix-api/ix-api-sandbox.git

## Released docker version

This will download and run a prepared docker image according to the latest release tag.

Note: you must be logged into the GitLab docker registry and Docker hub in order to access the images

	docker login 
    docker login registry.gitlab.com
	
Create the Docker named volume:

    make storage

Start the Sandbox environment with a simple make command:

    make up

Ensure the Sandbox database is up-to-date

    make migrate

Create a superuser account for the admin interface

    make superuser

Populate the Sandbox with mock data

    make bootstrap

The Sandbox is now running at [http://localhost:8000](http://localhost:8000)
    

## Development
The `bin/` directory contains several convenience scripts, which you can use to build and run the latest development version (`develop` branch)

### Setup

For setting up the development environment just run
    
    make containers_dev
    
Start the sandbox

    make dev
    
 (this takes a while, be patient before proceeding in another terminal window - 
 once you see 'database system is ready to accept connections' the system is ready for the next step)

Run all migrations by executing

    ./bin/manage migrate

Create an admin user:

    ./bin/manage createsuperuser

  NOTE: This will create an admin user, able to login to the sandbox's backend. You do not get an API_KEY and API_SECRET

Afterwards you should populate your catalog and configure your
IXP by running


    ./bin/manage bootstrap

  NOTE: This will create a new DB dataset. It will _not_ destruct the user created in the previous step, but only a new API user (think of this user as a reseller)


### Running

Make sure all containers are running:

    make dev

Then you can start a development server listening on port `:8000` with:

    ./bin/runserver

Now you should be able to see the standbox welcome
page on http://localhost:8000/

Additionally there are background jobs to be executed by workers.
To start the worker use:

    ./bin/runworker


### Testing

Run all tests with

    ./bin/test

Add flags for more verbose output

    ./bin/test -s -v


### Getting the OpenAPI specs

Start the sandbox and download `/api/docs/v1/schema.json` or `/api/docs/v1/schema.yaml`.


## Project Structure

All sanbox related apps are located within the `src/jea` module.
The sandbox is split into the following apps:

 - `jea.public` A minimal landing page
 - `jea.auth` The authentication backend application providing authentication services
 - `jea.catalog` The service catalog
 - `jea.crm` All crm related services
 - `jea.service` Package for network services and features
 - `jea.api.v1` The v1 implementation of the JointExternalApi.

### Tests

Tests are grouped by app and a `tests/` directory is located in the
first level of the application's root (e.g. `src/jea/api/v1/tests/`).

The test folder structure should reflect the structure of the app.

### The `jea.api.v1` application

The API application reflects to some degree the overall project structure.

All resources are grouped in their specific module: A resource usually
consists of a `views.py` and a `serializers.py`. Additional resource
related modules should be placed within the directory of the resource.

Example:
    
    src/jea/api/v1/crm/serializers.py
    src/jea/api/v1/crm/views.py


## Bootstrapping

The JEA sandbox comes with management command to setup a
demonstration / testing environment with a populated catalog
and test customers.

This is done by running the `jea.ctrl.managment` command: `bootstrap`.

### Commandline Arguments

You can provide various settings directly on invocation.
If some information is missing, bootstrap will ask you for it
or will generate some made up values.

    --yes
        Assume yes when asked, e.g. when clearing the database

    --api-key
        Provide a fixed api key

    --api-secret
        Provide a fixed api secret for the reseller customer

    --exchange-name
        The name of your exchange.

        Bootstrap will prompt for input when in interactive mode, 
        or will suggest some random name.

    --exchange-asn
        The IXPs ASN.

        Bootstrap will prompt for input or will make
        some ASN up from the private AS range.

    --api-customer
        The name of the reseller / API customer

    --defaults
        Don't prompt for any input, just make something up in case
        it's not provided by the above flags.

## Deploying to production

### Base image introduction

The `sandbox` app uses a base image created by `phusion`, the creators of `passenger` application server. They are a `ruby` oriented company, but their base images include `python` and `nodejs`. In our case, the [pasenger-docker](https://github.com/phusion/passenger-docker)'s `nodejs` variant was used. In a nutshell the advantages this provides are:

- It can run the app using a non-root user (/sbin/setuser). This is similar ot `gosu`
- It provides a neat and easy way to run additional `daemons` during the boot process (similar to `supervisord`)
- You can add startup scripts under `/etc/my_init.d` which run during the boot process
- It has a very small memory print
- It already includes python3 (v3.6.8)

### Specifically for our deployment workflow

#### Container user permissions vs host user permissions

Upon the container's boot there is a script which reads the `LOCAL_USER_ID` and `LOCAL_GROUP_ID` environmental variables. **Those are defined in the `shared/.env` file and refer to the current host user (in our case this is `gitlab-runner`); it then uses those to adjust the `/home/app` folder ownership. You can check what this exactly does at `marina/rootfs/etc/my_init.d/01_adjust_home_app_perms.sh` 

#### Location of the `sandbox` app on the filesystem

While we are using the `deployer` user as our user managing the deployments, the `sandbox` production environment is deployed under the `gitlab-runner`'s home folder. The reason is that the`gitlab-ci.yml` is executed by that user and its home folder saves us from any permission-related errors which may brake the deployment workflow

#### Access to the deployment server as `gitlab-runner`

You can either login to our `aws` server as `deployer` and then `sudo su - gitlab-runner` or `ssh` directly to it as `gitlab-runner` using the ssh configuration you use for the `deployer` user. Both users are now aware of the same authorized keys.

#### Structure of the deployment folder

The `sandbox` is deployed under `/home/gitlab-runner/sandbox.live` folder:

```
sandbox.live
    ├── docker-compose.yml >> Copy of `docker-compose.live.yml`
    └── shared
        ├── .env >> The environment configuration variables
        ├── pg-data >> Folder keeping the postgres database files
        └── reset-logs >> Folder keeping the messages generated by the `bootstrap` command. You will find here the customer's ID, API_KEY and API_SECRET along with other info
```

#### How the `sandbox` app and its dependencies are run inside the container

There are two daemons that need to be run for the `sandbox` app to perform correctly:

- The `celery` background job daemon
- The django `www` server

They are both run as daemons. Their execution is described in their relevant files found under `marina/rootfs/etc/service`

This may raise the question of what is then run to keep the container in a running state. This is the answer, found in the `Dockerfile.live`:

    CMD ["tail","-f","/dev/null"]


#### Logging in to the `sandbox` container

There an alias setup for `docker-compose`: `drc`

The `sandbox` `django` server is run under the `app` user, so you need to login as such:

- For an already running `app` service container:

    If you want to login as `app` (recommended):

        gitlab-runner@ip-172-31-41-172:~/sandbox.live$ drc exec -u app app bash -l
        app@481b3579602f:~/sandbox$

    If you want to login as `root`:

        gitlab-runner@ip-172-31-41-172:~/sandbox.live$ drc exec app bash -l
        root@481b3579602f:/home/app/sandbox#

- For a non-running `app` service container:

    If you want to login as `app` (recommended):
    
        gitlab-runner@ip-172-31-41-172:~/sandbox.live$ drc run --rm -e JEA_SBX_NO_INIT_SCRIPTS=1 app bash -l

    If you want to login as `root`:
    
        gitlab-runner@ip-172-31-41-172:~/sandbox.live$ drc run --rm -e JEA_SBX_NO_INIT_SCRIPTS=1 -e JEA_SBX_DEBUG=1 app bash -l
        
    You have probably noticed the env vars:
    `JEA_SBX_NO_INIT_SCRIPTS=1`: we ask that the initialization scripts do _not_ run.
    `JEA_SBX_DEBUG=1`: we ask to login as `root`
 
#### Available env vars
   
You can define any of those vars inside the `.env` file:

```
# The backend admin user username
JEA_SBX_SU_USERNAME=
# The backend admin user email
JEA_SBX_SU_EMAIL=
# The backend admin user password
JEA_SBX_SU_PASSWORD=
# The hostname of the postgres instance
DB_HOST=
# The password of the pg sandbox user
DB_PASSWORD=
# The username of the pg sandbox user
DB_USER=
# The database name of the sandbox app
DB_NAME=
# The password of the postgres user
POSTGRES_PASSWORD=

# The host OS current user id
LOCAL_USER_ID=

# The host OS current user group id
LOCAL_GROUP_ID=

#######   C A U T I O N   #########
####### DESTRUCTIVE TASKS #########

# Do we need a DB reset?
# 0 = no
# 1 = yes
JEA_SBX_DB_RESET=0

# Do you want to recreate the superuser?
# 0 = no
# 1 = yes
JEA_SBX_SU_RESET=0

####### DESTRUCTIVE TASKS #########
#######       E N D       #########


# Do not run the init scripts
# 0 = run them
# 1 = do not run them
JEA_SBX_NO_INIT_SCRIPTS=

# Silence messages during the init script execution
# 0 = nope
# 1 = yeah
JEA_SBX_QUIET=

# Debug the container by logging in as root user
# 0 = nope (thus run as app user)
# 1 = yes
JEA_SBX_DEBUG=

# The hostname that nginx-proxy container will redirect requests to
VIRTUAL_HOST=

# The relevant port
VIRTUAL_PORT=

# Letsencrypt host
LETSENCRYPT_HOST=

# Letsencrypt email
LETSENCRYPT_EMAIL=

```
