
#
# JEA :: SANDBOX
# --------------
#
# Makefile for project tasks
#

VERSION ?= dev


#
# Development
#

containers_dev:
	./bin/docker-compose build

volumes_dev:
	docker volume create --name=jea-sandbox_postgres_dev

dev: volumes_dev
	./bin/docker-compose up


#
# Production / Deployment
#


clean:
	find . -type f -name '*.py[co]' -delete -o -type d -name __pycache__ -delete

build_prod:
	./bin/docker build . -f Dockerfile.prod -t registry.gitlab.com/ix-api/ix-api-sandbox -t registry.gitlab.com/ix-api/ix-api-sandbox:$(VERSION)

push_prod:
	./bin/docker push registry.gitlab.com/ix-api/ix-api-sandbox:$(VERSION)

push_prod_latest:
	./bin/docker push registry.gitlab.com/ix-api/ix-api-sandbox:latest

new:
	make storage
	make up
	make migrate
	make superuser
	make bootstrap

storage:
	./bin/docker volume create --name jea-sandbox_postgres_prod
	
up:
	./bin/docker-compose -f docker-compose-prod.yml up

migrate:
	./bin/docker-compose -f docker-compose-prod.yml exec app python3 src/manage.py migrate

superuser:
	./bin/docker-compose -f docker-compose-prod.yml exec app python3 src/manage.py createsuperuser

bootstrap:
	./bin/docker-compose -f docker-compose-prod.yml exec app python3 src/manage.py bootstrap
